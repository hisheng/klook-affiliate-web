# klook-affiliate-web

affiliate 新架构仓库

```js
|-- root
  |-- config     // common config for frontend and server
  |-- locales    // common locales for frontend and server
  |-- shared     // shared utils for frontend and server
  |-- server     // affiliate server (egg.js) for web and widgets
  |-- web        // portal + admin (SPA)
  |-- widget     // ad widgets (SPA)
```


## 环境
- node v12.18.4
- npm 6.14.6

## 开发
分窗口执行以下命令

- `nvm use node@12` 依赖 node@12
- `npm install` 安装各个目录依赖
- `npm run dev-server` 启动 Server
- `npm run dev-web` 启动 web
- `npm run dev-widget` 启动 widget

访问 `http://localhost:8787`

## 打包
执行 `npm run build`，此命名包含两个子命令：

- `npm run build-web` 打包 web
- `npm run build-widget` 打包 widget

## 启动
在执行 `npm run build` 打包完毕后，可执行 `npm run start` 启动项目

访问 `http://localhost:8080`

> 再次启动时，需要先执行 `npm run stop` 停止 egg 进程


## 开发与部署时静态资源映射关系

![](https://res.klook.com/image/upload/v1632643680/cbs2l9empe1kaj3xleoy.png)


## TODOS

- [x] server 迁移 / 中间件迁移（到最新的 koa）(一周)
- [x] web 前端代码迁移到 vue-cli（初步，fix error, eslint）(一周)
- [x] server koa 升级为 eggjs (2-3 天)
- [x] widget 前端代码迁移到 vue-cli（从 web 中剥离，独立前端项目）(一周)
- [x] widget server 逻辑迁移 (2-3 天)

further plans

- [ ] 将 widget 的 server 逻辑从当前 eggjs 抽离，独立维护一个 koa server
- [ ] 将 widget 的 server 和前端拆分为独立仓库，并独立部署服务
- [ ] 所有的广告组件渲染都走 widget server, 和 portal 完全解耦，互不影响
- [ ] 将广告组件渲染交给 tetris
- [ ] web / widget 项目目录结构优化

web

- [ ] 完全移除 /web 中的 element-ui
- [ ] 完全移除 /web 中的 jquery
- [ ] 将 /web 中的 moment 替换为更轻量的 dayjs

widget

- [x] 完全移除 /widget 中的 jquery (只用到 jquery 的 ajax 部分)
- [x] 将 /widget 中的 lodash 改为按需
- [ ] 迁移 widget iframe 脚本