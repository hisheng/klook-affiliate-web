module.exports = {
    apps: [
        {
            name: 'klook-affiliate',
            script: 'npm',
            args: 'run start-test',
            exec_mode: 'fork',
            instances: 1,
            interpreter: 'node@12.18.3',
            output: '/root/.pm2/logs/klook-affiliate.log',
            error: '/root/.pm2/logs/klook-affiliate.log',
            logDateFormat: 'YYYY-MM-DD HH:mm:ss.SSS',
        },
    ],
};
