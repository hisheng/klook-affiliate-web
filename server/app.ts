import { Boot } from 'egg';
import initProxy from './app/proxy';
import initLogquery from './app/logquery';
import handleError from './app/util/error-handler';

class AppBootHook extends Boot {
  configDidLoad() {
    initProxy(this.app);
    initLogquery(this.app);
    handleError(this.app);
  }
}

module.exports = AppBootHook;
