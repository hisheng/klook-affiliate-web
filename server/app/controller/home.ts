import axios from 'axios';
import { Controller } from 'egg';
import * as config from '../../../config';
const util = require('../util');

export default class HomeController extends Controller {
  public async index() {
    const ranges = await this.service.common.getRanges();
    const renderCtx = {
      _lang: this.ctx.klkLang,
      _lang_path: this.ctx.klkLangPath,
      _lang_options: config.LANG_OPTIONS,
      _lang_options_widget: config.LANG_OPTIONS_WIDGET,
      _lang_options_api: config.LANG_OPTIONS_API,
      _lang_map: config.LANG_MAP,
      _all_lang_options: config.ALL_LANG_OPTIONS,
      _env: this.ctx.app.config.env,
      _url_langs_options: config.SUPPORT_LANGS_LIST,
      _support_currency: util.getCurrencyListByLang(this.ctx),
      _ranges: ranges || [],
      _user_info: this.ctx.user_info,
    };
    const metaData = {
      title: this.ctx.$t('NODE_site_title'),
      meta_keywords: this.ctx.$t('NODE_meta_keywords'),
      meta_description: this.ctx.$t('NODE_meta_description'),
      og_site_name: this.ctx.$t('NODE_og_site_name'),
      og_locale: config.KLK_LANG_OG_LOCALE_MAP[this.ctx.klkLang],
    };
    const locals = {
      ...renderCtx,
      ...metaData,
    };
    if (this.ctx.app.config.env === 'local') {
      const tplContent = await axios.get('http://localhost:8786/static/web/index.html').then(res => res.data);
      this.ctx.body = await this.ctx.renderString(tplContent, locals);
    } else {
      await this.ctx.render('web/index.html', locals);
    }
    // await this.ctx.render('web/index.njk', locals);
  }
}
