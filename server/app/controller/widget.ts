import axios from 'axios';
import { Controller } from 'egg';

// widget => tpl
const widgetTplMap = {
  dynamic_widget: 'dynamic_widget',
  static_widget: 'dynamic_widget',
  auto_dynamic_widget: 'auto_dynamic_widget',
  hotel_dynamic_widget: 'hotel_dynamic_widget',
  hotel_static_widget: 'hotel_dynamic_widget',
  auto_hotel_dynamic_widget: 'auto_hotel_dynamic_widget',
  search_vertical: 'search_vertical',
  banner: 'static_banner',
  mul_act: 'mul_act_banner',
  single_act: 'single_act_banner',
};

export default class WidgetController extends Controller {
  public async index() {
    await this.renderTpl(this.ctx.params.name);
  }

  public async v1() {
    const tpl = widgetTplMap[this.ctx.query.prod];
    if (!tpl) {
      this.ctx.status = 404;
      return;
    }
    await this.renderTpl(tpl);
  }

  public async v2() {
    const prod = this.ctx.query.prod;
    if (prod === 'search_banner') {
      const queryAdid = this.ctx.query.adid;
      await this.renderTpl('search_banner', {
        res_path_web: '//cdn.klook.com/affiliate_web',
        queryAdid,
      });
    } else if (widgetTplMap[prod]) {
      await this.renderTpl(widgetTplMap[prod]);
    } else {
      // 返回 404
      this.ctx.status = 404;
    }
  }

  public async v3() {
    await this.renderTpl('dynamic_widget');
  }

  public async test() {
    const isDev = this.ctx.app.config.env === 'local';
    await this.renderTpl('test', {
      ...this.ctx.app.config.widgetTestParams,
      isDev,
    });
  }

  private async renderTpl(tplName: string, locals?: any) {
    if (this.ctx.app.config.env === 'local') {
      const tplContent = await axios.get(`http://localhost:8785/static/widget/${tplName}.html`).then(res => res.data);
      this.ctx.body = await this.ctx.renderString(tplContent, locals);
    } else {
      await this.ctx.render(`widget/${tplName}.html`, locals);
    }
    // await this.ctx.render(`widget/${tplName}.html`, locals);
  }

  // 对历史的 iframe 脚本请求进行重定向
  // 主要有以下几类 url:
  // 1. ${host}/s/dist/desktop/affiliate_base_v3.2.js => Static Banners, Activity Banners
  // 2. ${host}/s/dist/desktop/dynamic_widget_v3.js => Dynamic Widgets
  // 3. ${host}/s/dist/desktop/hotel_dynamic_widget_v3.js => Dynamic Widgets(hotel)
  // 4. ${host}/s/dist/desktop/search_vertical_v3.js => Search Box
  public redirectHistoryIframeScriptUrl() {
    // TODO
    // const script = this.ctx.params.script;
    this.ctx.body = 'hello';
  }
}
