import { Context } from 'egg';
import { translate } from './locale';

export default {
  $t(this: Context, ...args) {
    // 支持 {0},{1} 这种格式
    return translate(this.klkLang, args[0], ...args.slice(1));
  },
};
