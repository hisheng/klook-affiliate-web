const fs = require('fs');
const path = require('path');
import * as config from '../../../config';

const locales = {};

const localesDir = path.join(__dirname, '../../../locales/');

config.SUPPORT_LANGS_LIST.forEach(lang => {
  const filename = lang + '.json';
  try {
    const data = fs.readFileSync(path.join(localesDir, filename));
    locales[lang] = JSON.parse(data);
  } catch (e) {
    console.log(`read or parse locales file error ${filename}: ${e}`);
  }
});

function translate(lang: string, key: string, ...args): string {
  const text = getText(key, lang);
  if (args) {
    return strFormat(text, args);
  }
  return text;
}

function getText(key: string, lang: string): string {
  const locale = locales[lang] || {};
  if (locale[key] || locale[key] === '') {
    return locale[key];
  }
  return key;
}

function strFormat(...args) {
  const format = /\{([\d\w\.]+)\}/g;
  let v;
  const str = args.shift() + '';
  if (args.length === 1 && typeof (args[0]) === 'object') {
    args = args[0];
  }
  format.lastIndex = 0;
  return str.replace(format, function(m, n) {
    v = args[n];
    return v === undefined
      ? m
      : v;
  });
}

export {
  translate,
};
