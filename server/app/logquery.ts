import { Application, LogqueryParams } from 'egg';
import Logquery from '@klook/logquery';

export default function(app: Application) {
  const { config: { env, logquery } } = app;

  if (env === 'local') return;

  const logQuery = new Logquery({
    url: logquery.url,
    dev: env !== 'prod',
    subject: 'log.go.frontend.klook-affiliate-front.node', // log.go.service.{{serviceName}}    其中serviceName和服务注册时使用的一致. exp: log.go.frontend.klook-nuxt-blog.node
    serviceName: 'klook-affiliate-front/node', // klook-xxx/xxxsrv {git仓库名}}/{{服务名}}
  });

  function addLogquery({ config, requestId, message }: LogqueryParams) {
    const attachments = {
      timestamp: Date.now(),
      level: config.level || 'E',
      message: JSON.stringify(message),
      tag: config.tag,
      funcName: config.funcName || '',
      requestId,
    };
    logQuery.service(attachments);
  }

  app.logquery = logQuery;
  app.context.logquery = logQuery;
  app.addLogquery = addLogquery;
  app.context.addLogquery = addLogquery;
}
