import { Context } from 'egg';

export default () => {
  return async function auth_klk(ctx: Context, next) {
    const token = ctx.cookies.get('sess', { signed: false });
    if (!token) {
      const path = ctx.path;
      ctx.status = 302;
      const redirectPath = `/${ctx.klkLangPath}home?redirect=${encodeURIComponent(path)}`;
      ctx.redirect(redirectPath);
      return;
    }
    await next();
  };
};
