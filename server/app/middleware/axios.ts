import { Context } from 'egg';
import axios from 'axios';
import { AxiosResponse, AxiosRequestConfig } from 'axios';
import * as config from '../../../config';
// const { sendToFeishu } = require('../../../shared/send_feishu');
import { formatToOptimus } from '../util';

axios.defaults.timeout = 20000;
axios.defaults.headers['affiliate-version'] = 2.0;
axios.defaults.headers['X-REQ-CLIENT'] = 'klook-affiliate';

function getCommonHeader(this: Context) {
  const headers: any = {
    'X-Klook-Request-Id': this.req.requestId,
    Currency: this.klkCurrency || 'HKD',
    token: this.userToken || '',
    'Accept-Language': this.klkLang && config.SUPPORT_LANGS[this.klkLang].name.replace('-', '_') || '',
  };
  if (this.header) {
    headers['User-Agent'] = this.header['user-agent'];
    headers.Referer = this.header.referer || '';
    headers.Cookie = this.header.cookie || '';
    headers['X-DeviceID'] = this.header['x-deviceid'] || '';
    headers['X-Request-From'] = this.header['X-Request-From'] || 'node';
  }
  if (this.cookies) {
    headers.sess = this.cookies.get('sess', { signed: false }) || '';
  }
  headers['X-FORWARDED-FOR'] = [ this.header && this.header['x-forwarded-for'], this.remoteUserIp ].filter(item => item).join(', ');
  return headers;
}

/* function sendErrorToFeishu(error) {
  const response = error.response;
  const err = new Error(
    response && response.data && response.data.error.message || '',
  );
  err.config = response.config;
  err.request = response.request;
  err.response = response;
  err.isAxiosError = true;
  sendToFeishu({
    level: 'E',
    message: err,
    type: 'Error(Axios)',
  });
} */

// 请求上报日志 格式化
function getLogRequestParams(ctx: Context, reqConfig: AxiosRequestConfig) {
  const { req, request } = ctx;
  const message = formatToOptimus({
    href: `${reqConfig.baseURL}${reqConfig.url}`,
    request_id: reqConfig.headers['X-Klook-Request-Id'],
    url: reqConfig.url,
    socket: request.socket || {},
    _duration_: Date.now() - (req.startTime || 0),
    status: '200', // 请求日志设置状态为 200
    method: reqConfig.headers && reqConfig.headers.method,
  });
  return {
    requestId: req.requestId,
    message,
  };
}

// response 上报格式化
function getLogParams(ctx: Context, resp: AxiosResponse) {
  const { req, request } = ctx;
  const message = formatToOptimus({
    href: resp && resp.config && resp.config.url,
    request_id: req.requestId,
    url: request.url,
    socket: request.socket || {},
    _duration_: Date.now() - (req.startTime || 0),
    status: resp && resp.status,
    method: resp && resp.config && resp.config.method,
  });
  return {
    requestId: req.requestId,
    message,
  };
}

export default () => {
  return async (ctx: Context, next) => {
    const addLogquery = ctx.app.addLogquery || function noop() {};
    // ctx.req.startTime = Date.now();
    const axiosIns = axios.create({
      baseURL: ctx.app.config.API_SERVER,
    });

    ctx.$axios = axiosIns;
    ctx.getAxiosHeaders = getCommonHeader.bind(ctx);

    axiosIns.interceptors.request.use((reqConfig: AxiosRequestConfig) => {
      const config = {
        level: 'I',
        funcName: 'axios.plugin.request',
        tag: 'axios.plugin.request',
      };
      // node 发送请求也上报一次日志
      addLogquery({ config, ...getLogRequestParams(ctx, reqConfig) });
      return reqConfig;
    }, e => {
      // node 请求错误, 发送lark
      /* sendToFeishu({
        level: 'E',
        message: err,
        type: 'Error(Axios)',
      }); */
      return Promise.reject(e);
    });

    axiosIns.interceptors.response.use((response: AxiosResponse) => {
      // Any status code that lie within the range of 2xx cause this function to trigger
      const config = {
        level: 'I',
        funcName: 'axios.plugin.responseSuccess',
        tag: 'axios.plugin.responseSuccess',
      };
        // 接收到响应发送一次日志
      addLogquery({ config, ...getLogParams(ctx, response) });
      // http status 为 200 不发送 lark, 可通过requestId 到 logquery 中查询
      return response;
    }, e => {
      // Any status codes that falls outside the range of 2xx cause this function to trigger
      // 发送日志
      const logConfig = {
        level: 'E',
        file: __filename,
        funcName: 'axios.plugin.responseError',
        tag: 'axios.plugin.responseError',
      };
      // 区分
      addLogquery({ config: logConfig, ...getLogParams(ctx, e.response) });
      // 发送 lark
      // sendErrorToFeishu(e);
      return Promise.reject(e);
    });

    await next();
  };
};
