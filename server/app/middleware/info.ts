import { Context } from 'egg';
import * as config from '../../../config';

// TODO
let ipData;

const getCurrencyByCountry = (ipData, accLang) => {
  let countryCode = ipData && ipData.country;
  if (!countryCode) {
    countryCode = ((accLang && accLang.split(',')[0]) || '').split(';')[0] || '';
  }
  return (config.SUPPORT_COUNTRY_CURRENCY_MAP[countryCode.toUpperCase] || {}).currency || 'USD';
};

const supportCurrency = Object.keys(config.SUPPORT_COUNTRY_CURRENCY_MAP).map(key => config.SUPPORT_COUNTRY_CURRENCY_MAP[key].currency);

function resetLangPath(ctx: Context) {
  ctx.klkLang = 'en';
  ctx.klkLangPath = '';

  Object.keys(config.SUPPORT_LANGS).forEach(lang => {
    const p = ctx.path;
    if (lang === 'en') {
      return;
    }
    // 其他语言的时候
    if (p.startsWith('/' + lang)) {
      ctx.klkLang = lang;
      ctx.klkLangPath = ctx.klkLang + '/';
      // 修改 path, 然后保存到 klkLang
      ctx.path = p.replace(ctx.klkLangPath, '');
    }
  });
}

export default () => {
  return async function(ctx: Context, next) {
    let needSetCurrencyCookie = false;
    if (!ctx.app.config.adsRenderRouter.includes(ctx.request.path)) {
      resetLangPath(ctx);
      // Supports X-Forwarded-Host when app.proxy is true, otherwise Host is used.
      // request.ip 在 app.proxy = true 的情况下，支持通过 X-Forwarded-For 传过来的 ip
      let ip = ctx.request.ip || '0.0.0.0';
      // localhost 拿到的是::1，后端认为是爬虫
      if (ip === '::1') {
        ip = '0.0.0.0';
      }

      ctx.remoteUserIp = ip;

      ctx.klkCurrency = (ctx.query._currency || '').toUpperCase() || ctx.cookies.get('klk_currency', { signed: false });
      if (supportCurrency.indexOf[ctx.klkCurrency] === -1) {
        const accLang = ctx.header['accept-language'];
        ctx.klkCurrency = getCurrencyByCountry(ipData, accLang);
        needSetCurrencyCookie = true;
      }

      if (ctx.cookies.get('klk_currency', { signed: false }) !== ctx.klkCurrency) {
        needSetCurrencyCookie = true;
      }

      ctx.userToken = ctx.cookies.get('sess', { signed: false });
      ctx.user_info = await ctx.service.user.get();
    }

    await next();

    if (ctx.status === 200 && needSetCurrencyCookie) {
      ctx.cookies.set('klk_currency', ctx.klkCurrency, {
        path: '/',
        httpOnly: false,
        signed: false,
      });
      ctx.cookies.set('_sync_currency', '1', {
        path: '/',
        httpOnly: false,
        signed: false,
      });
    }
  };
};
