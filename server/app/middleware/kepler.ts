import { Context } from 'egg';
const uuid = require('uuid');

export default () => {
  return async function(ctx: Context, next) {
    if (ctx.cookies.get('kepler_id', { signed: false })) {
      ctx.req.keplerId = ctx.cookies.get('kepler_id', { signed: false });
      // console.log('ctx.req.keplerId', ctx.req.keplerId)
    } else {
      ctx.req.keplerId = uuid.v4(); // uuid v4
      ctx.cookies.set('kepler_id', ctx.req.keplerId, {
        path: '/',
        expires: new Date(Date.now() + 2 * 365 * 24 * 3600 * 1000),
        httpOnly: false,
        sameSite: 'none',
        secure: ctx.protocol === 'https',
        signed: false,
      });
    }
    await next();
  };
};
