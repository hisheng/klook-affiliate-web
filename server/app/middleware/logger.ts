import { Context } from 'egg';

function getMethodByStatus(statusCode) {
  switch (true) {
    case statusCode >= 500:
      return 'error';
    case statusCode >= 400:
      return 'warn';
    case statusCode >= 300:
      return 'info';
    case statusCode >= 200:
      return 'info';
    default:
      return 'info';
  }
}

function print(context: Context, { format, method, type, addition }) {
  const { req, res, response, logquery } = context;

  const duration = Date.now() - req.startTime;
  let url = req.url || '';

  format = format.replace(/%requestId/g, req.requestId);
  format = format.replace(/%method/g, req.method || 'UNKNOW');
  format = format.replace(/%url/g, url);
  format = format.replace(/%status/g, String(res.statusCode));
  format = format.replace(/%elapsedTime/g, `${duration}ms`);

  const level = method[0].toUpperCase();
  const date = new Date().toISOString();

  if (logquery && context.app.config.env !== 'local') {
    let host = req.headers['x-klook-host'] || req.headers.host || '';
    host = host ? `https://${host}` : '';
    url = `${host}${url}`;

    const message = JSON.stringify({
      ...addition,
      action: 0,
      _page_: 'global_page',
      type,
      url,
      level: 'I',
      method: req.method,
      _duration_: duration,
      request_id: req.requestId,
      send_bytes: response.length || (req.socket || {}).bytesWritten || 0,
      received_bytes: response.length || (req.socket || {}).bytesRead || 0,
      business: 'klook-affiliate-front',
      description: format,
    });
    logquery.service({
      level,
      funcName: 'logger.affiliate.print',
      message,
      tag: 'Affiliate',
    });
  } else {
    console[method](`${date} - ${format}`);
  }
}

function printRequest(context: Context) {
  const { req } = context;
  const format = `<- ${req.requestId} ${req.method || 'UNKNOW'} %url`;
  const method = 'info';

  const addition = {
    send_bytes: 0,
  };

  print(context, { type: 'req', format, method, addition });
}

function printResponse(context: Context) {
  const { req, res } = context;
  const method = getMethodByStatus(res.statusCode);
  const format = `<- ${req.requestId} ${req.method || 'UNKNOW'} %url ${res.statusCode
  } %elapsedTime`;
  const addition = {
    http_status: res.statusCode,
    req_status: res.statusCode,
    method: req.method,
    _logger_name_: 'access_log:frontend:node:page',
  };
  print(context, { type: 'res', format, method, addition });
}

export default () => {
  return async function(ctx: Context, next) {
    ctx.req.startTime = Date.now();
    printRequest(ctx);

    ctx.res.on('finish', () => {
      printResponse(ctx);
    });

    await next();
  };
};
