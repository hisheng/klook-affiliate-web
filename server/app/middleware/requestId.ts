import { Context } from 'egg';
import { v4 as uuidv4 } from 'uuid';

export default () => {
  return async function(ctx: Context, next) {
    ctx.req.requestId = uuidv4().slice(0, 8);
    await next();
  };
};
