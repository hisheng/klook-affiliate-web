import { Context } from 'egg';
import TimeMarker from '../util/time-marker';

export default () => {
  return async (ctx: Context, next: () => Promise<any>) => {
    ctx.timeMarker = new TimeMarker();
    // 记录整个 render 时间
    ctx.timeMarker.start('render-all');
    await next();
    ctx.timeMarker.end('render-all');
    // set server-timing header
    ctx.timeMarker.setServerTiming(ctx);
  };
};
