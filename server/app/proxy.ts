import { Application } from 'egg';
const proxy = require('koa-proxies');

export default function(app: Application) {
  // local or local-prod
  if (!app.config.env.startsWith('local')) return;
  if (app.config.env === 'local') {
    // proxy web static
    app.use(proxy(/^\/static\/web/, {
      target: 'http://localhost:8786',
      changeOrigin: true,
      logs: true,
    }));
    // proxy widget static
    app.use(proxy(/^\/static\/widget/, {
      target: 'http://localhost:8785',
      changeOrigin: true,
      logs: true,
    }));
    /* app.use(proxy(/^\/v1\/affnode\//, {
      target: 'http://localhost:8785',
      changeOrigin: true,
      rewrite: path => path.replace('/v1/affnode', ''),
      logs: true,
    })); */
  }
  // proxy API
  app.use(proxy(/^\/(ajax|session|gen_report|gen_user_report|admin\/gen_user_report|jump|redirect|logout|v3\/affsrv(?!\/widget_template))/, {
    target: app.config.API_SERVER,
    changeOrigin: true,
    logs: true,
  }));
  app.use(proxy(/^\/s\/(widgets(?!\/banner[_v\d]*))/, {
    target: app.config.API_SERVER,
    changeOrigin: true,
    logs: true,
  }));
  app.use(proxy(/^\/(v1(?!\/affnode)|xos_api|v3\/userserv|v2\/usrcsrv)/, {
    target: app.config.API_SERVER,
    changeOrigin: true,
    logs: true,
  }));
}
