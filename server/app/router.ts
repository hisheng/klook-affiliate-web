import { Application } from 'egg';
import authMiddleware from './middleware/auth';
import * as config from '../../config';
import timeRecord from './middleware/time-record';

const renderMiddlewares = [ timeRecord() ];

const unloginRoutes = [
  '/',
  '/home',
  '/signup_auto',
  // '/activation_feedback',
  /^\/activation_feedback/,
  '/tools',
  '/help/terms_conditions',
  '/help/',
  '/contact_us',
  '/demo',
];

const loginRoutes = [
  '/dashboard',
  '/resendlink',
  '/my_ads',
  '/notification',
  '/change_website_name',
  '/my_ads/ad_tools',
  '/my_ads/ad_tools/:ad_tool_type',
  '/my_ads/co_brand',
  // '/performance',
  /^\/performance/,
  '/help/commission_rates',
  '/help/gift_card_activity',
  '/help/special_activity',
  '/my_account',
  '/information',
  // '/admin',
  /^\/admin/,
  '/product-list',
];

export default (app: Application) => {
  const { controller, router } = app;

  // ========== widget router START ==========
  router.get('/widget/test', ...renderMiddlewares, controller.widget.test);
  // ---------- 最新的 render url ----------
  router.get('/widget/:name', ...renderMiddlewares, controller.widget.index);
  // ---------- 兼容历史的 render url ----------
  router.get('/v1/affnode/render', ...renderMiddlewares, controller.widget.v1);
  router.get([ '/s/widgets/banner', '/s/widgets/banner_v2' ], ...renderMiddlewares, controller.widget.v2);
  router.get('/v3/affsrv/widget_template', ...renderMiddlewares, controller.widget.v3);
  // ---------- 兼容历史的 script url ----------
  // 对历史的 iframe 脚本请求进行重定向
  // 主要有以下几类 url:
  // 1. ${host}/s/dist/desktop/affiliate_base_v3.2.js => Static Banners, Activity Banners
  // 2. ${host}/s/dist/desktop/dynamic_widget_v3.js => Dynamic Widgets
  // 3. ${host}/s/dist/desktop/hotel_dynamic_widget_v3.js => Dynamic Widgets(hotel)
  // 4. ${host}/s/dist/desktop/search_vertical_v3.js => Search Box
  router.get('/s/dist/desktop/:script', controller.widget.redirectHistoryIframeScriptUrl);
  // ========== widget router END ==========

  // "/" => "/lang/home"
  router.get('/', async ctx => {
    const lang = ctx.cookies.get('locale', { signed: false });
    if (lang && lang !== 'en' && config.SUPPORT_LANGS_LIST.indexOf(lang) !== -1) {
      ctx.klkLang = lang;
      ctx.klkLangPath = lang + '/';
    }
    ctx.redirect(`/${ctx.klkLangPath}home`);
  });

  // routes need login
  router.get(loginRoutes, authMiddleware(), controller.home.index);

  // routes without login
  router.get(unloginRoutes, ...renderMiddlewares, controller.home.index);
};
