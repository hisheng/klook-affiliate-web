import { Service } from 'egg';
const util = require('../util');

/**
 * Common Service
 */
export default class Common extends Service {
  /**
   * get ranges
   */
  public async getRanges() {
    const cacheKey = 'comm:ranges_list:' + this.ctx.klkLang;
    const cacheData = util.memCache.get(cacheKey);
    if (cacheData) {
      return Promise.resolve(cacheData);
    }

    const headers = this.ctx.getAxiosHeaders();

    return this.ctx.$axios({
      url: '/v1/websrv/ranges',
      baseURL: this.ctx.app.config.WEB_SRV,
      headers,
    })
      .then(response => {
        if (response.data && response.data.success) {
          const ranges = response.data.result.ranges;
          util.memCache.set(cacheKey, ranges, 60 * 5);
          return ranges;
        }
        // return 'requestWithCache error';
      });
  }
}
