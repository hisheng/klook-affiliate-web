import { Service } from 'egg';
const createDOMPurify = require('dompurify');
const { JSDOM } = require('jsdom');

const window = new JSDOM('').window;
const DOMPurify = createDOMPurify(window);

/**
 * User Service
 */
export default class User extends Service {

  /**
   * get user info
   */
  public async get() {
    // 是否已登录
    const isLogin = this.ctx.cookies.get('sess', { signed: false }) && this.ctx.cookies.get('islogin', { signed: false });
    // 获取 userInfo
    let userInfo;
    if (isLogin) {
      const headers = this.ctx.getAxiosHeaders();
      await this.ctx.$axios({
        url: '/v3/affsrv/user',
        baseURL: this.ctx.app.config.AFFILIATE_URL,
        headers,
      })
        .then(res => res.data)
        .then(res => {
          if (res.success && res.result.user_info && res.result.user_info.id) {
            userInfo = res.result.user_info;
            const websites = userInfo.websites;
            // 对website字段做判断进行转义处理
            if (websites && Array.isArray(websites)) {
              websites.forEach(item => {
                for (const [ key, value ] of Object.entries(item)) {
                  if (typeof value === 'string') {
                    // 转义处理, dompurify 处理后值可能为 "http://abc</script><script>alert()</script><script>" -> "http://abc
                    item[key] = `${DOMPurify.sanitize(value)}`.split('"').join('');
                  }
                }
              });
            }
          } else {
            // 让sess失效
            this.ctx.cookies.set('sess', '', {
              path: '/',
              expires: new Date(Date.now() - 10000),
              signed: false,
            });
            // csrf-token 失效
            this.ctx.cookies.set('CSRF-TOKEN', '', {
              path: '/',
              expires: new Date(Date.now() - 10000),
              signed: false,
            });
          }
        });
    }

    if (!userInfo) {
      this.ctx.cookies.set('CSRF-TOKEN', '', {
        path: '/',
        expires: new Date(Date.now() - 10000),
        signed: false,
      });
      return {};
    }

    return userInfo;
  }
}
