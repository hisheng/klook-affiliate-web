// 统一错误处理
const process = require('process');
import { Application } from 'egg';
const { sendToFeishu } = require('../../../shared/send-feishu');

export default function(app: Application) {
  app.on('error', err => {
    console.log('Server error', err);
    if (app.config.env !== 'local') {
      const params = {
        config: {
          level: 'E',
          tag: 'affiliate.Node.Error',
        },
        requestId: '',
        message: err,
      };
      app.addLogquery(params);
      sendToFeishu({
        level: 'E',
        message: err,
        type: 'Error(Axios)',
      });
    }
  });

  process.on(
    'uncaughtException',
    err => {
      console.log('uncaughtException', err);
      sendToFeishu({
        level: 'E',
        message: err,
        type: 'uncaughtException',
      });
    },
  );

  process.on(
    'unhandledRejection',
    reason => {
      console.log('unhandledRejection', reason);
      sendToFeishu({
        level: 'E',
        message: reason,
        type: 'unhandledRejection',
      });
    },
  );

  process.on('warning', warning => {
    console.log('warning', warning);
    // warning 不上报，会出现很多错误信息
    // sendToFeishu({
    //     level: 'W',
    //     message: warning,
    //     type: 'warning',
    // });
  });
}
