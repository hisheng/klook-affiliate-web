import { Context } from 'egg';
import * as config from '../../../config';
const NodeCache = require('node-cache');

const formatToOptimus = params => {
  const { href, request_id, socket, url, status, method, _duration_ } = params;
  return {
    // type,
    url: href,
    level: 'I',
    action: 0,
    req_status: status + '',
    _page_: 'global_page',
    method: String(method || '').toLowerCase(),
    _duration_,
    request_id,
    send_bytes: (socket || {}).bytesWritten || 0,
    received_bytes: (socket || {}).bytesRead || 0,
    pageUrl: url || '',
    _logger_name_: 'access_log:frontend:node:api',
    business: 'klook-affiliate-front/axios',
  };
};

/**
 * 这个函数与浏览器端的代码一致，后续可以把两个地方的代码优化为一个
 * @param {*} key
 * @param {*} args
 */

// lang = en
const getCurrencyListByLang = (ctx: Context) => {
  return config.CURRENCY_LIST.map(currency => {
    const name = ctx.$t(`NODE_global.currency.${currency}`);
    return {
      name,
      value: currency,
      title: `${name} | ${config.CURRENCY_SYMBO_MAP[currency]}`,
    };
  });
};

/**
 * 根据堆栈的长度，截断多余的堆栈信息
 * @param  {String} stack  原始堆栈
 * @param  {Integer} maxDepth 最大堆栈深度
 * @return {String}        截断后的堆栈
 */
function subStack(stack, maxDepth) {
  if (!stack) {
    return null;
  }

  const arr = stack.toString().split('\n');
  const stackDepth = arr.length;
  if (stackDepth > maxDepth) {
    return arr.slice(0, maxDepth).join('\n');
  }
  return stack;
}

/*
function devProfile() {
    const v8Profiler = require('v8-profiler-next');
    const title = 'dev-affiliate-cpu-profiling'
    console.log('start cpu profiling...')
    v8Profiler.startProfiling(title, true);
    setTimeout(() => {
        const profile = v8Profiler.stopProfiling(title);

        profile.export(function (error, result) {
            // if it doesn't have the extension .cpuprofile then
            // chrome's profiler tool won't like it.
            // examine the profile:
            //   Navigate to chrome://inspect
            //   Click Open dedicated DevTools for Node
            //   Select the profiler tab
            //   Load your file
            fs.writeFileSync(`${title}.cpuprofile`, result);
            profile.delete();
            console.log('end cpu profiling...')
        });
    }, 10*1000);
} */

const memCache = new NodeCache();

export {
  // devProfile,
  memCache,
  getCurrencyListByLang,
  subStack,
  formatToOptimus,
};
