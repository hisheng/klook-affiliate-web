import { Context } from 'egg';

export default class TimeMarker {
  marks = {};
  start(key: string) {
    if (this.marks[key]) return;
    // start time
    this.marks[key] = [ +(new Date()) ];
  }
  end(key: string) {
    if (this.marks[key] && this.marks[key].length) {
      // end time
      this.marks[key].push(+(new Date()));
    }
  }
  setServerTiming(ctx: Context) {
    const serverTiming: string[] = [];
    Object.keys(this.marks).forEach(key => {
      const [ startTime, endTime ] = this.marks[key];
      if (!(startTime && endTime)) return;
      serverTiming.push(`${key};dur=${endTime - startTime}`);
    });
    ctx.set('server-timing', serverTiming.join(','));
  }
}
