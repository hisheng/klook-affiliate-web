import { EggAppConfig, EggAppInfo, PowerPartial } from 'egg';
const path = require('path');

export default (appInfo: EggAppInfo) => {
  const config = {} as PowerPartial<EggAppConfig>;

  // override config from framework / plugin
  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_affiliate_';

  // add your egg config in here
  config.middleware = [
    'requestId',
    'kepler',
    'logger',
    'axios',
    'info',
  ];

  // add your special config in here
  const bizConfig = {
    sourceUrl: `https://github.com/eggjs/examples/tree/master/${appInfo.name}`,
  };

  config.view = {
    root: [ path.join(__dirname, '../app/views'), path.join(__dirname, '../app/public') ].join(','),
    mapping: {
      '.njk': 'nunjucks',
      '.html': 'nunjucks',
    },
    defaultExtension: '.njk',
    defaultViewEngine: 'nunjucks',
  };

  // TODO: csrf 报错，暂时 disabled
  config.security = {
    csrf: {
      enable: false,
    },
  };

  // logquery
  config.logquery = {
    url: 'nats://nlb-nats.dev.klook.io:6222',
  };

  config.adsRenderRouter = [ '/v3/affsrv/widget_template', '/v1/affnode/render' ];

  config.static = {
    prefix: '/static/',
  };

  config.widgetTestParams = {
    host: 'http://localhost:8787',
    klk_aff_host: '//localhost:8787',
    dw_ttd: '108503',
    static_dw_ttd: '108218',
    hotel_dw: '108204',
    hotel_static_dw: '108221',
    auto_dw_ttd: '9043',
    auto_dw_hotel: '12471',
    act_banner: '108208',
    static_banner: '108213',
    searchbox: '108210',
    search_banner: '11',
    isDev: true,
  };
  /* config.widgetTestParams = {
    host: '',
    klk_aff_host: '//affiliate.klook.com',
    dw_ttd: '548576',
    static_dw_ttd: '548577',
    hotel_dw: '548578',
    hotel_static_dw: '548579',
    auto_dw_ttd: '12471',
    auto_dw_hotel: '12471',
    act_banner: '548580',
    static_banner: '548581',
    searchbox: '10572',
    search_banner: '100376',
    isDev: false,
  }; */

  // the return config will combines to EggAppConfig
  return {
    // Supports X-Forwarded-For when app.proxy is true
    proxy: true,
    ipHeaders: 'X-Forwarded-For',
    ...config,
    ...bizConfig,
  };
};
