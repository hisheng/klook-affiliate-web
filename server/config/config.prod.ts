import { EggAppConfig, PowerPartial } from 'egg';

export default () => {
  const config: PowerPartial<EggAppConfig> = {};

  const API_SERVER = 'https://affiliate.klook.com/';
  config.API_SERVER = API_SERVER;
  config.WEB_SRV = 'http://websrv.klook-appapi:8080';
  config.AFFILIATE_URL = 'http://affsrv.klook-partnership:8080';

  // logquery
  config.logquery = {
    url: 'nats://nlb-nats.klook.io:6222',
  };

  config.widgetTestParams = {
    host: '',
    klk_aff_host: '//affiliate.klook.com',
    dw_ttd: '548576',
    static_dw_ttd: '548577',
    hotel_dw: '548578',
    hotel_static_dw: '548579',
    auto_dw_ttd: '12471',
    auto_dw_hotel: '12471',
    act_banner: '548580',
    static_banner: '548581',
    searchbox: '10572',
    search_banner: '100376',
    isDev: false,
  };

  return config;
};
