import { EggAppConfig, PowerPartial } from 'egg';

export default () => {
  const config: PowerPartial<EggAppConfig> = {};

  const API_SERVER = 'http://affiliate43.fat.klook.io/';
  config.API_SERVER = API_SERVER;
  config.WEB_SRV = API_SERVER;
  config.AFFILIATE_URL = API_SERVER;

  // logquery
  config.logquery = {
    url: 'nats://nlb-nats.dev.klook.io:6222',
  };

  return config;
};
