// This file is created by egg-ts-helper@1.26.0
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportAuth from '../../../app/middleware/auth';
import ExportAxios from '../../../app/middleware/axios';
import ExportInfo from '../../../app/middleware/info';
import ExportKepler from '../../../app/middleware/kepler';
import ExportLogger from '../../../app/middleware/logger';
import ExportRequestId from '../../../app/middleware/requestId';
import ExportTimeRecord from '../../../app/middleware/time-record';

declare module 'egg' {
  interface IMiddleware {
    auth: typeof ExportAuth;
    axios: typeof ExportAxios;
    info: typeof ExportInfo;
    kepler: typeof ExportKepler;
    logger: typeof ExportLogger;
    requestId: typeof ExportRequestId;
    timeRecord: typeof ExportTimeRecord;
  }
}
