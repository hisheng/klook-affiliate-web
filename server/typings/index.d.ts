import 'egg';
import { IncomingMessage as _IncomingMessage, ServerResponse as _ServerResponse } from 'http';

declare module 'egg' {
  interface IncomingMessage extends _IncomingMessage {
    keplerId: string
    requestId: string
    startTime: number
  }
  interface ServerResponse extends _ServerResponse {
    //
  }
  interface LogqueryParams {
    config: any;
    requestId: string;
    message: any;
  }
  interface Application {
    logquery: any
    addLogquery: (params: LogqueryParams) => void;
  }
}
