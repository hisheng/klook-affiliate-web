import Logquery from '@klook/logquery';

/**
 * 日志上报到
 * https://logquery.klook.io/v1/logqueryserv/#/  https://logquery.dev.klook.io/v1/logqueryserv/#/
 */
const defaultOptions = {
  url:
    process.env.NODE_ENV === 'production'
      ? 'https://log.klook.com/v2/frontlogsrv/log/web'
      : 'https://dev-frontsrv-new.dev.klook.io/v2/frontlogsrv/log/web',
  headers: { 'X-Platform': 'desktop' },
  queue: {
    interval: 5000,
    size: 15,
  },
};

const logquery = new Logquery(defaultOptions);

export default class Logger {
  /**
   * @param {*} error
   */
  constructor() {
  }

  handlerError(error, customInfo) {
    error = { ...error, customInfo };
    logquery.service({
      timestamp: Date.now(),
      level: customInfo.level || 'E',
      message: JSON.stringify(error),
      tag: 'affiliate/Error',
    });
  }


  uploadOptimus(logObj, options) {
    logquery.optimus(logObj, options);
  }

  pushQuuen() {
    logquery.pushQueue();
  }
}
