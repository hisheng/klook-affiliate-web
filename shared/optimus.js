import querystring from 'querystring';
import Logger from "./logquery";
import { getCLS, getLCP } from 'web-vitals';

const vitalsData = {};

function recordSpecialData({ name, value }) {
  if (value) {
    vitalsData[name.toLowerCase()] = +Number(value).toFixed(3);
  }
}

/**
 * record lcp and cls time
 */
export function trackSpecial() {
  // getFID(recordSpecialData)
  const lcpPromise = new Promise(resolve => {
    getLCP(metric => {
      recordSpecialData(metric);
      resolve();
    }, true);
  });
  const clsPromise = new Promise(resolve => {
    getCLS(metric => {
      recordSpecialData(metric);
      resolve();
    }, true);
  });
  return Promise.all([lcpPromise, clsPromise]).catch(() => { });
}


const getContentLength = data => {
  const m = encodeURIComponent(data).match(/%[89ABab]/g);
  return (data ? data : '').length + (m ? m.length : 0);
};
const logger = new Logger();
/**
 * 追踪页面渲染成功
 * @param routerName
 * @param pathname
 */
async function trackPageRenderSuccess({ domain, url, pathname }) {
  const { timing } = window.performance;
  if (!pathname) {
    return;
  }
  if (timing.loadEventEnd <= 0) {
    const timer = window.setTimeout(() => {
      window.clearTimeout(timer);
      trackPageRenderSuccess({ domain, url, pathname });
    }, 1000);
    return;
  }
  if (Object.keys(vitalsData).length < 2) {
    await trackSpecial();
  }
  console.log({ domain, url, pathname });
  const logObj = {
    timestamp: Date.now(),
    level: 'I',
    message: {
      _domain_: domain,
      _logger_name_: 'access_log:frontend:web',
      _page_: pathname,
      _duration_: timing.loadEventEnd - timing.navigationStart,
      _succ_: true,
      _url_: url,
      _code_: 200,
      _error_: '',
      network: 'NA',
      upstream: 0,
      downstream: 0,
      redirect_dt: timing.redirectEnd - timing.redirectStart, // 重定向耗时
      dns_dt: timing.domainLookupEnd - timing.domainLookupStart, // DNS 查询耗时
      tcp_dt: timing.connectEnd - timing.connectStart, // TCP 连接耗时
      ttfb: timing.responseStart - timing.navigationStart, // 首字节返回耗时
      req_dt: timing.responseEnd - timing.requestStart, // 请求耗时
      analysis_dt: timing.responseEnd - timing.navigationStart, // 解析 DOM 树耗时
      blank_dt: timing.domLoading - timing.fetchStart, // 白屏耗时
      domready_dt: timing.domComplete - timing.domLoading, // DOMReady 耗时
      firstcp_dt: timing.loadEventEnd - timing.navigationStart, // 首屏渲染耗时
      ...vitalsData, // 添加lcp, cls 时间统计
    },
  };
  logger.uploadOptimus(logObj);
  setTimeout(() => logger.pushQuuen(), 0); // @NOTE: 页面加载完成，立即上报，防止漏报
}

// 追踪接口请求成功
function trackRequestSuccess(response, param) {
  let pathName = window.location.pathname;
  setTimeout(() => {
    try {
      const [url, query = ''] = param.url.split('?');
      const error = (response.error) || {};
      const logObj = {
        timestamp: Date.now(),
        level: 'I',
        message: {
          _domain_: window.location.host,
          _logger_name_: 'access_log:frontend_http_client',
          _url_: url,
          _page_: pathName || 'global_page',
          _duration_: Date.now() - param.startTime,
          _succ_: true,
          _code_: error.code || '',
          _error_: error.message || '',
          http_rsp_code: response.status,
          network: 'NA',
          upstream: param.data
            ? getContentLength(JSON.stringify(param.data))
            : 0,
          downstream: getContentLength(JSON.stringify(response.data)),
          arguments:
            querystring.stringify(param.param) +
            (query ? '&' + query : ''),
        },
      };
      logger.uploadOptimus(logObj, {
        headers: { 'X-Platform': 'desktop' },
      });
    } catch (error) {
      console.log(error);
    }
  }, 0);
}

// 追踪接口请求失败
function trackRequestError(error, params) {
  let pathName = window.location.pathname;
  setTimeout(() => {
    try {
      const [url, query = ''] = params.url.split('?');
      const logObj = {
        timestamp: Date.now(),
        level: 'I',
        message: {
          _logger_name_: 'access_log:frontend_http_client',
          _url_: url,
          _page_: pathName || 'global_page',
          _duration_: Date.now() - params.startTime,
          _succ_: false,
          _code_: '',
          _error_: error || '',
          http_rsp_code: (error && error.status) || 400,
          network: 'NA',
          upstream: 0,
          downstream: 0,
          arguments:
            querystring.stringify(params) +
            (query ? '&' + query : ''),
        },
      };
      logger.uploadOptimus(logObj, {
        headers: { 'X-Platform': 'desktop' },
      });
    } catch (error) {
      console.log('track api error:', error);
    }
  }, 0);
}

export { trackRequestSuccess, trackRequestError, trackPageRenderSuccess };
