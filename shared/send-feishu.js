const axios = require('axios');
const { sep } = require('path');

const MAX_STACK_DEPTH = 8;

function parseStack(stack) {
  const cwd = process.cwd() + sep;

  const lines = stack
    .split('\n')
    .splice(1)
    .slice(0, MAX_STACK_DEPTH)
    .map(l =>
      l
        .trim()
        .replace('file://', '')
        .replace(cwd, ''),
    );

  return lines.join('\n');
}

function getType(obj) {
  return Object.prototype.toString
    .call(obj)
    .slice(8, -1)
    .toLowerCase();
}

// check if a given error is a request error
function isRequestError(err) {
  return (
    (getType(err) === 'error' || err instanceof Error) &&
    err.request !== undefined &&
    err.config !== undefined &&
    err.isAxiosError
  );
}

function setRequestInfo(log, err) {
  const reqHeaders = err.config.headers;
  if (reqHeaders) {
    // token is sensitive
    if (reqHeaders.token) {
      reqHeaders.token = reqHeaders.token.replace(
        /^(.{4})(.+)$/,
        function (s, s1, s2) {
          return s1 + '*'.repeat(s2.length);
        },
      );
    }
    // remove cookie
    if (reqHeaders.cookie) delete reqHeaders.cookie;
  }
  // set request id
  if (err.response) {
    log.requestId = err.response.headers['x-klook-request-id'];
  }
  const reqInfo = {
    config: {
      url: err.config.url,
      method: err.config.method,
      timeout: err.config.timeout,
      headers: reqHeaders,
    },
    request: err.request
      ? {
        status: err.request.status,
        statusText: err.request.statusText,
        timeout: err.request.timeout,
        readyState: err.request.readyState,
      }
      : '',
    response: err.response
      ? {
        headers: err.response.headers,
      }
      : '',
  };
  log.requestInfo = JSON.stringify(reqInfo, null, 2);
}

const isNode =
  typeof process !== 'undefined' &&
  Object.prototype.toString.call(process) === '[object process]';

function nodeLogFormat(log) {
  return {
    ...log,
    versions: {
      node: process.versions.node,
      v8: process.versions.v8,
    },
  };
}

function browserLogFormat(log) {
  return {
    href: location.href,
    referer: document.referrer,
    agent: navigator.userAgent,
    ...log,
  };
}

function formatObj(obj) {
  return JSON.stringify(obj, null, 2);
}

function sendToFeishu(log, url) {
  if (!(['prd', 'production'].includes(process.env.NODE_ENV))) {
    return;
  }
  log.timestamp = log.timestamp || Date.now();
  log.localTime = new Date(log.timestamp).toLocaleString();
  log.project = 'affiliate';
  log.level = log.level || 'E';
  const { message } = log;
  log.env = process ? process.env.NODE_ENV : '';
  // set stack
  const isError =
    message instanceof Error || getType(message) === 'error';

  if (isError) {
    log.stack = parseStack((message).stack);
  }

  // set request headers
  if (isRequestError(message)) {
    setRequestInfo(log, message);
  }

  // transform message to string
  let $message = '';
  if (isError) {
    $message = message.message;
  } else if (getType(message) === 'object') {
    $message = JSON.stringify(message);
  } else if (getType(message) === 'string') {
    $message = message;
  }
  log.message = $message;
  const comingLogs = isNode ? nodeLogFormat(log) : browserLogFormat(log);
  let feishu_url = url || 'https://open.larksuite.com/open-apis/bot/v2/hook/dcbf4887-df88-4fd4-bea2-0d2e261a7ebb';
  const attachments = Object.keys(comingLogs).map(key => {
    return [
      {
        tag: 'text',
        text: `${key.replace(/^\S/, s => s.toUpperCase())} : ${getType(comingLogs[key]) === 'object'
            ? formatObj(comingLogs[key])
            : comingLogs[key]
          }`,
      },
    ];
  });
  return axios.post(feishu_url, {
    msg_type: 'post',
    content: {
      post: {
        zh_cn: {
          title: 'affiliate错误提醒',
          content: attachments,
        },
      },
    },
  });
}

module.exports = {
  sendToFeishu
};
