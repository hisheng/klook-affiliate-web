#!/bin/bash

# abort on errors
set -e

function_copy(){
    rsync -r --exclude='node_modules' ./ $target_dir
    if [ "$?" = "1" ]; then
        echo "cp error and exit"
        exit 1
    fi
}

function_git_status(){
    echo "------ Last Commit: -------"
    git status | head -1
    git show -s --date=relative | head -3
    echo "-------------------------------"
}

function_checkout_branch(){
    git checkout -- .
    git fetch
    git checkout $branch
    if [ "$?" = "1" ]; then
        echo "checkout error and exit"
        exit 1
    fi
}

function_build(){
    npm postinstall
    basepath=$(cd `dirname $0`; pwd)
    echo "-------------------------------路径"
    echo "[$basepath]"
    npm run build
    function_copy
    time_now=`date "+%Y-%m-%d %H:%M:%S"`
    commit_hash=`git log --pretty=oneline | head -n 1`
    echo "[$time_now] [build] branch=> $branch, hash => $commit_hash success " >> /tmp/.klook_affiliate_log
}

function_deploy(){

    deployDir=/srv/klook-affiliate-front

    if [ ! -d "$deployDir" ]; then
        mkdir -p $deployDir
    fi

    cd $deployDir
    # sync file from build folder to deploy folder
    echo -e "\033[32msync the dist from build to deploy.\033[0m"
    # rm -rf s/dist/
    echo "delete old dist"
    rsync -rvpog --checksum  /opt/release/klook-affiliate-front/ $deployDir

    npm install --production
    #    if [ "$(pm2 id klook-affiliate)" = "[]" ]; then
    #        NODE_ENV=$node_env pm2 start ./server/index.js --name klook-affiliate --interpreter=node@12.18.3 --output /root/.pm2/logs/klook-affiliate.log --error /root/.pm2/logs/klook-affiliate.log --log-date-format "YYYY-MM-DD HH:mm:ss.SSS"
    #    else
    #        NODE_ENV=$node_env pm2 restart klook-affiliate
    #    fi
    pm2 delete ./pm2.config.js
    pm2 start ./pm2.config.js

    time_now=`date "+%Y-%m-%d %H:%M:%S"`
    commit_hash=`git log --pretty=oneline | head -n 1`
    echo "[$time_now] [deploy] branch => $branch, node_env => $node_env , hash => $commit_hash success " >> /srv/builds/.klook_affiliate_log
}

function_change_node_version(){
    # use nvm
    . ~/.nvm/nvm.sh

    if [ "$NODE_ENV" = "test" ]; then
        node_env=$NODE_ENV
    fi

    echo "当前NODE_ENV"
    echo $node_env

    echo "当前npm版本"
    npm -v

    echo "当前nvm版本"
    nvm --version

    test -n "$NODE_VERSION" || NODE_VERSION="v12.18.3"
    nvm use $NODE_VERSION || nvm install $NODE_VERSION

    echo "当前node版本"
    node -v
}

cmd=$1 # build deploy
target_dir=$2 # test env dir
branch=$3 # branch name
node_env='test'

if [ -z "$branch" ]; then
    echo "\033[31mthe branch or tag or commit is null that default origin/master.\033[0m"
    branch='origin/master'
fi

function_change_node_version

if [ "$cmd" = "build" ]; then
    function_checkout_branch
    function_build
elif [ "$cmd" = "deploy" ]; then
    function_deploy
fi

function_git_status
