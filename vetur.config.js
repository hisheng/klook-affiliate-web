module.exports = {
  projects: [
    {
      root: './web',
      tsconfig: "./tsconfig.json"
    },
    {
      root: './widgets',
      tsconfig: "./tsconfig.json"
    },
  ]
};