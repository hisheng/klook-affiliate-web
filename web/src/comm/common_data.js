import urlObj from "./url";
import formRules from "./form_rules";
import klook from "./core";
import { __ } from "./utils";
const requestWithCache = (function () {
  var result = {};
  return function (url, cacheKey) {
    var cacheKey = cacheKey || url;
    return new Promise(function (resolve) {
      if (result[cacheKey]) {
        resolve(result[cacheKey]);
      } else {
        klook.ajaxGet(url, (resp) => {
          if (resp.success) {
            result[cacheKey] = resp.result;
            resolve(result[cacheKey]);
          } else {
            resolve(resp);
          }
        });
      }
    });
  };
})();

const SOURCE_TYPE_MAP = () => {
  return {
    0: __("COMMON_DATA_source_type.search_engine"),
    1: __("COMMON_DATA_source_type.klook_website"),
    2: __("COMMON_DATA_source_type.travel_websites"),
    3: __("COMMON_DATA_source_type.facebook"),
    4: __("COMMON_DATA_source_type.instagram"),
    5: __("COMMON_DATA_source_type.affiliate_network"),
    6: __("COMMON_DATA_source_type.recommendation_from_online_articles"),
    7: __("COMMON_DATA_source_type.recommendation_from_a_friend"),
    8: __("COMMON_DATA_source_type.contacted_by_klook_affiliates_team"),
    "-1": __("COMMON_DATA_source_type.other"),
  };
};

const WEBSITE_TYPE_MAP = () => [
  {
    value: "13",
    name: __("COMMON_DATA_sitetype.affiliate_network"),
  },
  {
    value: "1",
    name: __("COMMON_DATA_sitetype.airline"),
  },
  { value: "6", name: __("COMMON_DATA_sitetype.coupon") },
  { value: "7", name: __("COMMON_DATA_sitetype.ecommerce") },
  { value: "11", name: __("COMMON_DATA_sitetype.finance") },
  { value: "5", name: __("COMMON_DATA_sitetype.hotel") },
  { value: "-1", name: __("COMMON_DATA_sitetype.influencer") },
  { value: "8", name: __("COMMON_DATA_sitetype.membership") },
  { value: "4", name: __("COMMON_DATA_sitetype.news") },
  { value: "10", name: __("COMMON_DATA_sitetype.discussion") },
  { value: "3", name: __("COMMON_DATA_sitetype.ota") },
  { value: "9", name: __("COMMON_DATA_sitetype.telecom") },
  { value: "12", name: __("COMMON_DATA_sitetype.tourism") },
  { value: "0", name: __("COMMON_DATA_sitetype.blog") },
  { value: "2", name: __("COMMON_DATA_sitetype.trip_planning_tool") },
];

const UV_LEVEL_MAP = {
  1: "1-1000",
  2: "1,001-10,000",
  3: "10,001-100,000",
  4: "100,001-500,000",
  5: "500,001-1,000,000",
  6: "1,000,000+",
};

const AJAX_ERROR_CODE_OBJ = {
  success: 0,
  user_not_logged_in: -10001,
  param_invalid: -10002,
  user_already_exist: -10003,
  cant_find_user: -10004,
  need_auth: -10005,
  user_not_authed: -10006,
  auth_fail: -10007,
  admin_required: -10008,
  register_fail: -10009,
  ip_exceed_limit: -10010,
  email_exceed_limit: -10011,
  user_not_admin: -10012,
  user_declined: -10013,
};

// {
//    "city_id": 2,
//    "city_name": "Hong Kong",
//    "city_seo": "hong-kong"
// },
//range => areas => countries => cities
/*
    防止基础服务挂导致 RANGES = undefined 报错页面显示不出来
 */
window.RANGES = (window.RANGES && RANGES) || [];
const CITY_LIST = RANGES.reduce((res, cur) => {
  return res.concat(
    cur.countries.reduce((res2, cur2) => res2.concat(cur2.cities), [])
  );
}, []);

const COUNTRY_LIST = RANGES.reduce((res, cur) => {
  return res.concat(cur.countries);
}, []);

const ID_COUNTRY_MAP = {};
const ID_CITY_MAP = {};
RANGES.forEach((destination) => {
  destination.countries.forEach((country) => {
    ID_COUNTRY_MAP[country.country_id] = country;
    country.cities.forEach((city) => {
      ID_CITY_MAP[city.city_id] = city;
    });
  });
});

const CDN_PREFIX = "https://cdn.klook.com/s/dist_web/klook-affiliate-front";

// TODO:
var script_url_prefix = CDN_PREFIX;
// var script_url_prefix =
//   window.KLK_ENV && window.KLK_ENV == "prd"
//     ? CDN_PREFIX
//     : "//" + window.location.host;
var script_content_prefix = `(function(d, sc, u) { var s = d.createElement(sc), p = d.getElementsByTagName(sc)[0]; s.type = 'text/javascript'; s.async = true; s.src = u; p.parentNode.insertBefore(s,p); })(document, 'script', '${script_url_prefix}/s/dist/desktop/`;
const AD_SCRIPT = (function () {
  return script_content_prefix + `affiliate_base_v3.2.js')`;
})();

// 定义 widget 脚本类型， 传入 vertical 名称 和 对应 iframe 版本号
const AD_SCRIPT_WIDGET_TEMPLATE = (vertical, version) => {
  return `${script_content_prefix}${vertical}_v${version}.js')`;
};

const AD_SCRIPT_SEARCH_VERTICAL_TEMPLATE = function (version) {
  return script_content_prefix + `search_vertical_v${version}.js')`;
};

const AD_TYPE_LIST = [
  "link",
  "search",
  "activity",
  "banner",
  "code",
  "dynamic_widget",
  "static_widget",
];

const currencyDecimalplace = {
  CHF: 2,
  SGD: 2,
  AUD: 2,
  JOD: 2,
  USD: 2,
  EUR: 2,
  GBP: 2,
  MYR: 2,
  OMR: 2,
  NZD: 2,
  CAD: 2,
};

// payment_admin.vue 接口返回多语言字段的映射
const PAYMENTS_KEY_MAP = () => {
  return {
    alipay_id: __("COMMON_DATA_alipay_id"),
    country_code: __("MULTIPLE_country_code"),
    bank_name_branch_name: __("MULTIPLE_bank_name_branch_name"),
    bank_name: __("MULTIPLE_bank_name"),
    branch_code: __("MULTIPLE_branch_code"),
    bank_account_name: __("MULTIPLE_account_name"),
    bank_account_id: __("MULTIPLE_account_number"),
    swift_code: __("MULTIPLE_swift_code"),
    iban_number: __("MULTIPLE_iban_number"),
    sort_code: __("MULTIPLE_sort_code"),
    phone_number: __("MULTIPLE_phone_number"),
    paypal_id: __("COMMON_DATA_paypal_id"),
    bank_swift_code: __("COMMON_DATA_bank_swift_code"),
    bank_other_info: __("COMMON_DATA_bank_other_info"),
    bank_code: __("MULTIPLE_bank_code"),
    bsb_code: __("MULTIPLE_bsb_code"),
    ifsc_code: __("MULTIPLE_ifsc_code"),
    routing_code: __("MULTIPLE_routing_code"),
  };
};

function generateActivityUrlByActivityId(activity_id) {
  var lang_path = KLK_LANG_PATH;

  return `https://www.klook.com/${lang_path}activity/${activity_id}-/`;
}

// affiliate type 五种类型, admin->affiliate admin模块下使用
const AFFILIATE_TYPE = () => {
  return {
    head: __("COMMON_DATA_affiliate_type_head"),
    mid: __("COMMON_DATA_affiliate_type_mid"),
    long: __("COMMON_DATA_affiliate_type_long"),
    key: __("COMMON_DATA_affiliate_type_key"),
    network: __("COMMON_DATA_affiliate_type_network"),
  };
};

// admin_edit_settings_dialog.vue. commission_model的映射
const COMMISSION_MODEL = () => {
  return {
    affiliate: __("COMMON_DATA_affiliate"),
    tracking_only: __("COMMON_DATA_tracking_only"),
  };
};

// GTM event tracking 埋点上报
const WIDGET_MAP_GOOGLE_TRACKING = {
  dynamicWidgets: "Dynamic Widgets",
  link: "Text Links",
  search: "Search Box",
  search_vertical: "Search Box",
  banner: "Static Banners",
  code: "Promo Code",
  activity: "Activity Banners",
  co_brand: "Co-brand",
};

// render广告页面 多语言映射
const RENDER_LANGUAGE_MAP = {
  "en-US": {
    activity_reviews: "Reviews",
    view_more: "View More",
    search: "Search",
    search_placeholder: "Search by destination or activity",
    search_title: "Discover and book amazing things to do at exclusive prices",
    banner_book_now: "BOOK NOW",
    discount: "%s% OFF",
  },
  "zh-CN": {
    activity_reviews: "评价",
    view_more: "查看更多",
    search: "搜索",
    search_placeholder: "输入目的地/景点/活动",
    search_title: "发现更好玩的世界，预订独一无二的旅行体验",
    banner_book_now: "立即预订",
    discount: "%s折",
  },
  "zh-TW": {
    activity_reviews: "評價",
    view_more: "查看更多",
    search: "搜索",
    search_placeholder: "輸入目的地/景點/活動",
    search_title: "發現更好玩的世界，預訂獨一無二的旅行體驗",
    banner_book_now: "立即預訂",
    discount: "%s折",
  },
  "zh-HK": {
    activity_reviews: "評論",
    view_more: "查看更多",
    search: "搜索",
    search_placeholder: "輸入目的地/景點/活動",
    search_title: "發現更好玩的世界，預訂獨一無二的旅行體驗",
    banner_book_now: "立即預訂",
    discount: "%s折",
  },
  ko: {
    activity_reviews: "리뷰",
    view_more: "더 보기",
    search: "검색",
    search_placeholder: "도시 또는 액티비티 검색",
    search_title: "더 넓은 세상을 경험하고, 잊지 못할 순간을 예약하세요.",
    banner_book_now: "지금 예약하기",
    discount: "%s% 할인",
  },
  th: {
    activity_reviews: "ความคิดเห็น",
    view_more: "ดูเพิ่มเติม",
    search: "ค้นหา",
    search_placeholder:
      "ค้นหาโดยใช้จุดหมายปลายทางหรือกิจกรรม ค้นหาด้วยจุดหมายปลายทางหรือกิจกรรม",
    search_title: "สำรวจและจองกิจกรรมที่น่าสนใจมากมายในราคาสุดพิเศษ",
    banner_book_now: "จองตอนนี้",
    discount: "ลด %s%",
  },
  vi: {
    activity_reviews: "đánh giá",
    view_more: "Xem thêm",
    search: "Tìm kiếm",
    search_placeholder: "Tìm theo điểm đến hoặc hoạt động",
    search_title:
      "Khám phá và đặt trước các hoạt động du lịch đặc sắc với giá độc quyền",
    banner_book_now: "Đặt ngay",
    discount: "GIÁM GIÁ %s%",
  },
  id: {
    activity_reviews: "ulasan",
    view_more: "Lihat Selengkapnya",
    search: "Cari",
    search_placeholder: "Cari berdasarkan destinasi atau aktivitas",
    search_title:
      "Temukan dan pesanlah aktivitas seru dengan harga yang eksklusif",
    banner_book_now: "PESAN SEKARANG",
    discount: "DISKON %s%",
  },
  ja: {
    activity_reviews: "件のクチコミ",
    view_more: "もっと見る",
    search: "検索",
    search_placeholder: "目的地・アクティビティから検索",
    search_title:
      "現地のワクワクを見つける手がかりに。旅を思いのままにアレンジしよう。",
    banner_book_now: "詳細を見る",
    discount: "%s% OFF",
  },
  fr: {
    activity_reviews: "La revue",
    view_more: "voir plus",
    search: "Search",
    search_placeholder: "Search by destination or activity",
    search_title: "Réservez des activités inoubliables aux meilleurs prix",
    banner_book_now: "BOOK NOW",
    discount: "%s% OFF",
  },
  de: {
    activity_reviews: "Rezension",
    view_more: "Mehr sehen",
    search: "Search",
    search_placeholder: "Search by destination or activity",
    search_title:
      "Entdecke und buche einzigartige Aktivitäten zu exklusiven Preisen",
    banner_book_now: "BOOK NOW",
    discount: "%s% OFF",
  },
  es: {
    activity_reviews: "Revisión",
    view_more: "ver más",
    search: "Search",
    search_placeholder: "Search by destination or activity",
    search_title: "Scopri e prenota attività straordinarie a prezzi esclusivi",
    banner_book_now: "BOOK NOW",
    discount: "%s% OFF",
  },
  ru: {
    activity_reviews: "отзыв",
    view_more: "узнать больше",
    search: "Search",
    search_placeholder: "Search by destination or activity",
    search_title:
      "Находите удивительные варианты досуга и бронируйте их по эксклюзивным ценам",
    banner_book_now: "BOOK NOW",
    discount: "%s% OFF",
  },
};

// searchBox 渲染页面埋点关系表
// id: 埋点数据
let common_obj = {
  event: "adDisplay",
  eventCategory: "My Ads",
  eventAction: "Partner Website",
  eventLabel: "Search Box",
};
const SEARCH_VERTICAL_FILLING_POINT_MAP = {
  11: {
    ...common_obj,
    verticalName: "Attractions & Shows",
  },
  12: {
    ...common_obj,
    verticalName: "Tours & Sightseeing",
  },
  13: {
    ...common_obj,
    verticalName: "Activities & Experiences",
  },
  14: {
    ...common_obj,
    verticalName: "Best Food & Must Eats",
  },
  21: {
    ...common_obj,
    verticalName: "JR Pass",
  },
  22: {
    ...common_obj,
    verticalName: "Europe Train",
  },
  23: {
    ...common_obj,
    verticalName: "China Train",
  },
  24: {
    ...common_obj,
    verticalName: "Taiwan Train",
  },
  25: {
    ...common_obj,
    verticalName: "Vietnam Train",
  },
  31: {
    ...common_obj,
    verticalName: "WiFi & SIM Card",
  },
  41: {
    ...common_obj,
    verticalName: "Transport & Travel Services",
  },
};

export {
  WEBSITE_TYPE_MAP,
  urlObj,
  formRules,
  CITY_LIST,
  ID_CITY_MAP,
  COUNTRY_LIST,
  ID_COUNTRY_MAP,
  AD_SCRIPT,
  AD_SCRIPT_WIDGET_TEMPLATE,
  AD_SCRIPT_SEARCH_VERTICAL_TEMPLATE,
  UV_LEVEL_MAP,
  AD_TYPE_LIST,
  requestWithCache,
  AJAX_ERROR_CODE_OBJ,
  generateActivityUrlByActivityId,
  CDN_PREFIX,
  PAYMENTS_KEY_MAP,
  AFFILIATE_TYPE,
  COMMISSION_MODEL,
  WIDGET_MAP_GOOGLE_TRACKING,
  SOURCE_TYPE_MAP,
  RENDER_LANGUAGE_MAP,
  SEARCH_VERTICAL_FILLING_POINT_MAP,
  currencyDecimalplace,
};
