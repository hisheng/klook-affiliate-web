import DOMPurify from "dompurify";
import urlObj from "./url";
var path = require("path");
var Cookies = require("js-cookie");
const config = require("../../../config");
const sendAffiliateLog = require("../desktop/js/monitor").sendAffiliateLog;
import Logger from "@/../../shared/logquery";
import { trackRequestSuccess, trackRequestError } from "@/../../shared/optimus";
import { router } from "../desktop/js/router";
import uuid from "uuid";
const logger = new Logger();
window.addEventListener("error", (error) =>
  logger.handlerError(error, { type: "js" })
);

var klook = (function () {
  var CURRENCY_MAP = {
    CNY: "¥",
    TWD: "NT$",
    HKD: "HK$",
    USD: "US$",
    SGD: "S$",
    GBP: "£",
    THB: "฿",
    JPY: "¥",
    KRW: "₩",
    IDR: "Rps",
  };

  var AJAX_ERROR_CODE_OBJ = {
    success: 0,
    user_not_logged_in: -10001,
    param_invalid: -10002,
    user_already_exist: -10003,
    cant_find_user: -10004,
    need_auth: -10005,
    user_not_authed: -10006,
    auth_fail: -10007,
    admin_required: -10008,
    register_fail: -10009,
    ip_exceed_limit: -10010,
    email_exceed_limit: -10011,
    user_not_admin: -10012,
    user_declined: -10013,
  };

  function isiOS() {
    var u = navigator.userAgent,
      app = navigator.appVersion;
    return !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
  }

  function isIeBrowser() {
    if (!!window.ActiveXObject || "ActiveXObject" in window) {
      return true;
    } else {
      return false;
    }
  }

  const ieDown = (url) => {
    window.open(url);
  };

  const downloadFile = (fileName, url) => {
    if (isIeBrowser()) {
      ieDown(url);
    } else {
      const aLink = document.createElement("a");
      const evt = document.createEvent("MouseEvents");
      evt.initMouseEvent(
        "click",
        true,
        false,
        window,
        0,
        0,
        0,
        0,
        0,
        false,
        false,
        false,
        false,
        0,
        null
      );
      aLink.download = fileName;
      aLink.href = url;
      aLink.dispatchEvent(evt);
    }
  };
  function isSuperAdmin() {
    return +USER_INFO.is_super_admin === 1;
  }
  /*
   * 前端环境变量 dev/production, 通过webpack注入
   *
   * */

  function isDev() {
    return process.env.NODE_ENV === "dev";
  }

  function save_local(key, value, expires) {
    if (window.localStorage) {
      window.localStorage.setItem(key, value);
    } else {
      $.cookie(key, value, {
        path: "/",
        expires: expires || 365,
      });
    }
  }

  function remove_local(key) {
    if (window.localStorage) {
      window.localStorage.removeItem(key);
    }
    $.removeCookie(key, {
      path: "/",
    });
  }

  function get_local(key) {
    if (window.localStorage) {
      return window.localStorage.getItem(key);
    }
    return $.cookie(key);
  }

  function showError(err) {
    if (err.code == CODE_CLIENT_ERROR) {
      popupAlert(err.message);
    } else {
      popupAlert(err.message + "[" + err.code + "]");
    }
  }

  function urlParam(name, value, url) {
    if (typeof value == "undefined") {
      return (
        decodeURIComponent(
          (new RegExp("[?|&]" + name + "=" + "([^&;]+?)(&|#|;|$)").exec(
            url || location.search
          ) || [, ""])[1].replace(/\+/g, "%20")
        ) || null
      );
    } else {
      url = url || window.location.href;
      name = name.toString();
      value = encodeURIComponent(value.toString());
      var r = new RegExp("(^|\\W)" + name + "=[^&]*", "g");
      var vUrl = url.split("#");
      vUrl[0] = vUrl[0].match(r)
        ? vUrl[0].replace(r, "$1" + name + "=" + value)
        : vUrl[0] +
          (vUrl[0].indexOf("?") == -1 ? "?" : "&") +
          name +
          "=" +
          value;
      return vUrl.join("#");
    }
  }

  var CODE_CLIENT_ERROR = 990001;

  function addLangToUrl(url) {
    //KLK_LANG need to be render in the layout
    return urlParam("lang", window.KLK_LANG && KLK_LANG.replace("-", "_"), url);
  }

  function isOutsideApi(url) {
    return Object.keys(urlObj.klook_c_related_url_map)
      .map((url_key) => urlObj.klook_c_related_url_map[url_key])
      .some(function (cUrl) {
        return url.indexOf(cUrl) !== -1;
      });
  }

  function getAPILangByUrl(url) {
    if (isOutsideApi(url)) {
      return getCorrectAPILang();
    }
    let innerAPILang = config.SUPPORT_LANGS[KLK_LANG].name || KLK_LANG;
    return innerAPILang;
  }

  function getCorrectAPILang() {
    if (KLK_LANG == "en") {
      return "en_US";
    }
    return KLK_LANG.replace("-", "_");
  }

  function fetchToken() {
    return new Promise((resolve, reject) => {
      ajaxGet(urlObj.csrf_token, null, (res) => {
        // 请求发出之后，改变状态
        if (!res.success) {
          reject();
        } else {
          let data = Cookies.get("CSRF-TOKEN");
          if (data) {
            resolve(data);
          } else {
            reject();
          }
        }
      });
    });
  }

  async function getTokenByCookie(url) {
    // 1. 先获取cookie中token
    let token = Cookies.get("CSRF-TOKEN");
    let islogin = Cookies.get("islogin");
    // 已登陆情况下，如果已经过期，则重新获取，过滤自己本身的请求, 当过期时间到达时，浏览器并发请求需要控制发请求次数
    if (window.USER_INFO && window.USER_INFO.id) {
      // 判断已经登陆还需要判断 islogin 的值
      if (islogin) {
        if (!token && url !== urlObj.csrf_token) {
          token = await fetchToken();
        }
        return token;
      } else {
        throw new Error("未登陆");
      }
    }
  }

  const logout = () => {
    Cookies.remove("CSRF-TOKEN", "", {
      path: "/",
    });
    Cookies.remove("islogin", "", {
      path: "/",
    });
    window.location.replace("/" + window.KLK_LANG_PATH + "home");
  };

  const isWhiteList = (url) => {
    const whiteList = [
      urlObj.aff_user_register,
      urlObj.email_validation,
      urlObj.user_activation(""),
      urlObj.session,
      urlObj.get_country_list,
    ];
    const arr = whiteList.filter((item) => url.startsWith(item));
    return arr.length > 0;
  };

  var ajaxBase = async function (method, url, data, fn, headers) {
    if (typeof data == "function") {
      headers = fn;
      fn = data;
      data = undefined;
    }

    headers = headers || {};
    if (!isOutsideApi(url) && !isWhiteList(url)) {
      try {
        let token = await getTokenByCookie(url);
        headers["X-CSRF-Token"] = `JWT ${token || ""}`;
      } catch (e) {
        // 捕获到错误之后, 清除 sess 和 token
        logout();
        return;
      }
    }
    // 这里需要对 widget 的渲染做兼容
    window.KLK_LANG ? (url = addLangToUrl(url)) : null;

    var real_method = method;

    if (method == "POSTJSON") {
      real_method = "POST";
    }

    if (method == "PUTJSON") {
      real_method = "PUT";
    }

    var params = {
      method: real_method,
      url: url,
      cache: false,
      dataType: "json",
      startTime: Date.now(),
    };

    if (method == "POSTJSON" || method == "PUTJSON") {
      data = data || {};
      if (typeof data == "object") {
        data = JSON.stringify(data);
      } else {
        data = data.toString();
      }
      params.contentType = "application/json; charset=utf-8";
    }

    if (!headers["Accept-Language"] && window.KLK_LANG) {
      headers["Accept-Language"] = getAPILangByUrl(url);
    }
    // 设置公共头部字段
    headers["X-Klook-Kepler-Id"] = Cookies.get("kepler_id");
    headers["X-Klook-Request-Id"] = uuid.v4();
    headers["X-Klook-Client-Version"] = process.env.__VERSION__; // 前端 master 值
    if (headers) {
      params.headers = headers;
    }

    if (data) {
      params.data = data;
    }

    ajax_wrap(fn, params);

    params.timeout = 60 * 2 * 1000;
    return $.ajax(params);
  };

  var ajaxGet = function (url, data, fn, headers) {
    return ajaxBase("GET", url, data, fn, headers);
  };

  var ajaxPut = function (url, data, fn, headers) {
    return ajaxBase("PUT", url, data, fn, headers);
  };

  var ajaxPutJSON = function (url, data, fn, headers) {
    return ajaxBase("PUTJSON", url, data, fn, headers);
  };

  function ajax_wrap(fn, params) {
    // 广告渲染和 affiliate portal 都会调用该文件, 会把 router 也打包进来
    // 因此在涉及到页面 html 模版中，都需要和 affiliate portal 的渲染保持一致, 否则会报错【涉及一些全局变量】。
    // 对于广告渲染页面没有router, 因此这里也需要做兼容
    params.pathName =
      (router.app && router.app._route.name) || window.location.pathname;
    if (fn) {
      params.success = function (resp, isSuccess, xhr) {
        if (!resp.success) {
          logger.handlerError(
            { name: "Ajax Failure", type: "ajax" },
            { reqUrl: params.url || "", reqParams: params, reqResponse: resp }
          );
          sendAffiliateLog(
            "ajax_err",
            (resp.error || {}).message,
            params.url,
            "",
            "",
            {
              data: params.data,
              // response : resp //减少上报信息
            }
          );
        }
        trackRequestSuccess({ ...resp, status: xhr.status }, params);
        //如果没有message返回默认的message
        resp.error = resp.error || {};
        if (resp.error.code == AJAX_ERROR_CODE_OBJ.user_declined) {
          if (
            params.url.indexOf(urlObj.user_info) != -1 ||
            params.url.indexOf(urlObj.session) != -1
          ) {
            fn(resp);
          } else {
            Cookies.set("sess", "", {
              path: "/",
              expires: new Date(Date.now() - 10000),
            });
            location.href = path.join("/", KLK_LANG_PATH, "/home");
          }
        } else if (
          +resp.error.code === AJAX_ERROR_CODE_OBJ.user_not_logged_in
        ) {
          Cookies.set("sess", "", {
            path: "/",
            expires: new Date(Date.now() - 10000),
          });
          // csrf-token 失效
          Cookies.remove("CSRF-TOKEN", "", {
            path: "/",
          });
          // 需要用户跳转到登陆页面
          window.location.replace("/" + window.KLK_LANG_PATH + "home");
        } else {
          fn(resp);
        }
      };

      params.error = function (xhr, errorType, error) {
        sendAffiliateLog("ajax_err", "", params.url, "", "", {
          // errorType : errorType //减少上报信息
        });

        logger.handlerError(error, {
          type: "ajax",
          reqUrl: (error.config || {}).url,
        });

        trackRequestError({ message: error, status: xhr.status }, params);
        fn({
          success: false,
          error: {
            code: CODE_CLIENT_ERROR,
            message: "MULTIPLE_client_network_failure",
          },
        });
      };
    }
  }

  var ajaxPostJSON = function (url, data, fn, headers) {
    return ajaxBase("POSTJSON", url, data, fn, headers);
  };

  /**
   *  代理到一个函数上面去
   */
  var ajaxPost = function (url, data, fn, headers) {
    return ajaxBase("POST", url, data, fn, headers);
  };

  function strformat() {
    var format = /\{([\d\w\.]+)\}/g;
    var args = Array.prototype.slice.call(arguments),
      v;
    var str = args.shift() + "";
    if (args.length == 1 && typeof args[0] == "object") {
      args = args[0];
    }
    format.lastIndex = 0;
    return str.replace(format, function (m, n) {
      v = args[n];
      return v === undefined ? m : v;
    });
  }

  function fmtActivityStartTime(s, datefmt) {
    datefmt = datefmt || "YYYY-MM-DD";
    var startTime = moment.parseZone(s);
    if (startTime.format("HHmm") == "0000") {
      return startTime.format(datefmt);
    } else {
      return startTime.format(datefmt + " HH:mm");
    }
  }

  function loadCss(url) {
    var link = document.createElement("link");
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = url;
    document.getElementsByTagName("head")[0].appendChild(link);
  }

  function loadScript(url, success) {
    var head =
      document.getElementsByTagName("head")[0] || document.documentElement;
    var script = document.createElement("script");

    script.src = url;

    // Handle Script loading
    var done = false;

    // Attach handlers for all browsers
    script.onload = script.onreadystatechange = function () {
      if (
        !done &&
        (!this.readyState ||
          this.readyState === "loaded" ||
          this.readyState === "complete")
      ) {
        done = true;

        if (typeof success === "function") {
          success();
        }

        // Handle memory leak in IE
        script.onload = script.onreadystatechange = null;
        if (head && script.parentNode) {
          head.removeChild(script);
        }
      }
    };

    // Use insertBefore instead of appendChild  to circumvent an IE6 bug.
    // This arises when a base node is used (#2709 and #4378).
    head.insertBefore(script, head.firstChild);
  }

  function getObjFromInput($ele) {
    var inputs = $ele.find("input[name],textarea[name],select[name]");
    var obj = {};
    _.each(inputs, function (x) {
      var val = $(x).val();
      if (val) {
        obj[$(x).attr("name")] = val;
      }
    });
    return obj;
  }

  function getURLByEnv(path) {
    var url = "/xos_api" + path;
    if (KLK_ENV != "prd") {
      url = _CONF.DEV_WEB_HOST + path;
    }
    return url;
  }

  function addApiUrl(path) {
    // TODO
    if (path.indexOf(_CONF.DEV_API_HOST) != -1) {
      //重复调用
      return path;
    }
    if (KLK_ENV != "prd") {
      path = _CONF.DEV_API_HOST + path;
    }
    return path;
  }

  function checkEmail(email) {
    var reg =
      /^[a-zA-Z0-9_-]+(\.([a-zA-Z0-9_-])+)*@[a-zA-Z0-9_-]+[.][a-zA-Z0-9_-]+([.][a-zA-Z0-9_-]+)*$/;
    return reg.test(email);
  }

  function checkPwd(pwd) {
    //The password must be 8-20 bits, including at least one letter and number，这个是后来加上的规则，之前是只要包含letter和number就好了
    var letterReg = /[a-zA-Z]/g,
      numReg = /[0-9]/g;

    return (
      pwd.length >= 8 &&
      pwd.length <= 20 &&
      letterReg.test(pwd) &&
      numReg.test(pwd)
    );
  }

  function formatPriceThousands(price) {
    var price = (price || "0").toString(),
      tmp;

    if (price.indexOf(".") < 0) {
      tmp = price.replace(/(?=(?!(\b))(\d{3})+$)/g, ",");
    } else {
      price = price.split(".");
      tmp =
        price[0].toString().replace(/(?=(?!(\b))(\d{3})+$)/g, ",") +
        "." +
        price[1];
    }

    return tmp;
  }

  function download_file(src) {
    if ($("#download_iframe").length == 0) {
      $("body").append(
        '<iframe src="{0}" id="download_iframe" style="border:none;padding:0;margin:0;overflow:hidden;visibility:hidden;display:none" ></iframe>'
      );
    }
    //兼容https
    src = src.replace(/^http:/, "");
    $("#download_iframe").attr("src", src);
  }

  function transfer_cent_to_usd_with_formate_thousand(money) {
    //or tansfer 人民币分 to 人民币    资金从数据库出来显示前端之前的处理
    return formatPriceThousands(transfer_cent_to_usd(money)); //所有资金类都做千分位处理
  }

  function accMul(arg1, arg2) {
    var m = 0,
      s1,
      s2;
    try {
      s1 = arg1.toString();
    } catch (e) {
      s1 = 0;
    }
    try {
      s2 = arg2.toString();
    } catch (e) {
      s2 = 0;
    }
    try {
      m += s1.split(".")[1].length;
    } catch (e) {}
    try {
      m += s2.split(".")[1].length;
    } catch (e) {}
    return (
      (Number(s1.replace(".", "")) * Number(s2.replace(".", ""))) /
      Math.pow(10, m)
    );
  }

  function accAdd(arg1, arg2) {
    var r1, r2, m;
    try {
      r1 = arg1.toString().split(".")[1].length;
    } catch (e) {
      r1 = 0;
    }
    try {
      r2 = arg2.toString().split(".")[1].length;
    } catch (e) {
      r2 = 0;
    }
    m = Math.pow(10, Math.max(r1, r2));
    return (accMul(arg1, m) + accMul(arg2, m)) / m;
  }

  function transfer_cent_to_usd(money) {
    // return money / 100;
    return accMul(money, 0.01);
  }

  function transfer_usd_to_cent(money) {
    // or tranfer 人民币 to 人民币分    资金从前端传给后端之前的处理
    // return money * 100;
    return accMul(money, 100);
  }

  function adsEvent(type, data) {
    klook.ajaxPostJSON(
      urlObj.ads_event,
      {
        type,
        data: data || "",
      },
      null,
      {
        "X-iframe-Data": JSON.stringify({
          type,
          data: data || "",
        }),
      }
    );
  }

  function getUrlParam(name) {
    const reg = new RegExp(`(^|&)${name}=([^&]*)(&|$)`);
    const r = window.location.search.substr(1).match(reg);
    if (r !== null) return r[2];
    return null;
  }

  function ads_monitor(prod, receiveMessageCallback) {
    const sendParentMessage = () => {
      const reg = new RegExp(`(^|&)renderId=([^&]*)(&|$)`);
      const r = window.location.search.substr(1).match(reg);
      if (r) {
        window.parent.postMessage(
          {
            type: "iframeRender",
            content: {
              renderID: r[2],
            },
          },
          "*"
        );
      }
    };

    const receiveMessage = (event) => {
      let data;
      if (event.data.type === `${prod}_onload`) {
        data = DOMPurify.sanitize(JSON.stringify(event.data.content));
        receiveMessageCallback && receiveMessageCallback(data);
      }
    };

    const main = () => {
      window.addEventListener("message", receiveMessage, false);
      sendParentMessage();
    };
    main();
  }

  /**
   * cacheJS
   * @param {*} key 缓存的key
   * @param {*} value  value
   * @param {*} seconds 缓存的时间
   * @returns 过期为'' 没有过期返回value
   */
  function cacheJS(key, value, seconds = 0) {
    var timestamp = Date.parse(new Date()) / 1000;
    if (key && value) {
      //设置缓存
      var expire = timestamp + seconds;
      value = value + "|" + expire;
      localStorage.setItem(key, value);
    } else if (key) {
      //获取缓存
      var val = localStorage.getItem(key) || "";
      var tmp = val ? val.split("|") : [];
      if (!tmp[1] || timestamp >= tmp[1]) {
        localStorage.setItem(key, "");
        return "";
      } else {
        return tmp[0];
      }
    } else {
      console.log("need key");
    }
  }
  return {
    isDev: isDev,
    accMul: accMul,
    accAdd: accAdd,
    transfer_cent_to_usd: transfer_cent_to_usd,
    transfer_cent_to_usd_with_formate_thousand:
      transfer_cent_to_usd_with_formate_thousand,
    transfer_usd_to_cent: transfer_usd_to_cent,
    download_file: download_file,
    formatPriceThousands: formatPriceThousands,
    addApiUrl: addApiUrl,
    getURLByEnv: getURLByEnv,
    urlParam: urlParam,
    loadScript: loadScript,
    getTokenByCookie: getTokenByCookie,
    ajaxGet: ajaxGet,
    ajaxPut: ajaxPut,
    ajaxPutJSON: ajaxPutJSON,
    save_local: save_local,
    get_local: get_local,
    remove_local: remove_local,
    ajaxPost: ajaxPost,
    strformat: strformat,
    ajaxPostJSON: ajaxPostJSON,
    isiOS: isiOS,
    isIeBrowser: isIeBrowser,
    checkEmail: checkEmail,
    checkPwd: checkPwd,
    downloadFile: downloadFile,
    AJAX_ERROR_CODE_OBJ: AJAX_ERROR_CODE_OBJ,
    logger,
    adsEvent,
    getUrlParam,
    ads_monitor,
    cacheJS,
  };
})();

klook.md5 = (function () {
  function md5cycle(x, k) {
    var a = x[0],
      b = x[1],
      c = x[2],
      d = x[3];

    a = ff(a, b, c, d, k[0], 7, -680876936);
    d = ff(d, a, b, c, k[1], 12, -389564586);
    c = ff(c, d, a, b, k[2], 17, 606105819);
    b = ff(b, c, d, a, k[3], 22, -1044525330);
    a = ff(a, b, c, d, k[4], 7, -176418897);
    d = ff(d, a, b, c, k[5], 12, 1200080426);
    c = ff(c, d, a, b, k[6], 17, -1473231341);
    b = ff(b, c, d, a, k[7], 22, -45705983);
    a = ff(a, b, c, d, k[8], 7, 1770035416);
    d = ff(d, a, b, c, k[9], 12, -1958414417);
    c = ff(c, d, a, b, k[10], 17, -42063);
    b = ff(b, c, d, a, k[11], 22, -1990404162);
    a = ff(a, b, c, d, k[12], 7, 1804603682);
    d = ff(d, a, b, c, k[13], 12, -40341101);
    c = ff(c, d, a, b, k[14], 17, -1502002290);
    b = ff(b, c, d, a, k[15], 22, 1236535329);

    a = gg(a, b, c, d, k[1], 5, -165796510);
    d = gg(d, a, b, c, k[6], 9, -1069501632);
    c = gg(c, d, a, b, k[11], 14, 643717713);
    b = gg(b, c, d, a, k[0], 20, -373897302);
    a = gg(a, b, c, d, k[5], 5, -701558691);
    d = gg(d, a, b, c, k[10], 9, 38016083);
    c = gg(c, d, a, b, k[15], 14, -660478335);
    b = gg(b, c, d, a, k[4], 20, -405537848);
    a = gg(a, b, c, d, k[9], 5, 568446438);
    d = gg(d, a, b, c, k[14], 9, -1019803690);
    c = gg(c, d, a, b, k[3], 14, -187363961);
    b = gg(b, c, d, a, k[8], 20, 1163531501);
    a = gg(a, b, c, d, k[13], 5, -1444681467);
    d = gg(d, a, b, c, k[2], 9, -51403784);
    c = gg(c, d, a, b, k[7], 14, 1735328473);
    b = gg(b, c, d, a, k[12], 20, -1926607734);

    a = hh(a, b, c, d, k[5], 4, -378558);
    d = hh(d, a, b, c, k[8], 11, -2022574463);
    c = hh(c, d, a, b, k[11], 16, 1839030562);
    b = hh(b, c, d, a, k[14], 23, -35309556);
    a = hh(a, b, c, d, k[1], 4, -1530992060);
    d = hh(d, a, b, c, k[4], 11, 1272893353);
    c = hh(c, d, a, b, k[7], 16, -155497632);
    b = hh(b, c, d, a, k[10], 23, -1094730640);
    a = hh(a, b, c, d, k[13], 4, 681279174);
    d = hh(d, a, b, c, k[0], 11, -358537222);
    c = hh(c, d, a, b, k[3], 16, -722521979);
    b = hh(b, c, d, a, k[6], 23, 76029189);
    a = hh(a, b, c, d, k[9], 4, -640364487);
    d = hh(d, a, b, c, k[12], 11, -421815835);
    c = hh(c, d, a, b, k[15], 16, 530742520);
    b = hh(b, c, d, a, k[2], 23, -995338651);

    a = ii(a, b, c, d, k[0], 6, -198630844);
    d = ii(d, a, b, c, k[7], 10, 1126891415);
    c = ii(c, d, a, b, k[14], 15, -1416354905);
    b = ii(b, c, d, a, k[5], 21, -57434055);
    a = ii(a, b, c, d, k[12], 6, 1700485571);
    d = ii(d, a, b, c, k[3], 10, -1894986606);
    c = ii(c, d, a, b, k[10], 15, -1051523);
    b = ii(b, c, d, a, k[1], 21, -2054922799);
    a = ii(a, b, c, d, k[8], 6, 1873313359);
    d = ii(d, a, b, c, k[15], 10, -30611744);
    c = ii(c, d, a, b, k[6], 15, -1560198380);
    b = ii(b, c, d, a, k[13], 21, 1309151649);
    a = ii(a, b, c, d, k[4], 6, -145523070);
    d = ii(d, a, b, c, k[11], 10, -1120210379);
    c = ii(c, d, a, b, k[2], 15, 718787259);
    b = ii(b, c, d, a, k[9], 21, -343485551);

    x[0] = add32(a, x[0]);
    x[1] = add32(b, x[1]);
    x[2] = add32(c, x[2]);
    x[3] = add32(d, x[3]);
  }

  function cmn(q, a, b, x, s, t) {
    a = add32(add32(a, q), add32(x, t));
    return add32((a << s) | (a >>> (32 - s)), b);
  }

  function ff(a, b, c, d, x, s, t) {
    return cmn((b & c) | (~b & d), a, b, x, s, t);
  }

  function gg(a, b, c, d, x, s, t) {
    return cmn((b & d) | (c & ~d), a, b, x, s, t);
  }

  function hh(a, b, c, d, x, s, t) {
    return cmn(b ^ c ^ d, a, b, x, s, t);
  }

  function ii(a, b, c, d, x, s, t) {
    return cmn(c ^ (b | ~d), a, b, x, s, t);
  }

  function md51(s) {
    // Converts the string to UTF-8 "bytes" when necessary
    if (/[\x80-\xFF]/.test(s)) {
      s = unescape(encodeURI(s));
    }
    var txt = "";
    var n = s.length,
      state = [1732584193, -271733879, -1732584194, 271733878],
      i;
    for (i = 64; i <= s.length; i += 64) {
      md5cycle(state, md5blk(s.substring(i - 64, i)));
    }
    s = s.substring(i - 64);
    var tail = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    for (i = 0; i < s.length; i++)
      tail[i >> 2] |= s.charCodeAt(i) << (i % 4 << 3);
    tail[i >> 2] |= 0x80 << (i % 4 << 3);
    if (i > 55) {
      md5cycle(state, tail);
      for (i = 0; i < 16; i++) tail[i] = 0;
    }
    tail[14] = n * 8;
    md5cycle(state, tail);
    return state;
  }

  function md5blk(s) {
    /* I figured global was faster. */
    var md5blks = [],
      i; /* Andy King said do it this way. */
    for (i = 0; i < 64; i += 4) {
      md5blks[i >> 2] =
        s.charCodeAt(i) +
        (s.charCodeAt(i + 1) << 8) +
        (s.charCodeAt(i + 2) << 16) +
        (s.charCodeAt(i + 3) << 24);
    }
    return md5blks;
  }

  var hex_chr = "0123456789abcdef".split("");

  function rhex(n) {
    var s = "",
      j = 0;
    for (; j < 4; j++)
      s += hex_chr[(n >> (j * 8 + 4)) & 0x0f] + hex_chr[(n >> (j * 8)) & 0x0f];
    return s;
  }

  function hex(x) {
    for (var i = 0; i < x.length; i++) x[i] = rhex(x[i]);
    return x.join("");
  }

  var md5 = function (s) {
    return hex(md51(s));
  };

  /* this function is much faster, so if possible we use it. Some IEs are the
      only ones I know of that need the idiotic second function, generated by an
      if clause.  */
  function add32(a, b) {
    return (a + b) & 0xffffffff;
  }

  if (md5("hello") != "5d41402abc4b2a76b9719d911017c592") {
    function add32(x, y) {
      var lsw = (x & 0xffff) + (y & 0xffff),
        msw = (x >> 16) + (y >> 16) + (lsw >> 16);
      return (msw << 16) | (lsw & 0xffff);
    }
  }
  return md5;
})();

export default klook;
