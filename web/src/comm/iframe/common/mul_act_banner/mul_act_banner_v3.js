(() => {
  class BaseIframe {
    constructor(className, version, callBack) {
      this.className = className;
      this.callBack = callBack;
      this.version = version;
      this.iframeheight = 0;
      this.hostUrl = "";
      this.openAlert = true; // 默认开启
      this.iframeClass = `klook_iframe_${new Date().getMilliseconds()}`;
      this.initSuccessStatus = { [this.iframeClass]: false };
      this.init();
      this.dataset = {};
      this.errorUrl = "/v1/affnode/error_msg";
    }

    //回调函数
    message(event) {
      if (event.data.type === `${this.prod}_success`) {
        this.initSuccessStatus[this.iframeClass] = true;
      }
    }

    init() {
      // 获取页面属性值
      const elements = document.querySelectorAll(this.className);
      elements.forEach((element, index) => {
        // 记录初始化失败
        const obj = (element && element.dataset) || {};
        // 设置高度
        const attrObj = (this.callBack && this.callBack(obj, element)) || obj;
        elements[index].innerHTML = "";
        elements[index].appendChild(this.createComponent(attrObj));
      });
      this.fun = this.message.bind(this);
      this.shouldMonitor() &&
        window.addEventListener("message", this.fun, false);
    }

    shouldMonitor() {
      return (
        this.openAlert &&
        `${window.location.protocol}//${window.location.host}` !== this.hostUrl
      );
    }

    checkIframeInitSuccess(dataAttr, success, error) {
      // 10S 后检查状态, 如果页面上存在多个同种类型的ads, 只会检测最后一次是否成功和失败
      setTimeout(() => {
        // 检查所有的 initSuccessArr
        const initSuccess = this.initSuccessStatus[this.iframeClass];
        // 移除事件
        window.removeEventListener("message", this.fun, false);
        if (initSuccess) {
          console.log(`${dataAttr.prod}, ${this.iframeClass} ,初始化成功`);
          success && success(dataAttr.prod);
        } else {
          console.log(`${dataAttr.prod}, ${this.iframeClass} ,初始化失败`);
          error && error(dataAttr.prod);
          this.sendErrorMessage(dataAttr);
        }
      }, 10000);
    }

    getInformation(element) {
      // 当前页面 url
      const url = window.location.href;
      // 页面中同类型第几个 iframe
      // iframe 宽, 高容器位置
      // 广告是否被进入视图口, 监听
      const { clientHeight, clientWidth } = element;
      return {
        url,
        size: {
          clientWidth, // 元素宽
          clientHeight, // 元素高
        },
        // 绝对位置
        position: {
          x:
            element.getBoundingClientRect().left +
            document.documentElement.scrollLeft,
          y:
            element.getBoundingClientRect().top +
            document.documentElement.scrollTop,
        },
      };
    }

    // iframe 加载失败, 上报接口到 node 接口
    sendErrorMessage(dataAttr) {
      // 定义上报接口
      const errorObj = {
        level: "E",
        type: "IFRAME_SCRIPT_TIME_OUT",
        err_msg: "iframe onload timeout",
        href: window.location.href,
        referer: window.document.referrer,
        userAgent: window.navigator.userAgent,
        version: this.version,
        prod: dataAttr.prod,
        dataset: Object.assign({}, dataAttr),
      };
      const xhr = new XMLHttpRequest();
      xhr.open("POST", `${this.hostUrl}${this.errorUrl}`);
      xhr.setRequestHeader("Content-Type", "application/json");
      xhr.send(JSON.stringify(errorObj, null, 2));
    }

    createComponent(attrObj) {
      if (attrObj.open_alert) {
        this.openAlert = attrObj.open_alert;
      }
      this.prod = attrObj.prod;
      const url = `/v1/affnode/render`;
      const src = `${this.host(attrObj.host)}${url}`;
      const query = Object.entries(attrObj)
        .map((item, index) => {
          let key = item[0];
          let value = item[1];
          if (key !== "host") {
            return `${key}=${encodeURIComponent(value.trim())}`;
          }
        })
        .join("&");
      this.url = `${src}?${query}`;
      var iframe = document.createElement("iframe");
      iframe.setAttribute("class", this.iframeClass);
      iframe.setAttribute("allowtransparency", true);
      iframe.setAttribute("marginheight", 0);
      iframe.setAttribute("marginwidth", 0);
      iframe.setAttribute("frameborder", 0);
      iframe.style.cssText = `${this.iframeStyle(
        attrObj.width || "100%",
        attrObj.height || "100%"
      )}`;
      iframe.title =
        "Klook.com third party widget. Discover and book amazing things to do at exclusive prices. Links open in an external site that may or may not meet accessibility guidelines.";
      iframe.onload = () => {
        // 过 20S 发送 PostMessage 请求通信
        setTimeout(() => {
          iframe.contentWindow.postMessage(
            {
              type: `${this.prod}_onload`,
              content: {
                index: this.iframeClass,
                ...this.getInformation(iframe),
              },
            },
            this.hostUrl
          );
          // 过 10S 判断一下同类型广告最后一个是否渲染成功
          this.shouldMonitor() && this.checkIframeInitSuccess(attrObj);
        }, 20000);
      };
      // 先指定onload， 再赋值 src
      iframe.src = this.url;
      return iframe;
    }

    host(dataHost) {
      // 由于脚本可单独使用, 开发/测试 通过显式ins标签属性传入；产线不传走 affiliate 产线域名
      this.hostUrl = dataHost || "https://affiliate.klook.com";
      return dataHost ? dataHost : "https://affiliate.klook.com";
    }

    iframeStyle(width, height) {
      return `border:none;padding:0;margin:0;overflow:hidden;max-width:none;width:${width};height:${height};`;
    }
  }

  class Mul_act_banner_v3 extends BaseIframe {
    constructor(className, version, callback) {
      super(className, version, callback);
    }
  }

  new Mul_act_banner_v3(".mul_act_banner", 3);
})();
