(() => {
  class BaseIframe {
    constructor(className, version, callBack) {
      this.className = className;
      this.callBack = callBack;
      this.version = version;
      this.iframeheight = 0;
      this.hostUrl = "";
      this.renderElement = null;
      this.openAlert = true; // 默认开启
      // 有可能页面存在多个脚本, 记录多个状态
      this.iframeClass = [];
      this.initSuccessStatus = {};
      this.init();
      this.dataset = {};
    }

    //回调函数
    message(event) {
      if (
        event.data.type === `iframeRender` &&
        this.iframeClass.includes(event.data.content.renderID)
      ) {
        this.initSuccessStatus[event.data.content.renderID] = true;
        this.sendInfo(event.data.content.renderID);
      }
    }

    sendInfo(className) {
      const ele = document.querySelector(`.${className}`);
      if (ele) {
        if (ele.contentWindow && ele.contentWindow.postMessage) {
          // 统计当前页面数据, 方便 affiliate 分析
          ele.contentWindow.postMessage(
            {
              type: `${this.prod}_onload`,
              content: this.getInformation(),
            },
            this.hostUrl
          );
        }
      }
    }

    init() {
      // 获取页面属性值
      this.fun = this.message.bind(this);
      window.addEventListener("message", this.fun, false);
      const elements = document.querySelectorAll(this.className);
      elements.forEach((item) => {
        // 判断 item children 是否为 iframe
        if (item.getElementsByTagName("a").length > 0) {
          const element = item;
          if (element) {
            // 未进行渲染, 进行初始化
            const timeStamp = `klook_iframe_${Date.now()}`;
            this.iframeClass.push(timeStamp);
            this.initSuccessStatus[timeStamp] = false;
            const obj = (element && element.dataset) || {};
            // 设置高度
            const attrObj =
              (this.callBack && this.callBack(obj, element)) || obj;
            // render
            element.innerHTML = this.createComponent(attrObj, timeStamp);
            // 获取 iframe
            this.renderElement = element.childNodes[0];
            // iframeInit
            this.iframeInit();
            // check iframe
            this.checkIframeInitSuccess(
              this.renderElement,
              this.checkSuccess.bind(this),
              this.checkFail.bind(this)
            );
          }
        }
      });
    }

    iframeInit() {
      const data = this.getInformation();
      // 发送数据
      this.uploadMessage({
        type: 1,
        data,
      });
    }

    checkSuccess() {
      const data = this.getInformation();
      // 发送数据
      this.uploadMessage({
        type: 2,
        data,
      });
    }

    checkFail() {
      const data = this.getInformation();
      // 发送数据
      this.uploadMessage({
        type: 3,
        data,
      });
    }

    uploadMessage(content) {
      content.data = JSON.stringify(content.data);
      const xhr = new XMLHttpRequest();
      xhr.open("POST", `${this.hostUrl}/v3/affsrv/ads/event`);
      xhr.setRequestHeader("Content-Type", "application/json");
      xhr.send(JSON.stringify(content, null, 2));
    }

    checkIframeInitSuccess(element, success, error) {
      setTimeout(() => {
        const initSuccess = this.initSuccessStatus[element.className];
        if (initSuccess) {
          success && success();
        } else {
          error && error();
        }
      }, 5000);
    }

    getInformation() {
      const { clientHeight, clientWidth, className } = this.renderElement;
      return {
        renderId: className,
        href: window.location.href,
        referer: window.document.referrer,
        userAgent: window.navigator.userAgent,
        version: this.version,
        dataset: Object.assign({}, this.renderElement.parentNode.dataset),
        size: {
          clientWidth, // 元素宽
          clientHeight, // 元素高
        },
        // 绝对位置
        position: {
          x:
            this.renderElement.getBoundingClientRect().left +
            document.documentElement.scrollLeft,
          y:
            this.renderElement.getBoundingClientRect().top +
            document.documentElement.scrollTop,
        },
      };
    }

    createComponent(attrObj, timeStamp) {
      if (attrObj.open_alert) {
        this.openAlert = attrObj.open_alert;
      }
      this.prod = attrObj.prod;
      const url = `/v1/affnode/render`;
      const src = `${this.host(attrObj.host)}${url}`;
      const query = Object.entries(attrObj)
        .map((item, index) => {
          let key = item[0];
          let value = item[1];
          if (key !== "host") {
            return `${key}=${encodeURIComponent(value.trim())}`;
          }
        })
        .join("&");
      return `<iframe src=${src}?${query}&renderId=${timeStamp}
                            class=${timeStamp}
                            style=${this.iframeStyle(
                              attrObj.width || "100%",
                              attrObj.height || "100%"
                            )}
                            marginheight="0"
                            marginwidth="0"
                            frameborder="0"
                            allowtransparency="true"
                            title="Klook.com third party widget. Discover and book amazing things to do at exclusive prices. Links open in an external site that may or may not meet accessibility guidelines."
                        >
                </iframe>`;
    }

    host(dataHost) {
      // 由于脚本可单独使用, 开发/测试 通过显式ins标签属性传入；产线不传走 affiliate 产线域名
      this.hostUrl = dataHost || "https://affiliate.klook.com";
      return dataHost || "https://affiliate.klook.com";
    }

    iframeStyle(width, height) {
      return `border:none;padding:0;margin:0;overflow:hidden;max-width:none;width:${width};height:${height};`;
    }
  }

  class DynamicWidget extends BaseIframe {
    constructor(className, version, callback) {
      super(className, version, callback);
    }
  }

  new DynamicWidget(".klook_aff_search_box", 3);
})();
