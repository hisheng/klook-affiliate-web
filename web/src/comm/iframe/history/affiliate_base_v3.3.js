window.KlookAff = (function () {
  if (!document.getElementsByClassName) {
    document.getElementsByClassName = function (l) {
      var o = document,
        n,
        m,
        k,
        j = [];
      if (o.querySelectorAll) {
        return o.querySelectorAll("." + l);
      }
      if (o.evaluate) {
        m = ".//*[contains(concat(' ', @class, ' '), ' " + l + " ')]";
        n = o.evaluate(m, o, null, 0, null);
        while ((k = n.iterateNext())) {
          j.push(k);
        }
      } else {
        n = o.getElementsByTagName("*");
        m = new RegExp("(^|\\s)" + l + "(\\s|$)");
        for (k = 0; k < n.length; k++) {
          if (m.test(n[k].className)) {
            j.push(n[k]);
          }
        }
      }
      return j;
    };
  }
  // 对外我们一致的dynamic widget产品，因为有两种模式，所以内部我们后端保存的实际是两种prodType
  var dynamicWidgetProdType = ["static_widget", "dynamic_widget"];
  if (!String.prototype.trim) {
    String.prototype.trim = function () {
      return this.replace(/^\s+|\s+$/g, "");
    };
  }
  function d(j) {
    if (window.console && typeof window.console.error == "function") {
      console.error(j);
    }
  }
  function isNumber(j) {
    return !isNaN(parseInt(j, 10));
  }
  function joinAttribute(k) {
    var l,
      j = "";
    for (l in k) {
      if (typeof k[l] != "undefined") {
        j += l + '="' + k[l] + '" ';
      }
    }
    return j;
  }
  function attribute(k, j, l) {
    if (typeof l == "undefined") {
      return k.getAttribute(j);
    } else {
      k.setAttribute(j, l);
    }
  }

  function joinParams(j) {
    var k,
      l = "";
    for (k in j) {
      if (typeof j[k] != "undefined") {
        l += k + "=" + encodeURIComponent(j[k]) + "&";
      }
    }
    return l;
  }

  function get_host() {
    if (window.__klk_aff_host) {
      return window.__klk_aff_host;
    }
    return "//affiliate.klook.com";
  }

  function getStyle(widthValue, heightValue) {
    return (
      "border:none;padding:0;margin:0;overflow:hidden;max-width:none;width:" +
      widthValue +
      ";height:" +
      heightValue
    );
  }

  function joinHtml(j) {
    var prodType = j.prod.toLowerCase();
    var shouldUseV3 = dynamicWidgetProdType.indexOf(prodType) > -1;
    var htmlRenderTemplate = shouldUseV3
      ? "/v3/affsrv/widget_template?"
      : "/s/widgets/banner_v2?";
    var m = get_host() + htmlRenderTemplate,
      l = "<iframe ";
    // 为了减少给后端发送无效请求，增加渲染接口的参数判断逻辑，在以下情况（没有adid,没有wid,或者在prod为mul_act的时候没有actids），不发送渲染的请求，参数错误会报错.
    if (
      !isNaN(j.adid) &&
      !isNaN(j.wid) &&
      (prodType != "mul_act" || j.actids != "")
    ) {
      m += joinParams({
        prod: prodType,
        w: j.width,
        h: j.height,
        lang: j.lang,
        adid: j.adid,
        wid: j.wid,
        actids: j.actids,
        price: j.price,
        currency: j.currency,
        banner_id: j.banner_id,
        tmpl: j.tmpl,
        bgtype: j.bgtype,
        cid: j.cid,
        tid: j.tid,
        amount: j.amount,
      });
      l += joinAttribute({
        src: m,
        scrolling: "no",
        style: shouldUseV3
          ? getStyle("100%", "100%")
          : getStyle(
              parseInt(j.width, 10) + "px",
              parseInt(j.height, 10) + "px"
            ),
        marginheight: "0",
        marginwidth: "0",
        frameborder: "0",
        allowtransparency: "true",
        title:
          "Klook.com third party widget. Discover and book amazing things to do at exclusive prices. Links open in an external site that may or may not meet accessibility guidelines.",
      });
      l += ">";
      l += "</iframe>";
      return l;
    }
    return "";
  }
  function getAttrData(n) {
    var j = [
        "width",
        "height",
        "prod",
        "adid",
        "wid",
        "lang",
        "currency",
        "actids",
        "price",
        "bgtype",
        "cid",
        "tid",
        "amount",
      ],
      m = {},
      l,
      k;
    for (l = j.length - 1; l >= 0; l--) {
      k = attribute(n, "data-" + j[l]);
      if (typeof k == "string" && k.length) {
        m[j[l]] = k.trim();
      }
    }
    return m;
  }
  function checkData(l) {
    var p = l.width,
      k = l.height,
      j = l.prod,
      n = ["banner", "search_banner", "mul_act", "single_act", "app"].concat(
        dynamicWidgetProdType
      ),
      m,
      o = false;
    if (p && k && j) {
      if (
        dynamicWidgetProdType.indexOf(j) <= -1 &&
        (!isNumber(p) || !isNumber(k))
      ) {
        throw Error("KlookAff: width, height should be integer");
      }
      j = j.toLowerCase();
      for (m = n.length - 1; m >= 0; m--) {
        if (n[m] == j) {
          o = true;
          break;
        }
      }
      if (!o) {
        throw Error("KlookAff: Invalid product type: " + j);
      }
    }
  }
  /**
   * 这个脚本的效果就是，运行之后会把一个iframe插入到<ins class="klookaff" *** ></ins>里面去
   * 这个iframe会带着参数去请求渲染widget的HTML模版，服务端会读取参数,
   * 然后把数据加入到html模版里面去，把最终的一个静态的html文件返回来
   */
  function c() {
    // n: 这里获取到<ins>那个DOM元素，n就是由它组成的一个单元素的数组
    // l: 1
    var n = document.getElementsByClassName("klookaff"),
      m = "data-kl-touched",
      k,
      l = n.length,
      j,
      q;
    for (var o = 0; o < l; o++) {
      // j: 就是<ins>
      j = n[o];
      k = attribute(j, m);
      if (!k) {
        // q: 就是<ins>里面所有属性组成的一个对象.
        q = getAttrData(n[o]);
        try {
          checkData(q);
          // joinHtml(q): 生成包装好的<iframe>，包括了请求的html模版和传过去的参数.
          // 作为innerHTML部分加入到<ins>里面.
          if (joinHtml(q)) {
            n[o].innerHTML = joinHtml(q);
          }
        } catch (p) {
          d(p.message);
        }
        attribute(j, m, "true");
      }
    }
  }
  return {
    run: c,
  };
})();
KlookAff.run();
