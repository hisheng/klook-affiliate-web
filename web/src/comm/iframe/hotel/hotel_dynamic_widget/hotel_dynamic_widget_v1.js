(() => {
  class BaseIframe {
    constructor(className, callBack) {
      this.className = className;
      this.callBack = callBack;
      this.iframeheight = 0;
      this.init();
    }

    init() {
      // 获取页面属性值
      const elements = document.querySelectorAll(this.className);
      elements.forEach((element, index) => {
        const obj = (element && element.dataset) || {};
        // 设置高度
        const attrObj = (this.callBack && this.callBack(obj, element)) || obj;
        elements[index].innerHTML = this.renderTemplate(attrObj);
      });
    }

    renderTemplate(attrObj) {
      const url = `/v1/affnode/render`;
      const src = `${this.host(attrObj.host)}${url}`;
      const query = Object.entries(attrObj)
        .map((item, index) => {
          let key = item[0];
          let value = item[1];
          if (key !== "host") {
            return `${key}=${encodeURIComponent(value.trim())}`;
          }
        })
        .join("&");
      return `<iframe src=${src}?${query}
                        style=${this.iframeStyle(
                          attrObj.width || "100%",
                          attrObj.height || "100%"
                        )}
                        marginheight="0"
                        marginwidth="0"
                        frameborder="0"
                        allowtransparency="true"
                        title="Klook.com third party widget. Discover and book amazing things to do at exclusive prices. Links open in an external site that may or may not meet accessibility guidelines."
                        >
                </iframe>`;
    }

    host(dataHost) {
      // 由于脚本可单独使用, 开发/测试 通过显式ins标签属性传入；产线不传走 affiliate 产线域名
      return dataHost ? dataHost : "//affiliate.klook.com";
    }

    iframeStyle(width, height) {
      return `border:none;padding:0;margin:0;overflow:hidden;max-width:none;width:${width};height:${height}`;
    }
  }
  class HotelDynamicWidget extends BaseIframe {
    constructor(className, callback) {
      super(className, callback);
    }
  }
  // 可以直接通过设置data-height来设置高度,不传入该函数即可
  const changeDataAttr = (dataAttr, element) => {
    // 响应式设置iframe高度
    const { cardh, padding, lgh, edgeValue, amount } = dataAttr;
    const narrowHeight = +cardh * +amount + +padding; // 小窗口
    const wideHeight = +lgh;
    const iframeWidth = element.parentElement.clientWidth || 0;
    if (iframeWidth > 0) {
      dataAttr.height =
        iframeWidth < edgeValue ? `${narrowHeight}px` : `${wideHeight}px`;
    }
  };
  new HotelDynamicWidget(".klook_hotel_dynamic_widget", changeDataAttr);
})();
