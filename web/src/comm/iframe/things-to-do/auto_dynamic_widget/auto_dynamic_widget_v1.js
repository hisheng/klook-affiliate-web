window.klookaff_auto_dynamic_widget = (function () {
  if (!document.getElementsByClassName) {
    document.getElementsByClassName = function (l) {
      var o = document,
        n,
        m,
        k,
        j = [];
      if (o.querySelectorAll) {
        return o.querySelectorAll("." + l);
      }
      if (o.evaluate) {
        m = ".//*[contains(concat(' ', @class, ' '), ' " + l + " ')]";
        n = o.evaluate(m, o, null, 0, null);
        while ((k = n.iterateNext())) {
          j.push(k);
        }
      } else {
        n = o.getElementsByTagName("*");
        m = new RegExp("(^|\\s)" + l + "(\\s|$)");
        for (k = 0; k < n.length; k++) {
          if (m.test(n[k].className)) {
            j.push(n[k]);
          }
        }
      }
      return j;
    };
  }
  if (!String.prototype.trim) {
    String.prototype.trim = function () {
      return this.replace(/^\s+|\s+$/g, "");
    };
  }
  function d(j) {
    if (window.console && typeof window.console.error == "function") {
      console.error(j);
    }
  }
  function joinAttribute(k) {
    var l,
      j = "";
    for (l in k) {
      if (typeof k[l] != "undefined") {
        j += l + '="' + k[l] + '" ';
      }
    }
    return j;
  }
  function attribute(k, j, l) {
    if (typeof l == "undefined") {
      return k.getAttribute(j);
    } else {
      k.setAttribute(j, l);
    }
  }

  function joinParams(j) {
    var k,
      l = "";
    for (k in j) {
      if (typeof j[k] != "undefined") {
        l += k + "=" + encodeURIComponent(j[k]) + "&";
      }
    }
    return l;
  }

  function get_host() {
    if (window.__klk_aff_host) {
      return window.__klk_aff_host;
    }
    return "//affiliate.klook.com";
  }

  function getStyle(widthValue, heightValue) {
    return (
      "border:none;padding:0;margin:0;overflow:hidden;max-width:none;width:" +
      widthValue +
      ";height:" +
      heightValue
    );
  }

  function joinHtml(j, w = "100%", h = "100%") {
    var url = "/v1/affnode/render?";
    var m = get_host() + url,
      l = "<iframe ";
    m += joinParams(j);
    l += joinAttribute({
      src: m,
      scrolling: "no",
      style: getStyle(w, h),
      marginheight: "0",
      marginwidth: "0",
      frameborder: "0",
      allowtransparency: "true",
      title:
        "Klook.com third party widget. Discover and book amazing things to do at exclusive prices. Links open in an external site that may or may not meet accessibility guidelines.",
    });
    l += ">";
    l += "</iframe>";
    return l;
  }
  function checkData(l) {
    var n = l.amount;
    if (!n || [1, 2, 3].indexOf(+n) === -1) {
      throw Error(
        "KlookAff: Invalid product type: auto_dynamic_widget--amount"
      );
    }
  }
  function c() {
    var n = document.getElementsByClassName("klookaff_auto_dynamic_widget"),
      m = "data-kl-touched",
      k,
      l = n.length,
      j,
      q;
    for (var o = 0; o < l; o++) {
      j = n[o];
      k = attribute(j, m);
      if (!k) {
        q = n[o].dataset;
        q.prod = "auto_dynamic_widget";
        q.publisher_host = window.location.host;
        try {
          checkData(q);
          var ch = 126;
          var pd = 92;
          var lh = 470;
          var ev = 655;
          var at = +q.amount;
          var htm = joinHtml(q),
            minh_sm = ch * at + pd,
            minh_lg = lh,
            pw = j.parentElement.clientWidth || 0;
          if (pw > 0) {
            if (pw < ev) {
              htm = joinHtml(q, "100%", `${minh_sm}px`);
            }
            if (pw >= ev) {
              htm = joinHtml(q, "100%", `${minh_lg}px`);
            }
          }
          if (htm) {
            n[o].innerHTML = htm;
          }
        } catch (p) {
          d(p.message);
        }
        attribute(j, m, "true");
      }
    }
  }
  return {
    run: c,
  };
})();
klookaff_auto_dynamic_widget.run();
