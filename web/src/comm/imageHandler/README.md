# ImageHandler
> 提供简单的图像处理能力

## 外部依赖
- html2canvas

## 浏览器兼容性
- Firefox 3.5+
- Google Chrome
- Opera 12+
- IE9+
- Edge
- Safari 6+

## 使用方法
```js
import ImageHandler from 'path/imageHandler';

ImageHandler()
.composite(el, {
    scale: 0.8,
    quality: 1,
    bgcolor: 'red',
    transType: 'base64',
    callback: (dataUrl) => {...}
})
.binarize(el, {
    transType: 'blob',
    callback: (blob) => {...}
})
```

### Function Params
1. composite <Function> 合并DOM -> dataUrl/Blob

| 参数名称              | 类型          | 默认             | 说明                                       
|---------------------|---------------|-----------------|--------------------------------------------------------------
| el                  | Node          | 上一次处理后的模版 | 如果不传 option，可以不传；传了 option，但是想用上次的模版，就传 null    
| option              | Object        | {}              | 配置选项  
| option.scale        | Number        | 1               | 放大倍数（可选范围：0 - 1）
| option.quality      | Number        | 0.92            | 输出图片质量（可选范围：0 - 1）
| option.bgcolor      | String        | 'transparent'   | 输出图片背景色 (例：'transparent'、'blue'、'#ffffff')
| option.mimeType     | String        | 'image/png'     | 输出图片 MIME 类型，若transType 传 'blob'，则可选 'image/jpeg'
| option.transType    | String        | 'base64'        | 输出图片数据格式（可选：'base64' | 'blob' | 'all'）
| option.callback     | Function      | undefined       | 回调函数

2. binarize <Function> 图片二值化

| 参数名称              | 类型          | 默认             | 说明                                       
|---------------------|---------------|-----------------|--------------------------------------------------------------
| el                  | Node          | 上一次处理后的模版 | 如果不传 option，可以不传；传了 option，但是想用上次的模版，就传 null  
| option              | Object        | {}              | 配置选项  
| option.quality      | Number        | 0.92            | 输出图片质量（可选范围：0 - 1）
| option.mode         | Number        | 1               | 输出图片的模式（可选：1 - 黑底白图 | 2 - 白底黑图）
| option.threshold    | Number        | auto            | 阈值（可选范围：0 - 255），不传则自动计算出最佳阈值
| option.isTransparent| Boolean       | true            | 输出图片是否背景透明
| option.specialValues| Array         | []              | 由于最佳阈值情况下可能没办法显示特定的颜色，需要手动对某些色值做特殊处理
| option.mimeType     | String        | 'image/png'     | 输出图片 MIME 类型，若transType 传 'blob'，则可选 'image/jpeg'
| option.transType    | String        | 'base64'        | 输出图片数据格式（可选：'base64' | 'blob' | 'all'）
| option.callback     | Function      | undefined       | 回调函数

3. greyscale <Function> 图片灰度化

| 参数名称              | 类型          | 默认             | 说明                                       
|---------------------|---------------|-----------------|--------------------------------------------------------------
| el                  | Node          | 上一次处理后的模版 | 如果不传 option，可以不传；传了 option，但是想用上次的模版，就传 null  
| option              | Object        | {}              | 配置选项  
| option.quality      | Number        | 0.92            | 输出图片质量（可选范围：0 - 1）
| option.mimeType     | String        | 'image/png'     | 输出图片 MIME 类型，若transType 传 'blob'，则可选 'image/jpeg'
| option.transType    | String        | 'base64'        | 输出图片数据格式（可选：'base64' | 'blob' | 'all'）
| option.callback     | Function      | undefined       | 回调函数


4. clip <Function> 图片去白边 + 透明化

| 参数名称              | 类型          | 默认             | 说明                                       
|---------------------|---------------|-----------------|--------------------------------------------------------------
| el                  | Node          | 上一次处理后的模版 | 如果不传 option，可以不传；传了 option，但是想用上次的模版，就传 null  
| option              | Object        | {}              | 配置选项  
| option.quality      | Number        | 0.92            | 输出图片质量（可选范围：0 - 1）
| option.maxSum       | Number        | 696             | png 背景若不透明，则会用 232 - 242 这些安全色代替，接近纯白的颜色值有                                                                   | 243 - 255，因此考虑这些值都在排除范围之内，默认 232 * 3 = 696
| option.mimeType     | String        | 'image/png'     | 输出图片 MIME 类型，若transType 传 'blob'，则可选 'image/jpeg'
| option.transType    | String        | 'base64'        | 输出图片数据格式（可选：'base64' | 'blob' | 'all'）
| option.callback     | Function      | undefined       | 回调函数