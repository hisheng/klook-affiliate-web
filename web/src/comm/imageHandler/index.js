import html2canvas from "html2canvas";

/**
 * @file 此文件主要提供对图片的处理能力
 */
class _ImageHandler {
  constructor() {
    this.cvs = null;
    this.ctx = null;
    this.tasks = [];
    this.template = null; // image or canvas
    this._init();
    setTimeout(() => this._next(), 0);
  }

  /**
   * DOM to image
   *
   * @param {Node} el 需要渲染的 DOM 元素
   * @param {Object} option 配置选项
   * @param {Number} option.scale 放大倍数（范围 0 - 1，默认 1）
   * @param {Number} option.quality 输出图片质量（范围 0 - 1，默认 0.92）
   * @param {String} option.bgcolor 输出图片背景色 (例：'transparent'、'blue'、'#ffffff')
   * @param {String} option.mimeType 输出图片 MIME 类型（可选：'image/png'，默认 'image/png'）
   * @param {String} option.transType 输出图片数据格式（可选：'base64' | 'blob' | 'all'，默认 'base64'）
   * @param {Function} option.callback Callback Function
   */
  composite(el, option = {}) {
    let fn = () => {
      const {
        scale = 1,
        quality = 0.92,
        bgcolor = "transparent",
        mimeType = "image/png",
        transType = "base64",
        callback,
      } = option;
      el = el || this.template;

      if (el == null) {
        this._error(101, "composite");
        return;
      }
      // 需要在这里设置容器的宽度，否则会有右侧空白，导致图片尺寸有问题
      el.style.width = `${
        el.firstChild.clientWidth + el.lastChild.clientWidth
      }px`;
      html2canvas(el, {
        backgroundColor: bgcolor,
        // 需要用 scroll 的宽度，否则超出部分不会被截取
        width: el.scrollWidth,
        height: el.scrollHeight,
        scale,
        x: 0,
        y: 0,
      })
        .then((canvas) => {
          let initImage = new Image();
          let base64 = canvas.toDataURL(mimeType, quality);
          let transResult = base64;
          if (transType === "blob") {
            transResult = this._toBlob(transResult, mimeType);
          } else if (transType === "all") {
            transResult = {
              blob: this._toBlob(transResult, mimeType),
              base64,
            };
          }
          callback && callback(transResult);
          initImage.onload = () => {
            this.template = initImage;
            this._next();
          };
          initImage.src = base64;
        })
        .catch((err) => {
          console.error("ImageHandler Error: " + err);
        });
    };

    this.tasks.push(fn);
    return this;
  }

  /**
   * Greyscale image
   *
   * @param {Node} el 需要渲染的 DOM 元素
   * @param {Object} option 配置选项
   * @param {Number} option.quality 输出图片质量（范围 0 - 1，默认 0.92）
   * @param {String} option.mimeType 输出图片 MIME 类型（可选：'image/png' | 'image/jpeg'，默认 'image/png'）
   * @param {String} option.transType 输出图片数据格式（可选：'base64' | 'blob' | 'all'，默认 'base64'）
   * @param {Function} option.callback Callback Function
   */
  greyscale(el, option = {}) {
    let fn = () => {
      const {
        quality = 0.92,
        mimeType = "image/png",
        transType = "base64",
        callback,
      } = option;
      el = el || this.template;

      if (el == null) {
        this._error(102, "greyscale");
        return;
      }

      const { width, height } = el;

      if (!this.cvs.width || !this.cvs.height) {
        this._draw(el, width, height);
      }

      let imgData = this.ctx.getImageData(0, 0, width, height);
      let pixelData = imgData.data;

      for (let i = 0; i < pixelData.length; i += 4) {
        let sum = (pixelData[i] + pixelData[i + 1] + pixelData[i + 2]) / 3;

        pixelData[i] = sum;
        pixelData[i + 1] = sum;
        pixelData[i + 2] = sum;
      }

      this.ctx.putImageData(imgData, 0, 0);
      this.template = this.cvs;
      callback && callback(this._transform(transType, mimeType, quality));
      this._next();
    };

    this.tasks.push(fn);
    return this;
  }

  /**
   * Binarize image
   *
   * @param {Node} el 需要渲染的 DOM 元素
   * @param {Object} option 配置选项
   * @param {Number} option.quality 输出图片质量（范围 0 - 1，默认 0.92）
   * @param {Number} option.threshold 阈值 （范围：0 - 255，不传则会自动计算阈值）
   * @param {Number} option.mode 输出图片的模式（可选：1 - 黑底白图 | 2 - 白底黑图，默认 1）
   * @param {Boolean} option.isTransparent 输出图片是否背景透明，默认 true
   * @param {String} option.mimeType 输出图片 MIME 类型（可选：'image/png' | 'image/jpeg'，默认 'image/png'）
   * @param {String} option.transType 输出图片数据格式（可选：'base64' | 'blob' | 'all'，默认 'base64'）
   * @param {Array} option.specialValues 对某些色值做特殊处理，默认 []
   * @param {Function} option.callback Callback Function
   */
  binarize(el, option = {}) {
    let fn = () => {
      const {
        quality = 0.92,
        mode = 1,
        isTransparent = true,
        threshold,
        mimeType = "image/png",
        transType = "base64",
        specialValues = [],
        callback,
      } = option;
      el = el || this.template;

      if (el == null) {
        this._error(102, "binarize");
        return;
      }

      const { width, height } = el;

      if (!this.cvs.width || !this.cvs.height) {
        this._draw(el, width, height);
      }

      let imgData = this.ctx.getImageData(0, 0, width, height);
      let pixelData = imgData.data;

      for (let i = 0; i < pixelData.length; i += 4) {
        let sum = (pixelData[i] + pixelData[i + 1] + pixelData[i + 2]) / 3;

        pixelData[i] = sum;
        pixelData[i + 1] = sum;
        pixelData[i + 2] = sum;
      }
      let auto_threshold = 255 - this._OTSU(imgData);
      let real_threshold = threshold || auto_threshold;

      for (let i = 0; i < pixelData.length; i += 4) {
        let R = pixelData[i];
        let lVal, rVal;
        if (mode === 1) {
          lVal = 255;
          rVal = 0;
        } else {
          lVal = 0;
          rVal = 255;
        }
        let val = R < real_threshold || specialValues.includes(R) ? lVal : rVal;

        pixelData[i] = val;
        pixelData[i + 1] = val;
        pixelData[i + 2] = val;
        if (isTransparent) {
          pixelData[i + 3] = val === rVal ? 0 : pixelData[i + 3];
        }
      }
      this.ctx.putImageData(imgData, 0, 0);
      this.template = this.cvs;
      callback && callback(this._transform(transType, mimeType, quality));
      this._next();
    };

    this.tasks.push(fn);
    return this;
  }

  /**
   * Clip and transparency image
   *
   * @param {Node} el 需要渲染的 DOM 元素
   * @param {Object} option 配置选项
   * @param {Number} option.quality 输出图片质量（范围 0 - 1，默认 0.92）
   * @param {Number} option.maxSum png 背景若不透明，则会用 232 - 242 这些安全色代替，接近纯白的颜色值有 243 - 255，因此考虑这些值都在排除范围之内，默认 232 * 3 = 696
   * @param {String} option.mimeType 输出图片 MIME 类型（可选：'image/png' | 'image/jpeg'，默认 'image/png'）
   * @param {String} option.transType 输出图片数据格式（可选：'base64' | 'blob' | 'all'，默认 'base64'）
   * @param {Function} option.callback Callback Function
   */
  clip(el, option = {}) {
    let fn = () => {
      const {
        quality = 0.92,
        maxSum = 696,
        mimeType = "image/png",
        transType = "base64",
        callback,
      } = option;
      el = el || this.template;

      if (el == null) {
        this._error(102, "clip");
        return;
      }

      const { width, height } = el;

      if (!this.cvs.width || !this.cvs.height) {
        this._draw(el, width, height);
      }

      let imgData = this.ctx.getImageData(0, 0, width, height),
        pixelData = imgData.data;
      let l_offset = width,
        t_offset = height,
        r_offset = 0,
        b_offset = 0;

      for (let i = 0; i < width; i++) {
        for (let j = 0; j < height; j++) {
          let pos = (i + j * width) * 4;
          let sum = pixelData[pos] + pixelData[pos + 1] + pixelData[pos + 2];
          if (sum < maxSum) {
            l_offset = Math.min(i, l_offset);
            t_offset = Math.min(j, t_offset);
            r_offset = Math.max(i, r_offset);
            b_offset = Math.max(j, b_offset);
          } else {
            pixelData[pos + 3] = 0;
          }
        }
      }
      l_offset++;
      r_offset++;
      t_offset++;
      b_offset++;
      this.cvs.width = r_offset - l_offset;
      this.cvs.height = b_offset - t_offset;
      this.ctx.putImageData(
        imgData,
        -l_offset,
        -t_offset,
        l_offset,
        t_offset,
        r_offset - l_offset,
        b_offset - t_offset
      );
      this.template = this.cvs;
      callback && callback(this._transform(transType, mimeType, quality));
      this._next();
    };

    this.tasks.push(fn);
    return this;
  }

  // 大津算法，用于计算出最佳阈值
  _OTSU(imageData) {
    let histogram = new Array(256); // 灰度直方图
    let prob = new Array(256); // 概率密度
    let canvasData = imageData;
    let pixels = canvasData.data;
    let size = canvasData.width * canvasData.height;
    let threshold = 0,
      M = 0,
      cum = new Array(256),
      mean = new Array(256);

    for (let i = 0; i < 256; i++) {
      histogram[i] = 0;
      prob[i] = 0;
    }

    for (let i = 0; i < pixels.length; i += 4) {
      let r = pixels[i];
      histogram[r]++;
    }

    for (let i = 0; i < 256; i++) {
      prob[i] = histogram[i] / size;
    }

    cum[0] = prob[0];
    mean[0] = 0;
    for (let i = 1; i < 256; i++) {
      cum[i] = cum[i - 1] + prob[i];
      mean[i] = mean[i - 1] + i * prob[i];
    }
    M = mean[255];

    let max_ICV = 0;
    for (let i = 1; i < 256; i++) {
      let PA = cum[i],
        PB = 1 - PA,
        ICV = 0;

      if (PA != 0 && PB != 0) {
        let MA = mean[i] / PA,
          MB = (M - mean[i]) / PB;

        ICV = PA * (MA - M) * (MA - M) + PB * (MB - M) * (MB - M);

        if (ICV > max_ICV) {
          max_ICV = ICV;
          threshold = i;
        }
      }
    }
    return threshold;
  }

  // init canvas and context
  _init() {
    this.cvs = document.createElement("canvas");
    this.ctx = this.cvs.getContext("2d");
    this.cvs.width = 0;
    this.cvs.height = 0;
  }

  _transform(transType, mimeType, quality) {
    let base64 = this.cvs.toDataURL(mimeType, quality),
      transResult;

    switch (transType) {
      case "base64":
        transResult = base64;
        break;
      case "blob":
        transResult = this._toBlob(base64, mimeType);
        break;
      case "all":
        transResult = {
          blob: this._toBlob(base64, mimeType),
          base64,
        };
        break;
      default:
        transResult = base64;
        break;
    }
    return transResult;
  }

  // base64 to blob
  _toBlob(base64, mimeType) {
    if (!base64) {
      this._error(103, "_toBlob");
      return;
    }

    const bytes = window.atob(base64.split(",")[1]);
    const length = bytes.length;
    const ab = new ArrayBuffer(length);
    const ia = new Uint8Array(ab);

    for (let i = 0; i < length; i++) {
      ia[i] = bytes.charCodeAt(i);
    }
    return new Blob([ia], { type: mimeType, encoding: "utf-8" });
  }

  // draw image for canvas
  _draw(el, width, height) {
    this.cvs.width = width;
    this.cvs.height = height;
    this.ctx.drawImage(el, 0, 0, width, height);
  }

  _next() {
    let fn = this.tasks.shift();
    fn && fn();
  }

  _error(type, fname) {
    let prefix = "ImageHandler Error: ";
    let errorsMap = {
      101: `the "el" parameter of the ${fname} function must be a image or Node.`,
      102: `the "el" parameter of the ${fname} function must be a image.`,
      103: `the "base64" parameter of the ${fname} function must be a string.`,
    };

    console.error(prefix + errorsMap[type]);
  }
}

function ImageHandler() {
  return new _ImageHandler();
}

export default ImageHandler;
