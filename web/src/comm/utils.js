import { i18n } from "@/desktop/js/i18n";

function isEnglish(value) {
  const str = String(value).trim();
  const reg = new RegExp(/^[A-Za-z0-9 ,.'-]+$/);
  return reg.test(str);
}

function shouldEditWebsite(data) {
  let info = data ? data : USER_INFO;
  if (info && info.websites) {
    let index = info.websites.findIndex((item) => {
      return item.name === "" || !isEnglish(item.name);
    });
    return index !== -1;
  } else {
    return false;
  }
}

// @ts-ignore
const __ = (window.__ = function (key, args) {
  return i18n.t(key, args); // 太多地方直接使用__处理多语言，此处做个兼容
  // var key = arguments[0]
  // var txt_resource = window.jsLocale && jsLocale[key]
  // if (txt_resource === undefined) {
  //   txt_resource = key
  // }
  // var args = Array.prototype.slice.call(arguments, 1)
  // //支持{0},{1}这种格式
  // return strformat(txt_resource, args)
});

export { __, shouldEditWebsite };
