import Vue from "vue";
const vueErrHandler = require("./monitor").vueErrHandler;
const sendAffiliateLog = require("./monitor").sendAffiliateLog;

export const ClientMonitor = (vue_type, error_type) => {
  Vue.config.errorHandler = (err: Error, vm: any, info: string) => {
    if (klook.isDev()) {
      console.error(err);
    }
    klook.logger.handlerError(err, { type: "vue" });
    vueErrHandler(vue_type || "IFRAME_RENDER_VUE_ERROR", err, "", info); //减少上报信息
  };

  window.onerror = (
    msg: string | Event,
    url: string | undefined,
    lineNo: number | undefined,
    columnNo: number | undefined,
    error: Error | undefined
  ) => {
    sendAffiliateLog(
      error_type || "IFRAME_RENDER_JS_ERROR",
      msg,
      url,
      lineNo,
      columnNo,
      error
    ); //减少上报信息
  };
};
