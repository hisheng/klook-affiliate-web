import KlkCheckbox from "./src/checkbox.vue";
import KlkCheckboxGroup from "./src/checkbox_group.vue";

export { KlkCheckbox, KlkCheckboxGroup };
