### 使用方法：
```js
// 方法一
this.$Message({
    type: 'info',       // 弹框类型 可选：info(默认) | success | warning
    duration: 2000,     // 持续时间，默认 2000ms
    content: '',        // 弹框内容
    showClose: true,    // 是否显示关闭按钮，默认 true
    isCenter: true,     // 是否居中，默认 true
});

// 方法二
this.$Message.info('xxxxx');
```