import Vue from "vue";
import VueI18n from "vue-i18n";

Vue.use(VueI18n);

export const i18n = new VueI18n({
  locale: "en",
  messages: {},
  silentTranslationWarn: true,
});

export const loadLanguageAsync = (lang) => {
  return import(
    /* webpackChunkName: "lang-[request]" */ `../../../../locales/${lang}.json`
  ).then((msg) => {
    i18n.setLocaleMessage(lang, msg);
    i18n.locale = lang;
    document.querySelector("html").setAttribute("lang", lang);
    return Promise.resolve();
  });
};
