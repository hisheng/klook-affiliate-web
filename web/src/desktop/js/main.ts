require("../css/main.scss");
import Vue from "vue";
import App from "./app.vue";
// const App = require("./app.vue");
const createStore = require("./store").createStore;
import { ClientMonitor } from "./clientMonitor";
import { router } from "./router.js";
import { i18n, loadLanguageAsync } from "./i18n.js";

const store = createStore();

ClientMonitor("AFF_PORTAL_VUE_ERROR", "AFF_PORTAL_JS_ERROR");

Vue.config.devtools = process.env.NODE_ENV == "dev";

store
  .dispatch({
    type: "getExperimentsHitList",
  })
  .then((_) => {
    loadLanguageAsync(window.KLK_LANG || "en").then(() => {
      new Vue({
        store,
        i18n,
        router: router,
        el: "#app",
        render: function (h: any) {
          return h(App);
        },
      });
    });
  });
