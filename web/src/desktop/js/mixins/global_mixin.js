// var common_data = require('common_data')
var mixin = function () {
  return {
    created() {
      //Disabling data reactivity also avoids the performance cost of converting data into reactive objects.
      // if(!this.common_data){//children components auto inject common_data
      //     this.common_data = common_data;
      // }
    },
    methods: {
      handlePageSizeChange(newVal) {
        //由于pagination的page_size_opts在SS-38需求才加入，所以会影响之前所有的，于是做了这个折中全局绑定方案
        if (this.pagination && this.pagination.pageSize) {
          this.pagination.pageSize = newVal;
          // this.loadData && this.loadData();
        }
      },
      // $t(key) {//lang translate vue
      //      const otherArguments = Array.prototype.slice.call(arguments, 1);
      //      return __(key, otherArguments);
      //  }
    },
  };
};

const global_mixin = mixin();
export { global_mixin };
