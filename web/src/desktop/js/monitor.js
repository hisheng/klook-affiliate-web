let errorLastSendTime = {};

//生成32位的hash
function stringHashCode() {
  let hash = 0;
  if (this.length === 0) return hash;
  for (let i = 0; i < this.length; i++) {
    const c = this.charCodeAt(i);
    hash = (hash << 5) - hash + c;
    hash = hash & hash; // Convert to 32bit integer
  }
  return hash;
}

/**
 *  为ErrorObj生成唯一的key, 来判断这个key是否重复发送过
 */
function getErrorObjKey(errorObj) {
  let strs = [];
  const keys = Object.keys(errorObj).sort();
  for (let i in keys) {
    strs.push(i + "=" + errorObj[i]);
  }
  return stringHashCode.call(strs.join(""));
}

const vueErrHandler = (type, err, vm, info) => {
  let {
    message, // 异常信息
    name, // 异常名称
  } = err;
  sendAffiliateLog(type, `${name}:${message};${info}`, "", "", "", err);
};

const sendAffiliateLog = (type, msg, url, lineNo, columnNo, err) => {
  if (process.env.NODE_ENV !== "production") {
    return;
  }
  let main_url = "";

  try {
    main_url = document.referrer;
  } catch (e) {}
  const post_url = "/v1/affnode/error_msg",
    loc = window.location,
    errorObj = {
      type: type || "js_err",
      page_query: loc.search,
      page_url: loc.protocol + "//" + loc.host + loc.pathname,
      err_msg: msg,
      script_url: url,
      line_number: lineNo,
      column: columnNo,
      error_obj: err,
      main_url,
      user_agent: window.navigator.userAgent,
    };
  var filterList = ["ResizeObserver loop limit exceeded", "Script error."];
  //排除三方引入的js 上报log
  if (errorObj.err_msg && filterList.includes(errorObj.err_msg)) {
    return;
  }
  //有一些error会很快的重复出现, 这里为了减少发送
  const key = getErrorObjKey(errorObj);
  const t = Date.now();
  const lastSendTime = errorLastSendTime[key] || 0;
  if (t - lastSendTime > 60 * 1000) {
    errorLastSendTime[key] = t;
    const xhr = new XMLHttpRequest();
    xhr.open("POST", post_url);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify(errorObj));
  } else {
    console.log("same error log send too frequent!");
  }
};

export { vueErrHandler, sendAffiliateLog };
