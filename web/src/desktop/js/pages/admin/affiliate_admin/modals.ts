export type number_or_undefined_ts = number | undefined;
export type bonus_setting_list_item_ts = [number, number];

export type sales_bottom_or_top_ts = "sales_bottom" | "sales_top";

export type cols_item_ts = {
  width?: string;
  label: string;
};
export type level_data_ts = {
  level: string;
  sales_bottom: number;
  sales_top: number;
  additional_bonus_commission: number;
};
export type bonus_setting_list_ts = bonus_setting_list_item_ts[];
