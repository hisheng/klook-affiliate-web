import Vue from "vue";

type vue_ts = typeof Vue;

export interface bank_info_ts {
  bank_name_branch_name?: string;
  bank_name?: string;
  branch_code?: string;
  bank_account_name?: string;
  bank_account_id?: string;
  swift_code?: string;
  iban_number?: string;
  sort_code?: string;

  [index: string]: any;
}

export interface form_data_ts extends bank_info_ts {
  country_code: string;
}

export type AREA_BANK_MAP_item_ts = {
  label: string;
  name: string;

  [index: string]: string;
};

export type AREA_BANK_MAP_ts = {
  [index: string]: Array<AREA_BANK_MAP_item_ts>;
};

export type payment_method_map_view_ts = {
  [index: string]: typeof Vue;
  alipay: typeof Vue;
  paypal: typeof Vue;
  bank: typeof Vue;
};

// export type payment_method_map_view_ts extends Record<'alipay' | 'paypal' | 'bank', typeof Vue>
