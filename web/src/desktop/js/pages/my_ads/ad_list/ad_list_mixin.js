import { urlObj } from "common_data";
import moment from "moment";
import { RENDER_CONFIG } from "@/config";
export default {
  data() {
    // 当前为月份第一天时, 默认取上个月第一天. 否则为当前月第一天
    let start =
      moment().date() === 1
        ? moment().subtract(1, "month").startOf("month")
        : moment().startOf("month");
    return {
      RENDER_CONFIG,
      uid: "",
      user_name: "",
      search_options: {
        date_range: [start, moment().subtract(1, "day")],
        search_content: "",
        ad_type: "all",
      },
      ad_type_list: [
        {
          id: 1,
          label: __("MULTIPLE_all"),
          value: "all",
        },
        {
          id: 2,
          label: __("MULTIPLE_text_links"),
          value: "link",
        },
        {
          id: 3,
          label: __("AD_LIST_MIXIN_placement_link"),
          value: "placement_link",
        },
        {
          id: 4,
          label: __("MULTIPLE_dynamic_widgets"),
          value: "widget",
        },
        {
          id: 5,
          label: __("MULTIPLE_static_banners"),
          value: "banner",
        },
        {
          id: 6,
          label: __("MULTIPLE_activity_banners"),
          value: "activity",
        },
        {
          id: 7,
          label: __("MULTIPLE_search_box"),
          value: "search",
        },
        {
          id: 8,
          label: __("MULTIPLE_auto_dynamic_widget"),
          value: "auto_widget",
        },
      ],
      pagination: {
        total: 0,
        pageSize: 20,
        curPage: 1,
      },
      table_list: [],
      is_loading_shown: false,
      datepicker_lang: KLK_LANG,
    };
  },
  computed: {
    getAdType() {
      return function (type) {
        let type_map = {
          link: __("MULTIPLE_text_links"),
          placement_link: __("AD_LIST_MIXIN_placement_link"),
          dynamic_widget: __("MULTIPLE_dynamic_widgets"),
          banner: __("MULTIPLE_static_banners"),
          activity: __("MULTIPLE_activity_banners"),
          search_vertical: __("MULTIPLE_search_box"),
          search: `${__("MULTIPLE_search_box")}(old version)`, // 旧的search
          auto_dynamic_widget: __("MULTIPLE_auto_dynamic_widget"),
          listing_link: __("MULTIPLE_text_links"), // 新增 link
          city_link: __("MULTIPLE_text_links"),
          ...this.adsTypeI18Name(RENDER_CONFIG),
        };
        return type_map[type];
      };
    },
  },
  methods: {
    handleCurrentChange(curPage) {
      this.pagination.curPage = curPage;
      this.loadData();
    },
    fetchAdType(uid) {
      klook.ajaxGet(urlObj.ad_type_map, { uid }, (resp) => {
        if (resp.success) {
          const adTypes = resp.result || [];
          this.ad_type_list = this.ad_type_list.filter((item) => {
            return adTypes.includes(item.id);
          });
        }
      });
    },
    // 通过 ads_type 来加载对应的模版类型，用于node handlerbar 模版
    adsTypeI18Name(data) {
      let obj = {};
      data.forEach((vertical) => {
        vertical.render_map.forEach((item) => {
          item.info.type.forEach((ads_type) => {
            obj[ads_type.name] = __(item.i18Key);
          });
        });
      });
      return obj;
    },
    loadData() {
      function processData(ads_list, user) {
        return ads_list.map((item) => {
          return {
            adid: item.adid,
            ad_type: item.ad_type,
            pid: item.pid,
            sid: item.sid,
            placement_labels: [
              {
                key: __("MULTIPLE_label_1"),
                value: item.label1,
              },
              {
                key: __("MULTIPLE_label_2"),
                value: item.label2,
              },
              {
                key: __("MULTIPLE_label_3"),
                value: item.label3,
              },
            ],
            impression: item.impression,
            click: item.click,
            ctr: item.CTR,
            paid_order: item.paid_order,
            shared_amount: user.prefer_currency + " " + item.commission,
            epc: user.prefer_currency + " " + item.EPC,
          };
        });
      }

      const { search_content, ad_type } = this.search_options;
      const { curPage, pageSize } = this.pagination;
      const params = {
        uid: this.uid,
        start: moment(this.search_options.date_range[0]).format("YYYYMMDD"),
        end: moment(this.search_options.date_range[1]).format("YYYYMMDD"),
        page: curPage,
        pcount: pageSize,
      };
      if (search_content !== "") {
        params.pid = search_content;
      }
      if (ad_type !== "all") {
        params.ad_type = ad_type;
      }
      this.is_loading_shown = true;
      klook.ajaxGet(urlObj.get_ad_performance, params, (resp) => {
        if (resp.success) {
          const { ads, user, total } = resp.result;
          this.table_list = processData(ads, user);
          this.pagination.total = total;
          this.is_loading_shown = false;
        } else {
          this.$Message.warning(resp.error.message);
        }
      });
    },
    exportFile() {
      const { search_content, ad_type } = this.search_options;
      const start = moment(this.search_options.date_range[0]).format(
        "YYYYMMDD"
      );
      const end = moment(this.search_options.date_range[1]).format("YYYYMMDD");
      // 传给后端的多语言文案
      const title_map = {
        adid_locale: __("MULTIPLE_adid"),
        ad_type_locale: __("MULTIPLE_ad_type"),
        pid_locale: __("MULTIPLE_pid"),
        sid_locale: __("MULTIPLE_sid"),
        placement_label_locale: __("MULTIPLE_placement_labels"),
        impression_locale: __("MULTIPLE_impressions"),
        click_locale: __("MULTIPLE_clicks"),
        CTR_locale: __("MULTIPLE_ctr"),
        paid_order_locale: __("MULTIPLE_paid_orders"),
        commission_locale: __("MULTIPLE_shared_amount"),
        EPC_locale: __("MULTIPLE_epc"),
        placement_label1_locale: __("MULTIPLE_label_1"),
        placement_label2_locale: __("MULTIPLE_label_2"),
        placement_label3_locale: __("MULTIPLE_label_3"),
      };
      // Ad List 页面，不需要导出显示 pid 和 sid
      if (this.page_type === "ad_list") {
        title_map.pid_locale = "";
        title_map.sid_locale = "";
      }
      let url = `${urlObj.gen_ad_performance_report}?uid=${this.uid}&start=${start}&end=${end}&`;

      for (let i in title_map) {
        url += `${i}=${title_map[i]}&`;
      }
      if (search_content !== "") {
        url += `pid=${search_content}&`;
      }
      if (ad_type !== "all") {
        url += `ad_type=${ad_type}&`;
      }
      klook.downloadFile("", url);
    },
  },
};
