import { urlObj } from "common_data";
import { RENDER_CONFIG } from "@/config";
export default {
  data() {
    return {
      mode: "editing",
      uid: "",
      ad_id: "",
      loading: {
        loading_activity_list: false,
        loading_category_list: false,
        generate_code_loading: false,
      },
      RENDER_CONFIG,
      langOptions: [],
      langOptionsWidget: [],
    };
  },
  created() {
    this.ad_id = this.$route.query.ad_id || "";
    this.uid = this.$route.query.uid || USER_INFO.id;
  },
  watch: {
    form_data: {
      handler: function (newVal) {
        this.setEditingMode();
      },
      deep: true,
    },
  },
  mounted() {
    //affiliate portal 页面支持的语言
    this.langOptions = LANG_OPTIONS;
    //widget当中支持的语言
    this.langOptionsWidget = LANG_OPTIONS_WIDGET;
  },
  computed: {
    is_ad_ready_preview: function () {
      return this.ad_id && this.mode == "preview";
    },
  },
  methods: {
    setEditingMode() {
      this.mode = "editing";
    },
    setPreviewMode() {
      this.mode = "preview";
    },
    onCommonSettingChange(common_form_data) {
      this.form_data = _.extend({}, this.form_data, common_form_data);
    },
    validate(callback) {
      this.$refs["ad_form"].validate((valid, message) => {
        if (!valid) {
          this.passValidation = valid;
          this.errorMessage = this.errorMessage || message;
        } else {
          callback();
        }
      });
    },
    generateAd() {
      this.validate(() => {
        this.beforeSyncAd();
        if (this.ad_id) {
          this.updateAdv3();
        } else {
          this.createAdv3();
        }
      });
    },
    createAdv3() {
      const { websiteId, label1, label2, label3, nid } = this.form_data;
      const params = Object.assign(
        {
          uid: this.uid,
          wid: websiteId, // 在update的时候不能修改
          label1: label1,
          label2: label2,
          label3: label3,
        },
        this.generateAdInfoFormData()
      );
      // 有 nid 需要上传 nid
      if (nid) {
        params.nid = nid;
      }
      klook.ajaxPostJSON(urlObj.v3_affsrv_ad, params, (resp) => {
        if (resp.success) {
          this.ad_id = resp.result.adid;
          const query = {
            ad_id: this.ad_id,
          };
          if (this.$route.query.uid) {
            query.uid = this.uid;
          }
          this.$router.replace({ query });
          this.$nextTick(() => {
            this.afterSyncAd();
            this.renderAd && this.renderAd();
          });
        } else {
          this.$Message.warning(resp.error.message);
        }
      });
    },
    updateAdv3() {
      const params = Object.assign(
        {
          adid: this.ad_id,
          label1: this.form_data.label1,
          label2: this.form_data.label2,
          label3: this.form_data.label3,
        },
        this.generateAdInfoFormData()
      );
      klook.ajaxPutJSON(urlObj.v3_affsrv_ad, params, (resp) => {
        if (resp.success) {
          this.ad_id = resp.result.adid;
          const query = {
            ad_id: this.ad_id,
          };
          if (this.$route.query.uid) {
            query.uid = this.uid;
          }
          this.$router.replace({ query }).catch((err) => {});
          this.$nextTick(() => {
            this.afterSyncAd();
            this.renderAd && this.renderAd();
          });
        } else {
          this.$Message.warning(resp.error.message);
        }
      });
    },
    onAdInfoLoaded(adInfo) {
      if (this.ad_id) {
        // edit mode
        this.initEdit(adInfo);
        this.$nextTick(() => {
          this.setPreviewMode();
          this.renderAd && this.renderAd();
        });
      }
    },
    beforeSyncAd() {
      this.loading.generate_code_loading = true;
    },
    afterSyncAd() {
      this.setPreviewMode();
      this.loading.generate_code_loading = false;
    },
  },
};
