import moment from "moment";

export default {
  computed: {
    getAids() {
      return function (aids) {
        if (Array.isArray(aids) && aids.length) {
          let temp = aids.slice();
          if (temp.includes(0)) {
            temp = temp.slice(1);
          }
          if (!temp.length) {
            return "-";
          }
          return temp;
        }
        return "-";
      };
    },
    getType() {
      return function (type) {
        const map = {
          0: __("TABLE_COMPUTED_MIXIN_standard"),
          1: __("TABLE_COMPUTED_MIXIN_customized"),
        };
        return map[type] || "-";
      };
    },
    getFormatTime() {
      return function (time) {
        return (time && moment(time).format("YYYY-MM-DD HH:mm")) || "-";
      };
    },
    getStatus() {
      return function (state) {
        const statusMap = {
          0: __("MULTIPLE_not_set"),
          1: __("MULTIPLE_pending"),
          2: __("MULTIPLE_in_process"),
          3: __("MULTIPLE_active"),
        };
        return statusMap[state];
      };
    },
    getAction() {
      return function (operation) {
        const actionMap = {
          0: __("TABLE_COMPUTED_MIXIN_created"), // upload
          1: __("TABLE_COMPUTED_MIXIN_created"), // create
          2: __("TABLE_COMPUTED_MIXIN_created"), // reupload
          3: __("TABLE_COMPUTED_MIXIN_submitted"),
          4: __("TABLE_COMPUTED_MIXIN_rejected"),
          5: __("TABLE_COMPUTED_MIXIN_modified"),
          6: "", // approve
          7: __("TABLE_COMPUTED_MIXIN_recalled"),
        };
        return actionMap[operation] ? ` (${actionMap[operation]})` : "";
      };
    },
    getColor() {
      return function (state) {
        const colorMap = {
          0: "",
          1: "",
          2: "yellow",
          3: "lightgreen",
        };
        return colorMap[state];
      };
    },
  },
};
