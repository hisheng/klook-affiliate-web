require("../../../../css/main.scss");
const Vue = require("vue");
import { i18n, loadLanguageAsync } from "../../../i18n.js";
const Ads_demo_page = require("./ads_demo_page.vue");

loadLanguageAsync(window.KLK_LANG || "en").then(() => {
  const vue = new Vue({
    i18n,
    el: "#ads_demo_page",
    render: function (h: any) {
      return h(Ads_demo_page);
    },
  });
});
