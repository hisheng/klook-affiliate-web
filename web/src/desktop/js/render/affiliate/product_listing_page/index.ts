require('../../../../css/main.scss');
const Vue = require('vue');
import { i18n, loadLanguageAsync } from '../../../i18n.js'
import Product_listing_page from "./product_listing_page.vue";

loadLanguageAsync(window.KLK_LANG || 'en').then(() => {
    const vue = new Vue({
        i18n,
        el: '#product_listing_page',
        render: function (h: any) {
            return h(Product_listing_page);
        }
    });
});


