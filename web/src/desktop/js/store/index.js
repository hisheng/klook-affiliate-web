import Vue from "vue";
import Vuex from "vuex";
const actions = require("./actions");
const mutations = require("./mutations");
const getters = require("./getters");
const globalObjModule = require("./modules/global_obj.js");
const joinNowModule = require("./modules/join_now.js");
const accountModule = require("./modules/account.js");
const gtmEventModule = require("./modules/gtm_event.js");
const experimentModule = require("./modules/experiment.js");

Vue.use(Vuex);

export const createStore = function () {
  return new Vuex.Store({
    modules: {
      globalObjModule: globalObjModule,
      joinNowModule: joinNowModule,
      gtmEventModule: gtmEventModule,
      accountModule: accountModule,
      experimentModule: experimentModule,
    },
    actions,
    mutations,
    getters,
  });
};
