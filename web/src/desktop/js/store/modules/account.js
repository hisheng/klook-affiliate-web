// initial state
var state = {
  prefer_currency: "",
};

// getters
var getters = {};

// actions
var actions = {};

// mutations
var mutations = {
  set_prefer_currency(state, prefer_currency) {
    state.prefer_currency = prefer_currency;
  },
};

export { state, getters, actions, mutations };
