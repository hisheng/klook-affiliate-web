// initial state

import { urlObj } from "common_data";

const getTotal = () => {
  return new Promise((resolve, reject) => {
    klook
      .ajaxGet(urlObj.notification_read_totals, (resp) => {
        resolve(resp);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

const getQuestion = () => {
  return new Promise((resolve, reject) => {
    klook
      .ajaxGet(
        urlObj.feedback_get_question,
        {
          module: "overall",
          question_type: "csat",
        },
        (resp) => {
          resolve(resp);
        }
      )
      .catch((error) => {
        reject(error);
      });
  });
};

var state = {
  total: 0,
  delayTime: {},
  question: {},
};

// getters
var getters = {};

// actions
var actions = {
  async notificationTotal({ commit }) {
    const res = await getTotal();
    if (res && res.result) {
      commit("SET_TOTAL", res.result.total || 0);
    }
  },
  async initQuestion({ commit }) {
    const res = await getQuestion();
    if (res && res.result) {
      commit("SET_QUESTION", res.result || 0);
    }
  },
};

// mutations
var mutations = {
  ["SET_TOTAL"](state, total) {
    state.total = total;
  },
  ["SET_DELAY_TIME"](state, delayTime) {
    state.delayTime = delayTime;
  },
  ["SET_QUESTION"](state, question) {
    state.question = question;
  },
};

export { state, getters, actions, mutations };
