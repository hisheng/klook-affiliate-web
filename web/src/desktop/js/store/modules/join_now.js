// initial state
var state = {
  type: "",
};

// getters
var getters = {};

// actions
var actions = {};

// mutations
var mutations = {
  change_join_now_type(state, type) {
    state.type = type;
  },
};

export { state, getters, actions, mutations };
