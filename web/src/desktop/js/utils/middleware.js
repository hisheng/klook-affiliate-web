const Middleware = function () {
  this._middleware = [];
  this._composedFn = function () {};
};

Middleware.prototype = {
  use(fn) {
    this._middleware.unshift((next) => {
      return () => {
        fn(next);
      };
    });
    return this;
  },
  run(callback) {
    function composeMiddleware(callback) {
      this._composedFn = this._middleware.reduce((resultFn, curFn) => {
        return curFn(resultFn);
      }, callback || function () {});
    }

    composeMiddleware.call(this, callback);

    this._composedFn();

    return this;
  },
  clean() {
    function initMiddleware() {
      this._middleware = [];
      this._composedFn = function () {};
    }
    initMiddleware.call(this);
    return this;
  },
};

var Test = function () {
  this.name = Math.random();
};

const TestIns = new Test();

export { Middleware, TestIns as Test };
