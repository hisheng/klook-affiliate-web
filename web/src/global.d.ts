declare function __(key: string): any;
declare let require: NodeRequire;

declare let klook: {
  [index: string]: any;
};

declare let _: any;

declare type country_obj_map_ts = {
  [key: string]: {
    area: string;
    name: string;

    [index: string]: any;
  };
};

declare type resp_ts = {
  [key: string]: any;
  error: {
    [key: string]: any;
    code: number;
    message: string;
  };
  result: {
    [key: string]: any;
  };
};

interface Window {
  dataLayer: Array<any>;
  [key: string]: any;
}

declare module "common_data";
declare module "lodash";
declare module "url";
