declare module "*.vue" {
  import Vue from "vue";
  export default Vue;
}

// 1. 确保在声明补充的类型之前导入 'vue'
import Vue, { VNode } from "vue";

interface validateFn {
  (fn: (valid: boolean, msg: string) => void): void;
}
// 2. 定制一个文件，设置你想要补充的类型
//    在 types/vue.d.ts 里 Vue 有构造函数类型
declare module "vue/types/vue" {
  // 3. 声明为 Vue 补充的东西
  interface Vue {
    $Modal: {
      [index: string]: any;
    };
    $store: {
      [index: string]: any;
    };
    $renderStore: {
      [index: string]: any;
    };

    //以下是由于没想到如何解决$refs的问题而做的临时patch   在financial_info 的$refs问题
    form_data: {
      [index: string]: any;
    };
    validate: validateFn; //  属于KlkForm 的方法，
    // $refs: {
    //     [key: string]: Vue | Element | Vue[] | Element[];
    // }
  }
}
