// const fs = require("fs");
const path = require("path");
const webpack = require("webpack");
const { BundleAnalyzerPlugin } = require("webpack-bundle-analyzer");

module.exports = {
  devServer: {
    port: 8786,
  },
  publicPath: "/static/web",
  outputDir: path.join(__dirname, "../server/app/public/web"),
  css: {
    loaderOptions: {
      sass: {
        prependData: `@import "~@/desktop/css/vars.scss"`,
      },
      scss: {
        prependData: `@import "~@/desktop/css/vars.scss";`,
      },
    },
  },
  configureWebpack: {
    context: __dirname,
    resolve: {
      alias: {
        jquery: __dirname + "/node_modules/jquery/dist/jquery.min.js",
        klook: __dirname + "/src/comm/core.js",
        common_data: __dirname + "/src/comm/common_data.js",
      },
    },
    plugins: [
      new webpack.ProvidePlugin({
        _: "underscore",
        klook: ["klook", "default"],
        $: "jquery",
      }),
      // new BundleAnalyzerPlugin(),
    ],
    performance: {
      maxEntrypointSize: 2 * 1024 * 1024,
      maxAssetSize: 2 * 1024 * 1024,
    },
  },
  chainWebpack: (config) => {
    const iconsPath = path.resolve(
      __dirname,
      "./src/desktop/imgs/font-icon-svg"
    );
    config.module.rule("svg").exclude.add(iconsPath).end();

    config.module
      .rule("svg-sprite")
      .test(/\.svg$/)
      .include.add(iconsPath)
      .end()
      .use("svg-sprite-loader")
      .loader("svg-sprite-loader");

    config.plugin("html").tap((args) => {
      args[0].minify = false;
      args[0].template = path.join(__dirname, "public/index.njk");
      return args;
    });

    // saveConfigAndExit(config);
  },
};

// function saveConfigAndExit(config) {
//   const conf = config.toConfig();
//   conf.module.rules.forEach((r) => {
//     if (r.test) {
//       r.test = r.test.source;
//     }
//   });
//   fs.writeFileSync(
//     path.join(__dirname, "./webpack-config.js"),
//     JSON.stringify(conf, null, 2)
//   );
//   // process.exit(1);
// }
