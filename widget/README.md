# affiliate-widgets

affiliate 广告小部件

## widgets

- Static Banners
- Activity Banners
- Text Links
- Search Box
- Promo Codes
- Dynamic Widgets

## widget prod

- static_widget
- dynamic_widget
- banner
- search_banner
- mul_act
- single_act
- app

## iframe render url

host: dataHost || https://affiliate.klook.com

- ${host}/v1/affnode/render: auto_dynamic_widget, dynamic_widget, auto_hotel_dynamic_widget, hotel_dynamic_widget, mul_act_banner, search_vertical, single_act_banner, static_banner
- ${host}/v3/affsrv/widget_template: ['static_widget', 'dynamic_widget']
- ${host}/s/widgets/banner_v2: static_banner, Activity Banners

## iframe 脚本

Static Banners, Activity Banners => affiliate_base_v3.2

Static Banners:

```html
<ins
  class="klookaff"
  data-wid="17012"
  data-bgtype="Experience"
  data-adid="108547"
  data-lang="en"
  data-prod="banner"
  data-width="120"
  data-height="600"
>
  <a href="//www.klook.com/?aid=">Klook.com</a>
</ins>
<script type="text/javascript">
  (function (d, sc, u) {
    var s = d.createElement(sc),
      p = d.getElementsByTagName(sc)[0];
    s.type = "text/javascript";
    s.async = true;
    s.src = u;
    p.parentNode.insertBefore(s, p);
  })(
    document,
    "script",
    "//affiliate48.fat.klook.io/s/dist/desktop/affiliate_base_v3.2.js"
  );
</script>
```

Activity Banners:

```html
<ins
  class="klookaff"
  data-wid="17012"
  data-adid="108548"
  data-actids="33229,3634,33321"
  data-prod="mul_act"
  data-price="false"
  data-lang=""
  data-width="160"
  data-height="600"
  data-currency=""
  ><a href="//www.klook.com/">Klook.com</a></ins
>
<script type="text/javascript">
  (function (d, sc, u) {
    var s = d.createElement(sc),
      p = d.getElementsByTagName(sc)[0];
    s.type = "text/javascript";
    s.async = true;
    s.src = u;
    p.parentNode.insertBefore(s, p);
  })(
    document,
    "script",
    "//affiliate48.fat.klook.io/s/dist/desktop/affiliate_base_v3.2.js"
  );
</script>
```

Dynamic Widgets => dynamic_widget_v3.js

```html
<ins
  class="things_to_do_dynamic_widget"
  data-host="https://affiliate48.fat.klook.io"
  data-adid="108549"
  data-lang=""
  data-currency=""
  data-cardH="126"
  data-padding="92"
  data-lgH="470"
  data-edgeValue="655"
  data-cid="2"
  data-tid="-1"
  data-amount="3"
  data-prod="dynamic_widget"
  ><a href="//www.klook.com/">Klook.com</a></ins
>
<script type="text/javascript">
  (function (d, sc, u) {
    var s = d.createElement(sc),
      p = d.getElementsByTagName(sc)[0];
    s.type = "text/javascript";
    s.async = true;
    s.src = u;
    p.parentNode.insertBefore(s, p);
  })(
    document,
    "script",
    "//affiliate48.fat.klook.io/s/dist/desktop/dynamic_widget_v3.js"
  );
</script>
```

Dynamic Widgets(manual) => dynamic_widget_v3.js

```html
<ins
  class="things_to_do_dynamic_widget"
  data-adid="563053"
  data-lang=""
  data-currency=""
  data-cardH="126"
  data-padding="92"
  data-lgH="470"
  data-edgeValue="655"
  data-prod="static_widget"
  data-amount="1"
  ><a href="//www.klook.com/">Klook.com</a></ins
>
<script type="text/javascript">
  (function (d, sc, u) {
    var s = d.createElement(sc),
      p = d.getElementsByTagName(sc)[0];
    s.type = "text/javascript";
    s.async = true;
    s.src = u;
    p.parentNode.insertBefore(s, p);
  })(
    document,
    "script",
    "https://cdn.klook.com/s/dist_web/klook-affiliate-front/s/dist/desktop/dynamic_widget_v3.js"
  );
</script>
```

Dynamic Widgets(hotel) => hotel_dynamic_widget_v3.js

```html
<ins
  class="klook_hotel_dynamic_widget"
  data-host="https://affiliate48.fat.klook.io"
  data-adid="108551"
  data-lang=""
  data-currency=""
  data-cardH="126"
  data-padding="92"
  data-lgH="470"
  data-edgeValue="655"
  data-cid="2"
  data-tid=""
  data-amount="3"
  data-prod="hotel_dynamic_widget"
  ><a href="//www.klook.com/">Klook.com</a></ins
>
<script type="text/javascript">
  (function (d, sc, u) {
    var s = d.createElement(sc),
      p = d.getElementsByTagName(sc)[0];
    s.type = "text/javascript";
    s.async = true;
    s.src = u;
    p.parentNode.insertBefore(s, p);
  })(
    document,
    "script",
    "//affiliate48.fat.klook.io/s/dist/desktop/hotel_dynamic_widget_v3.js"
  );
</script>
```

Dynamic Widgets(hotel, manual) => hotel_dynamic_widget_v3.js

```html
<ins
  class="klook_hotel_dynamic_widget"
  data-adid="563054"
  data-lang=""
  data-currency=""
  data-cardH="126"
  data-padding="92"
  data-lgH="470"
  data-edgeValue="655"
  data-prod="hotel_static_widget"
  data-amount="1"
  ><a href="//www.klook.com/">Klook.com</a></ins
>
<script type="text/javascript">
  (function (d, sc, u) {
    var s = d.createElement(sc),
      p = d.getElementsByTagName(sc)[0];
    s.type = "text/javascript";
    s.async = true;
    s.src = u;
    p.parentNode.insertBefore(s, p);
  })(
    document,
    "script",
    "https://cdn.klook.com/s/dist_web/klook-affiliate-front/s/dist/desktop/hotel_dynamic_widget_v3.js"
  );
</script>
```

Search Box => search_vertical_v3

```html
<ins
  class="klook_aff_search_box"
  data-host="https://affiliate48.fat.klook.io"
  data-wid="17012"
  data-height="340px"
  data-adid="108553"
  data-lang=""
  data-prod="search_vertical"
  data-currency=""
  ><a href="//www.klook.com/?aid=">Klook.com</a></ins
>
<script type="text/javascript">
  (function (d, sc, u) {
    var s = d.createElement(sc),
      p = d.getElementsByTagName(sc)[0];
    s.type = "text/javascript";
    s.async = true;
    s.src = u;
    p.parentNode.insertBefore(s, p);
  })(
    document,
    "script",
    "//affiliate48.fat.klook.io/s/dist/desktop/search_vertical_v3.js"
  );
</script>
```
