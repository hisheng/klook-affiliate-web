export interface detail_type {
  card_tags: {
    bestseller: string;
    percentage_off: string;
  };
  id: number;
  template_id: number;
  title: string;
  subtitle: string;
  url_seo: string;
  city_id: number;
  city_name: string;
  instance: number;
  image_url: string;
  score: number;
  currency: string;
  market_price: string;
  market_price_format: string;
  sell_price: string;
  sell_price_format: string;
  start_time: string;
  sold_out: Boolean;
  published: Boolean;
  participate: number;
  participants_format: string;
  hot_state: string;
  discount: number;
  review_total: number;
  need_hide_review: boolean;
  image_url_host: string;
  country_id: number;
  country_name: string;
  jump_url: string;
  affiliate_url: string;
  klook_url: string;
  lang: string;

  [key: string]: any;
}

export interface show_type {
  klook_url: string;
  affiliate_url: string;
  lang: string;
  iskoa: boolean;
  view_more_text: string;
  activity_reviews: string;

  [key: string]: any;
}
