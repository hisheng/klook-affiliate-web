interface Window {
  dataLayer: Array<any>;
  [key: string]: any;
}

declare function __(key: string): any;
// declare let require: NodeRequire;

declare let klook: {
  [index: string]: any;
};

declare let _: any;

interface Window {
  dataLayer: Array<any>;
  [key: string]: any;
}

declare module "common_data";
declare module "lodash";
declare module "url";
