# 广告组件 iframe 初始化脚本

当前我们投放广告的方式是通过以下的 html 片段：

(Static Banners)

```html
<ins
  class="klookaff"
  data-wid="17012"
  data-bgtype="Experience"
  data-adid="108547"
  data-lang="en"
  data-prod="banner"
  data-width="120"
  data-height="600"
>
  <a href="//www.klook.com/?aid=">Klook.com</a>
</ins>
<script type="text/javascript">
  (function (d, sc, u) {
    var s = d.createElement(sc),
      p = d.getElementsByTagName(sc)[0];
    s.type = "text/javascript";
    s.async = true;
    s.src = u;
    p.parentNode.insertBefore(s, p);
  })(
    document,
    "script",
    "//affiliate48.fat.klook.io/s/dist/desktop/affiliate_base_v3.2.js"
  );
</script>
```

其流程原理：

1. 将用户配置的信息存储在 ins 标签的 data 属性上
2. script 加载初始化脚本
3. 初始化脚本执行时：读取 ins 上的 class 以及 data 属性，构造一个 iframe 插入 ins 中
4. iframe 向服务端发起对特定 widget 的渲染

由于历史原因，旧的方案中，widget 投放存在以下问题：

- 同一个初始化脚本存在多个版本
- 不同类型的 widget 使用不同的初始化脚本

给开发带来管理混乱，维护困难的弊病

在新的 affiliate 设计中，我们希望所有的 widget 都使用同一份 iframe 初始化脚本，达到以下效果：

- 兼容旧的、已投放 widget 片段的 script url (通过服务端 302 重定向实现)
- 统一 widget 的渲染逻辑，以及监控/错误上报逻辑等
- 一份 iframe 初始化脚本可以对页面上所有 widget 进行渲染
- 不同 widget 的差异化也在同一份 iframe 初始化脚本中处理，不再因此衍生出不同的 iframe 初始化脚本
- 去除版本的概念，不需要对历史版本继续维护，只需要保证向前兼容即可

为达到以上效果，我们需要对 iframe 初始化脚本编写完整的单元测试等，以保证其逻辑、流程完备性，以及向前的兼容性