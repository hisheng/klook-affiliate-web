// iframe 初始化脚本
interface DataSet {
  [attr: string]: string;
}

interface IframeItem {
  el: HTMLIFrameElement;
  dataset: DataSet;
  host: string;
  prod: string; // 广告组件类型
  success: boolean; // 是否渲染成功
  isDev: boolean; // 是否非线上环境
}

const IFRAME_STATUS = {
  INIT: 0,
  SUCCESS: 1,
  FAILED: 2,
};

// 默认产线地址
const DEFAULT_HOST = "https://affiliate.klook.com";

export default class BaseIframe {
  className: string;
  attrCb?: Function;
  iframeItems: {
    [className: string]: IframeItem;
  };

  constructor(className: string, attrCb?: Function) {
    this.className = className;
    this.attrCb = attrCb;
    this.iframeItems = {};
  }

  init() {
    const onMessage = this.onMessage.bind(this);
    window.addEventListener("message", onMessage, false);
    // 获取页面上所有的 ins 元素
    const elements = document.getElementsByClassName(
      this.className
    ) as HTMLCollectionOf<HTMLModElement>;
    Array.from(elements).forEach((el: HTMLModElement) => {
      this.initWidget(el);
    });
  }

  // 回调函数
  onMessage(event: MessageEvent) {
    const { type, content = {} } = event.data;
    const iframeItem = this.iframeItems[content.renderID];
    // 接收到 iframe 内页面渲染完成的消息，把 iframe 标记为已成功渲染
    // console.log(">>> onMessage", { event, type, iframeItem });
    if (type === `iframeRender` && iframeItem) {
      iframeItem.success = true;
      this.sendIFrameInfo(iframeItem);
    }
  }

  // 通过 ins 初始化一个 widget
  initWidget(insEl: HTMLModElement) {
    // 判断是否已渲染，已渲染 ins 内的 a 标签会被 iframe 替换掉
    if (insEl && insEl.getElementsByTagName("a").length > 0) {
      const obj = insEl.dataset || {};
      // get attrs
      const attrObj: DataSet = (this.attrCb && this.attrCb(obj, insEl)) || obj;
      if (!attrObj.prod) return;
      attrObj.prod = attrObj.prod.toLowerCase();
      // 未进行渲染, 进行初始化
      const iframeClass = `klook_iframe_${Date.now()}`;
      // render
      const host = this.host(attrObj.host);
      insEl.innerHTML = this.createIframe(attrObj, iframeClass, host);
      // create iframe
      const iframeEl = insEl.firstElementChild as HTMLIFrameElement;
      // store iframe
      const iframeItem = {
        el: iframeEl,
        dataset: attrObj,
        host,
        prod: attrObj.prod,
        success: false,
        isDev: !host.includes(DEFAULT_HOST),
      };
      this.iframeItems[iframeClass] = iframeItem;
      // iframe init info
      const data = this.getIFrameInfo(iframeEl);
      // log
      this.uploadLog(
        {
          type: IFRAME_STATUS.INIT,
          data,
        },
        host
      );
      // check if iframe render successfully 10s later
      setTimeout(() => {
        const data = this.getIFrameInfo(iframeEl);
        if (iframeItem.success) {
          // log
          this.uploadLog(
            {
              type: IFRAME_STATUS.SUCCESS,
              data,
            },
            host
          );
        } else {
          // log
          this.uploadLog(
            {
              type: IFRAME_STATUS.FAILED,
              data,
            },
            host
          );
          // 渲染失败上报
          !iframeItem.isDev && this.uploadIFrameError(iframeItem);
        }
      }, 10000);
    }
  }

  // 把当前 iframe 相关的信息发送给 iframe 内页面
  sendIFrameInfo(iframeItem: IframeItem) {
    const el: HTMLIFrameElement = iframeItem.el;
    if (el && el.contentWindow && el.contentWindow.postMessage) {
      // 统计当前页面数据, 方便 affiliate 分析
      el.contentWindow.postMessage(
        {
          type: `${iframeItem.prod}_onload`,
          content: this.getIFrameInfo(el),
        },
        iframeItem.host
      );
    }
  }

  uploadLog(content: any, host: string) {
    content.data = JSON.stringify(content.data);
    const xhr = new XMLHttpRequest();
    xhr.open("POST", `${host}/v3/affsrv/ads/event`);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify(content, null, 2));
  }

  // 上报 iframe 渲染失败
  uploadIFrameError(iframeItem: IframeItem) {
    // 定义上报接口
    const errorObj = {
      level: "E",
      type: "IFRAME_SCRIPT_TIME_OUT",
      err_msg: "iframe onload timeout",
      href: window.location.href,
      referer: window.document.referrer,
      userAgent: window.navigator.userAgent,
      prod: iframeItem.prod,
      dataset: Object.assign({}, iframeItem.dataset),
    };
    const xhr = new XMLHttpRequest();
    xhr.open("POST", `${iframeItem.host}/v1/affnode/error_msg`);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify(errorObj, null, 2));
  }

  getIFrameInfo(iframeEl: HTMLIFrameElement) {
    const { clientHeight, clientWidth, className } = iframeEl;
    const { left, top } = iframeEl.getBoundingClientRect();
    return {
      renderId: className,
      href: window.location.href,
      referer: window.document.referrer,
      userAgent: window.navigator.userAgent,
      dataset: Object.assign({}, iframeEl.parentElement.dataset),
      size: {
        clientWidth, // 元素宽
        clientHeight, // 元素高
      },
      // 绝对位置
      position: {
        x: left + document.documentElement.scrollLeft,
        y: top + document.documentElement.scrollTop,
      },
    };
  }

  createIframe(attrObj: DataSet, iframeClass: string, host: string): string {
    const src = `${host}/v1/affnode/render`;
    const query = Object.entries(attrObj)
      .map(([key, value]) => {
        if (key !== "host") {
          return `${key}=${encodeURIComponent(value.trim())}`;
        }
      })
      .join("&");
    return `<iframe
              src=${src}?${query}&renderId=${iframeClass}
              class=${iframeClass}
              style=${this.iframeStyle(
                attrObj.width || "100%",
                attrObj.height || "100%"
              )}
              marginheight="0"
              marginwidth="0"
              frameborder="0"
              allowtransparency="true"
              title="Klook.com third party widget. Discover and book amazing things to do at exclusive prices. Links open in an external site that may or may not meet accessibility guidelines."
            >
            </iframe>`;
  }

  host(dataHost?: string): string {
    // 由于脚本可单独使用, 开发/测试 通过显式ins标签属性传入；产线不传走 affiliate 产线域名
    // @ts-ignore
    return dataHost || window.__klk_aff_host || DEFAULT_HOST;
  }

  iframeStyle(width: string, height: string): string {
    return `border:none;padding:0;margin:0;overflow:hidden;max-width:none;width:${width}px;height:${height}px;`;
  }
}

/* const scriptClassMap = {
  affiliate_base_v3: ".klookaff",
  dynamic_widget_v3: ".things_to_do_dynamic_widget",
  hotel_dynamic_widget_v3: ".klook_hotel_dynamic_widget",
  search_vertical_v3: ".klook_aff_search_box",
}; */

const widgetClasseNames = [
  "klookaff",
  "things_to_do_dynamic_widget",
  "klook_hotel_dynamic_widget",
  "klook_aff_search_box",
];

widgetClasseNames.forEach((className) => {
  // console.log(`>>> search and render widget: ${className}`);
  const iframe = new BaseIframe(className);
  iframe.init();
});
