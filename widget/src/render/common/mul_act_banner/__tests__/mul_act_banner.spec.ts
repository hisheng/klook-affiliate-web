import { mount, createLocalVue } from "@vue/test-utils";
import Banner from "../mul_act_banner.vue";
import Store from "../../../store/render_store";
import vue from "vue";
import { MulActBannerData } from "../../../util/mock_data.js";

declare module "@vue/test-utils/types" {
  function mount(component: any, options?: any): any;
}

jest.mock("../../../util/request"); // mock 相应的api文件
const api = require("../../../util/request");

const localVue = createLocalVue();
localVue.use(Store.install);
const $renderStore = createStore();

describe("多个banner测试", async () => {
  it("测试335*280的banner", async () => {
    api.ajaxGetPromise.mockImplementation(() => {
      return Promise.resolve(MulActBannerData);
    });

    // 现在挂载组件，你便得到了这个包裹器
    const wrapper = mount(Banner, {
      localVue,
      $renderStore,
    });

    await wrapper.vm.fetchData();

    vue.nextTick(() => {
      expect(wrapper.html()).toMatchSnapshot();
    });
  });

  it("测试335*280price的banner", async () => {
    // 重新设置banner的size
    MulActBannerData.result.ad_detail.size_price = "336x280_price";

    api.ajaxGetPromise.mockImplementation(() => {
      return Promise.resolve(MulActBannerData);
    });

    // 现在挂载组件，你便得到了这个包裹器
    const wrapper = mount(Banner, {
      localVue,
      $renderStore,
    });

    await wrapper.vm.fetchData();

    vue.nextTick(() => {
      expect(wrapper.html()).toMatchSnapshot();
    });
  });

  it("测试160x600的banner", async () => {
    // 重新设置banner的size
    MulActBannerData.result.ad_detail.size_price = "160x600";

    api.ajaxGetPromise.mockImplementation(() => {
      return Promise.resolve(MulActBannerData);
    });

    // 现在挂载组件，你便得到了这个包裹器
    const wrapper = mount(Banner, {
      localVue,
      $renderStore,
    });

    await wrapper.vm.fetchData();

    vue.nextTick(() => {
      expect(wrapper.html()).toMatchSnapshot();
    });
  });

  it("测试160x600price的banner", async () => {
    // 重新设置banner的size
    MulActBannerData.result.ad_detail.size_price = "160x600_price";

    api.ajaxGetPromise.mockImplementation(() => {
      return Promise.resolve(MulActBannerData);
    });

    // 现在挂载组件，你便得到了这个包裹器
    const wrapper = mount(Banner, {
      localVue,
      $renderStore,
    });

    await wrapper.vm.fetchData();

    vue.nextTick(() => {
      expect(wrapper.html()).toMatchSnapshot();
    });
  });
});

function createStore(): any {
  return new Store.RenderStore({
    state: {
      currentExp: {},
      keplerId: "",
      tintedList: [],
      size_price: "",
      testActionValue: "",
    },
    mutations: {
      updateSizePrice(state, data) {
        state.size_price = data;
      },
    },
    actions: {},
  });
}
