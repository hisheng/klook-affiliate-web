import Vue from "vue";
import Banner from "./banner.vue";
import Store from "../../store/render_store";
import createStore from "../../store/create_render_store.js";

Vue.use(Store.install);

const $renderStore = createStore();

$renderStore.dispatch("getExperimentsHitList").then(() => {
  new Vue({
    el: "#mul_act_banner",
    $renderStore,
    components: {
      Banner,
    },
    render: function (h: any) {
      return h(Banner);
    },
  });
});
