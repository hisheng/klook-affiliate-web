import { mount, createLocalVue } from "@vue/test-utils";
import SearchBox from "../search_vertical.vue";
import { SearchBoxData } from "../../../util/mock_data.js";
import vue from "vue";
import Store from "../../../store/render_store";
import WidgetLocal from "../../../util/create_local";

jest.mock("../../../util/request"); // mock 相应的api文件
const api = require("../../../util/request");

declare module "@vue/test-utils/types" {
  function mount(component: any, options?: any): any;
}

api.ajaxGetPromise.mockImplementation(() => {
  return Promise.resolve(SearchBoxData);
});

const localVue = createLocalVue();
localVue.use(Store.install);
localVue.use(WidgetLocal);

const $renderStore = createStore();
describe("测试Search Box", async () => {
  it("测试Search Box", async () => {
    const wrapper = mount(SearchBox, {
      localVue,
      $renderStore,
    });

    await wrapper.vm.fetchData();

    vue.nextTick(() => {
      expect(wrapper.html()).toMatchSnapshot();
    });
  });
});

function createStore(): any {
  return new Store.RenderStore({
    state: {
      currentExp: {},
      keplerId: "",
      tintedList: [],
      size_price: "",
      testActionValue: "",
    },
    mutations: {},
    actions: {},
  });
}
