import Vue from "vue";
import SearchVertical from "./search_vertical.vue";
import Store from "../../store/render_store";
import createStore from "../../store/create_render_store.js";

Vue.use(Store.install);

const $renderStore = createStore();

$renderStore.dispatch("getExperimentsHitList").then(() => {
  new Vue({
    el: "#search_box",
    $renderStore,
    components: {
      SearchVertical,
    },
    render: function (h: any) {
      return h(SearchVertical);
    },
  });
});
