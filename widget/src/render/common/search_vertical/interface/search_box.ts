export enum widthCondition {
  BIG = 1,
  SMALL = 2,
}

export interface verticalDetail {
  vid: number;
  name: string;
  jump: string;
  img: string;
  svid: {
    img?: string;
    vid?: number;
    jump?: string;
    name?: string;
  }[];

  [key: string]: any;
}
