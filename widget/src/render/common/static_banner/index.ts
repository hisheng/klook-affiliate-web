import Vue from "vue";
import Static_banner from "./static_banner.vue";
import Store from "../../store/render_store";
import createStore from "../../store/create_render_store.js";

Vue.use(Store.install);

const $renderStore = createStore();

$renderStore.dispatch("getExperimentsHitList").then(() => {
  new Vue({
    $renderStore,
    el: "#static_banner",
    components: {
      Static_banner,
    },
    render: function (h: any) {
      return h(Static_banner);
    },
  });
});
