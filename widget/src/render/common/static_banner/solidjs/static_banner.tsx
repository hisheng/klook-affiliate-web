import { createMemo } from "solid-js";
import { createStore } from "solid-js/store";
import { render } from "solid-js/web";
import { getUrlParam, urlObj } from "../../../util/common_data.js";
import { ajaxGetPromise } from "../../../util/request.js";

function App() {
  const [state, setState] = createStore({
    lang: "en-US",
    bg_type: "Klook",
    size: "",
    url: "",
    adid: "",
    aid: "",
  });
  const width = createMemo(() => {
    return state.size.split("x")[0];
  });
  const height = createMemo(() => {
    return state.size.split("x")[1];
  });
  // mock
  const currentExp = {
    id: "",
    groupId: "",
  };
  function created() {
    const adid = getUrlParam("adid");
    if (!adid || +adid < 0 || !Number.isInteger(+adid)) {
      return;
    }
    setState({
      adid,
    });
    fetchData();
  }
  async function fetchData() {
    const res = await ajaxGetPromise(
      urlObj.static_banner_render_data(state.adid)
    );
    if (res.success) {
      const { lang, url, aid } = res.result;
      const { bg_type, size } = res.result.content;
      setState({
        url,
        lang,
        size,
        bg_type,
        aid,
      });
      /* window.dataLayer &&
        window.dataLayer.push({
          event: "adDisplay",
          eventCategory: `Experimentation`,
          eventAction: `Affiliate Ad Impression`,
          eventLabel: `Static Banner`,
          KeplerID: '',
          AffiliateKeplerGroup: `${currentExp.id || ""}_${
            currentExp.groupId || ""
          }`,
          AffiliateADID: state.adid,
          PartnerWebsiteAID: state.aid,
        }); */
    }
  }
  function handleClickActivity() {
    // 获得当前命中的实验，进行上报
    /* window.dataLayer &&
      window.dataLayer.push({
        event: "adDisplay",
        eventCategory: `Experimentation`,
        eventAction: `Affiliate Ad Click`,
        eventLabel: `Static Banner`,
        KeplerID: '',
        AffiliateKeplerGroup: `${currentExp.id || ""}_${
          currentExp.groupId || ""
        }`,
        AffiliateADID: state.adid,
        PartnerWebsiteAID: state.aid,
      }); */
  }

  created();

  return (
    <a
      class="static_banner"
      target="_blank"
      href={state.url}
      onClick={handleClickActivity()}
    >
      <img
        v-if="size"
        src={`//cdn.klook.com/s/dist_web/klook-affiliate-front/s/dist/widgets/imgs/banner_v3/${
          state.lang
        }/${state.bg_type}_${width()}*${height()}.jpg`}
        width={width()}
        height={height()}
        srcset={`//cdn.klook.com/s/dist_web/klook-affiliate-front/s/dist/widgets/imgs/banner_v3/${
          state.lang
        }/${state.bg_type}_${width()}*${height()}@2x.jpg`}
        alt="ads"
      />
    </a>
  );
}

render(() => <App />, document.getElementById("static_banner"));
