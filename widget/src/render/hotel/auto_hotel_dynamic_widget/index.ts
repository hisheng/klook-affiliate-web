import Vue from "vue";
import AutoDynamicWidget from "./auto_dynamic_widget.vue";
import WidgetLocal from "../../util/create_local";
Vue.use(WidgetLocal);

new Vue({
  el: "#auto_dynamic_widget",
  components: {
    AutoDynamicWidget,
  },
  render: function (h: any) {
    return h(AutoDynamicWidget);
  },
  created() {},
  mounted() {},
});
