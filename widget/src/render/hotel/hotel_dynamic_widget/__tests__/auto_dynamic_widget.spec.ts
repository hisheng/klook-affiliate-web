import vue from "vue";
import { mount, createLocalVue } from "@vue/test-utils";
import Widget from "../dynamic_widget.vue";
import { hotelMockData } from "../../../util/mock_data.js";
import WidgetLocal from "../../../util/create_local";

jest.mock("../../../util/request"); // mock 相应的api文件
const api = require("../../../util/request");

api.ajaxGetPromise.mockImplementation(() => {
  return Promise.resolve(hotelMockData);
});

const localVue = createLocalVue();

declare module "@vue/test-utils/types" {
  function mount(component: any, options?: any): any;
}

localVue.use(WidgetLocal);

describe("Hotel Dynamic Widget 测试", async () => {
  it("renders the correct markup", async () => {
    // 现在挂载组件，你便得到了这个包裹器
    const wrapper = mount(Widget, {
      localVue,
    });

    await wrapper.vm.fetchData();

    vue.nextTick(() => {
      expect(wrapper.html()).toMatchSnapshot();
    });
  });
});
