import Vue from "vue";
import Widget from "./dynamic_widget.vue";
import WidgetLocal from "../../util/create_local";

Vue.use(WidgetLocal);

// 无论如何都需要响应渲染
new Vue({
  el: "#widget",
  render: function (h: any) {
    return h(Widget);
  },
});
