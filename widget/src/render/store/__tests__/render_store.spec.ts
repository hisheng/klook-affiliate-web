import Store from "../render_store";
import { mount, createLocalVue } from "@vue/test-utils";
import TestComponent from "./component/test_component.vue";
const localVue = createLocalVue();

declare module "@vue/test-utils/types" {
  function mount(component: any, options?: any): any;
}

localVue.use(Store.install);
const $renderStore: any = createStore();

describe("测试广告render store", async () => {
  it("测试mutations", () => {
    $renderStore.commit("updateCurrentExp", { a: 1, b: 2 });
    $renderStore.commit("updateSizePrice", "300");
    $renderStore.commit("SET_KEPLER_ID", "MHeE9xKHihw17Jdo");
    $renderStore.commit("SET_TINTED_LIST", [1, 2, 3]);

    expect(Object.keys($renderStore.state.currentExp)).toHaveLength(2);
    expect($renderStore.state.size_price).toBe("300");
    expect($renderStore.state.keplerId).toBe("MHeE9xKHihw17Jdo");
    expect($renderStore.state.tintedList).toHaveLength(3);
  });

  it("测试action", async () => {
    await $renderStore.dispatch("testAction");
    expect($renderStore.state.testActionValue).toHaveLength(4);
  });

  it("测试store install方法以及父子组件", async () => {
    const wrapper = mount(TestComponent, {
      localVue,
      $renderStore,
    });
    $renderStore.commit("SET_KEPLER_ID", "MHeE9xKHihw17Jdo");
    await $renderStore.dispatch("testAction");
    const child = wrapper.find(".child");
    expect(child.text()).toBe("MHeE9xKHihw17Jdo-test");
  });
});

function createStore(): any {
  return new Store.RenderStore({
    state: {
      currentExp: {},
      keplerId: "",
      tintedList: [],
      size_price: "",
      testActionValue: "",
    },
    mutations: {
      updateCurrentExp(state, data) {
        state.currentExp = data;
      },
      updateSizePrice(state, data) {
        state.size_price = data;
      },
      SET_TINTED_LIST(state, tintedList) {
        state.tintedList = tintedList;
      },
      SET_CURRENT_HIT_EXP(state, exp) {
        state.currentExp = exp;
      },
      SET_KEPLER_ID(state, id) {
        state.keplerId = id;
      },
      updateTestAction(state, value) {
        state.testActionValue = value;
      },
    },
    actions: {
      async testAction(store) {
        await new Promise((resolve) => {
          setTimeout(() => {
            store.commit("updateTestAction", "test");
            resolve();
          }, 1000);
        });
      },
    },
  });
}
