import store from "./render_store";
import Cookies from "js-cookie";
import { urlObj } from "../util/common_data";
import { ajaxGetPromise } from "../util/request";
// import { sendAffiliateLog } from "monitor";

export default function () {
  return new store.RenderStore({
    state: {
      currentExp: {},
      keplerId: Cookies.get("kepler_id"),
      experimentsGroup: {},
      tintedList: [],
      size_price: "",
    },
    mutations: {
      updateCurrentExp(state, data) {
        state.currentExp = data;
      },
      updateSizePrice(state, data) {
        state.size_price = data;
      },
      SET_EXPERIMENTS_HIT_LIST(state, experimentsHitGroup) {
        // 重新结构化Store挂载的全局实验列表
        const tmpExperimentsGroup = {};
        experimentsHitGroup.map((exp) => {
          tmpExperimentsGroup[exp.experiment_name] = {
            id: exp.experiment_id,
            group: {
              id: exp.group_id,
              name: exp.group_name,
            },
            // 避免同一个实验多次发送埋点数据的状态标记位
            sendStatus: false,
          };
        });
        state.experimentsGroup = tmpExperimentsGroup;
      },
      SET_TINTED_LIST(state, tintedList) {
        state.tintedList = tintedList;
      },
      SET_CURRENT_HIT_EXP(state, exp) {
        state.currentExp = exp;
      },
      SET_KEPLER_ID(state, id) {
        state.keplerId = id;
      },
    },
    actions: {
      async getExperimentsHitList(store) {
        try {
          // 调用kepler 服务，获取命中实验
          const kepler_id = store.state.keplerId || Cookies.get("kepler_id");
          if (kepler_id) {
            const res = await ajaxGetPromise(urlObj.kepler_experiments);
            const expsList = res.result.exps || [];
            // 染色逻辑
            if (res && res.result) {
              const dyeList = [];
              expsList.forEach((item) => {
                if (item.need_dye) {
                  dyeList.push(`${item.experiment_id}:${item.group_id}`);
                }
              });
              if (dyeList.length) {
                // 后端实验
                store.commit(
                  "SET_TINTED_LIST",
                  JSON.stringify({ kepler: dyeList })
                );
              }
            }
            // 保存所有命中实验
            window.tintedList = JSON.stringify({ kepler: expsList || [] });
            store.commit("SET_EXPERIMENTS_HIT_LIST", res.result.exps || []);
          } else {
            console.warn("缺少 kepler_id");
            store.commit("SET_EXPERIMENTS_HIT_LIST", []);
          }
        } catch (error) {
          console.error({ error });
          let errorType = (error && error.message) || "IFRAME_REQUEST_ERROR";
          // TODO
          // sendAffiliateLog(errorType, error);
          store.commit("SET_EXPERIMENTS_HIT_LIST", []);
        }
      },
    },
  });
}
