let Vue = null;

class RenderStore {
  constructor(options) {
    let vm = new Vue({
      data: {
        state: options.state,
      },
    });
    // state
    this.state = vm.state;

    // mutations
    this.mutations = {}; // 存储传进来的mutations
    let mutations = options.mutations || {};
    Object.keys(mutations).forEach((key) => {
      this.mutations[key] = (params) => {
        mutations[key].call(this, this.state, params);
      };
    });

    // actions
    this.actions = {};
    let actions = options.actions || {};
    Object.keys(actions).forEach((key) => {
      this.actions[key] = async (params) => {
        await actions[key].call(this, this, params);
      };
    });
  }

  async dispatch(type, payload) {
    return await this.actions[type](payload);
  }

  commit(key, params) {
    this.mutations[key](params);
  }
}

function install(_Vue) {
  Vue = _Vue;
  Vue.mixin({
    beforeCreate() {
      if (this.$options && this.$options.$renderStore) {
        this.$renderStore = this.$options.$renderStore;
      } else if (this.$parent && this.$parent.$renderStore) {
        this.$renderStore = this.$parent.$renderStore;
      }
    },
  });
}

export default {
  RenderStore,
  install,
};
