import Vue from "vue";
import Widget from "./widget.vue";
import WidgetLocal from "../../util/create_local";
Vue.use(WidgetLocal);
new Vue({
  el: "#auto_dynamic_widget",
  components: {
    Widget,
  },
  render: function (h: any) {
    return h(Widget);
  },
  created() {},
  mounted() {},
});
