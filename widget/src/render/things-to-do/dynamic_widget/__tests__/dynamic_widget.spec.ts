import { mount, createLocalVue } from "@vue/test-utils";
import Widget from "../dynamic_widget.vue";
import Store from "../../../store/render_store";
import vue from "vue";
import { mockData } from "../../../util/mock_data.js";
import WidgetLocal from "../../../util/create_local";

jest.mock("../../../util/request"); // mock 相应的api文件
const api = require("../../../util/request");

api.ajaxGetPromise.mockImplementation(() => {
  return Promise.resolve(mockData);
});

const localVue = createLocalVue();

declare module "@vue/test-utils/types" {
  function mount(component: any, options?: any): any;
}

localVue.use(Store.install);
localVue.use(WidgetLocal);
const $renderStore = createStore();

describe("TTD Dynamic Widget 测试", async () => {
  it("renders the correct markup", async () => {
    // 现在挂载组件，你便得到了这个包裹器
    const wrapper = mount(Widget, {
      localVue,
      $renderStore,
    });

    await wrapper.vm.fetchData();

    vue.nextTick(() => {
      expect(wrapper.html()).toMatchSnapshot();
    });
  });
});

function createStore(): any {
  return new Store.RenderStore({
    state: {
      currentExp: {},
      keplerId: "",
      tintedList: [],
      size_price: "",
      testActionValue: "",
    },
    mutations: {},
    actions: {},
  });
}
