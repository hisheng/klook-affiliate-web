// import { ClientMonitor } from "../../../clientMonitor";
import Widget from "./dynamic_widget.vue";
import Store from "../../store/render_store";
import createStore from "../../store/create_render_store.js";
import WidgetLocal from "../../util/create_local";
import Vue from "vue";

Vue.use(WidgetLocal);
Vue.use(Store.install);

const $renderStore = createStore();
// TODO: 监控
// ClientMonitor("", "");

$renderStore.dispatch("getExperimentsHitList").then(() => {
  new Vue({
    el: "#widget",
    $renderStore,
    components: {
      Widget,
    },
    render: function (h: any) {
      return h(Widget);
    },
  });
});
