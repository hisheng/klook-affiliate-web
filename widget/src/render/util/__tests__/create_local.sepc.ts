import { WidgetLocal } from "../create_local";

describe("测试 local $t", async () => {
  const widgetLocal = new WidgetLocal();

  it("无参数多语言", async () => {
    widgetLocal.setLang("th");
    expect(widgetLocal.$t("powered_by")).toBe("ขับเคลื่อนโดย KLOOK");
  });

  it("有参数多语言", async () => {
    widgetLocal.setLang("ko");
    expect(widgetLocal.$t("from", ["998"])).toBe("998 부터");
  });
});
