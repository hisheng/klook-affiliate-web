import { Vue, Component, Prop } from "vue-property-decorator";

export type IABTestingConfigs = {
  experimentName: string; // 实验名称
  groupComponents: {
    [variantName: string]: (() => Promise<any> | any) | any; // abtest 组件list
  };
  autoDataSend?: boolean | undefined; // 是否发送埋点
};

type hitExperiment = {
  component: object | undefined;
  name: string;
  groupName: string;
  id: number;
  groupId: number;
} | null;

export default function (config: IABTestingConfigs) {
  const { experimentName, groupComponents, autoDataSend = true } = config;
  @Component({
    // 避免PM在创建实验名称的时候添加了空格及制表符，导致组件名不符合规范
    name: `AB-${experimentName.replace(/\s+/g, "-")}`,
  })
  class AB extends Vue {
    // 父级组件props透传
    @Prop({ default: () => ({}) }) extraData!: object;
    // 在某些条件下展示 slot (一般是旧的) 内容
    @Prop({ default: false }) isShowSlot!: boolean;

    get hitExperiment(): hitExperiment {
      const { [experimentName]: hitExp } =
        this.$renderStore.state.experimentsGroup;
      if (hitExp) {
        const hitExpGroupName = hitExp.group.name;
        this.$renderStore.commit("SET_CURRENT_HIT_EXP", {
          name: experimentName,
          groupName: hitExp.group.name,
          id: hitExp.id,
          groupId: hitExp.group.id,
        });
        return {
          component:
            groupComponents[hitExpGroupName] ||
            (groupComponents.control ? groupComponents.control : undefined),
          name: experimentName,
          groupName: hitExp.group.name,
          id: hitExp.id,
          groupId: hitExp.group.id,
        };
      } else {
        return null;
      }
    }

    mounted() {
      autoDataSend && this.customTrack();
    }

    customTrack(customTrackingData: object = {}) {
      if (this.hitExperiment) {
        // 命中后, 上报数据
        window.dataLayer.push({
          // 需要定义公共上传的数据
          ...customTrackingData,
        });
      }
    }

    render(h: any) {
      if (this.hitExperiment && !this.isShowSlot) {
        return h(this.hitExperiment.component, {
          props: {
            hitExperimentName: experimentName,
            hitGroupName: this.hitExperiment.groupName,
            hitExperimentId: this.hitExperiment.id,
            hitGroupId: this.hitExperiment.groupId,
            customTrack: this.customTrack,
          },
          on: {
            ...this.$listeners, // 处理事件
          },
        });
      } else {
        // 没有命中实验，返回
        return this.$slots.default ? h("div", this.$slots.default) : null;
      }
    }
  }

  return AB;
}
