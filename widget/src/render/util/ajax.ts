// 旧的请求是直接使用 jQuery 的 ajax 模块，但需要把 jQuery 整个包引入，却不用其他功能，增大了打包体积
// 新的方案是提供一个 jquery-ajax 相似的函数
// 具体参考了此库：https://github.dev/ded/reqwest/blob/master/src/reqwest.js
// 但只保留了 widget 需要的部分逻辑，以求打包体积最小

const defaultHeaders = {
  "Content-Type": "application/json; charset=utf-8",
};

const accepts = {
  "*": "text/javascript, text/html, application/xml, text/xml, */*",
  xml: "application/xml, text/xml",
  html: "text/html",
  text: "text/plain",
  json: "application/json, text/javascript",
  js: "application/javascript, text/javascript",
};

export default function ajax(options) {
  let isFinish = false; // success or error
  let isTimeout = false;
  const {
    method,
    type, // An alias for method
    url,
    dataType = "json",
    contentType,
    headers = {},
    data,
    success,
    error,
    timeout,
  } = options;
  const request = new XMLHttpRequest();
  request.open(method || type, url, true);
  // set headers
  if (contentType) headers["Content-Type"] = contentType;
  headers["Accept"] = accepts[dataType];
  Object.entries({
    ...defaultHeaders,
    ...headers,
  }).forEach(([key, value]) => {
    request.setRequestHeader(key, value as string);
  });
  const hasSuccessCb = success && typeof success === "function";
  const hasErrorCb = error && typeof error === "function";
  request.onload = function () {
    isFinish = true;
    if (isTimeout) return;
    if (this.status >= 200 && this.status < 400) {
      // success
      let resp = this.response;
      if (dataType === "json") {
        resp = JSON.parse(resp);
      }
      // see jquery success callback define: https://api.jquery.com/jquery.ajax/
      hasSuccessCb && success(resp, this.statusText, request);
    } else {
      // we reached our target server, but it returned an error
      // see jquery error callback define: https://api.jquery.com/jquery.ajax/
      hasErrorCb && error(request, this.statusText, new Error("Server Error"));
    }
  };
  request.onerror = function () {
    isFinish = true;
    if (isTimeout) return;
    hasErrorCb && error(request, this.statusText, new Error("Request Error"));
  };
  // deal with timeout
  if (timeout) {
    setTimeout(() => {
      if (isFinish) return;
      isTimeout = true;
      request.abort();
      if (hasErrorCb) {
        const err = new Error("Request is aborted: timeout");
        error(request, "", err);
      }
    }, timeout);
  }
  // send
  request.send(data);
}
