import DOMPurify from "dompurify";
import { ajaxPost } from "./request.js";

const urlObj = {
  // 广告上报
  ads_event: "/v3/affsrv/ads/event",
  kepler_experiments: "/v2/usrcsrv/hit/experiments",
  // Widget渲染相关
  widgetRenderData: (adid) => {
    return `/v3/affsrv/ads/widget?adid=${adid}`;
  },
  widgetRenderDataVariant: (adid) => {
    return `/v3/affsrv/ads/widget/dynamic?adid=${adid}`;
  },
  staticWidgetRenderDataVariant: (adid) => {
    return `/v3/affsrv/ads/widget/static?adid=${adid}`;
  },
  search_box_render_data: (adid) => {
    return `/v3/affsrv/ads/search?adid=${adid}`;
  },

  static_banner_render_data: (adid) => {
    return `/v3/affsrv/ads/banner/static?adid=${adid}`;
  },

  activity_banner_render_data: (adid) => {
    return `/v3/affsrv/ads/banner/activity?adid=${adid}`;
  },
  // auto_dynamic_widget
  // auto_widget: '/v3/affsrv/ads/autowidget',
  auto_widget: "/v3/affsrv/ads/widget/dynamic/auto",
  // ads_render
  // hotel
  ads_render_hotel_dynamic_widget: "/v3/affsrv/ads/hotelwidget/dynamic",
  ads_render_hotel_static_widget: "/v3/affsrv/ads/hotelwidget/static",
  ads_render_hotel_auto_dynamic_widget: "/v3/affsrv/ads/hotelwidget/auto",
};

let common_obj = {
  event: "adDisplay",
  eventCategory: "My Ads",
  eventAction: "Partner Website",
  eventLabel: "Search Box",
};
const SEARCH_VERTICAL_FILLING_POINT_MAP = {
  11: {
    ...common_obj,
    verticalName: "Attractions & Shows",
  },
  12: {
    ...common_obj,
    verticalName: "Tours & Sightseeing",
  },
  13: {
    ...common_obj,
    verticalName: "Activities & Experiences",
  },
  14: {
    ...common_obj,
    verticalName: "Best Food & Must Eats",
  },
  21: {
    ...common_obj,
    verticalName: "JR Pass",
  },
  22: {
    ...common_obj,
    verticalName: "Europe Train",
  },
  23: {
    ...common_obj,
    verticalName: "China Train",
  },
  24: {
    ...common_obj,
    verticalName: "Taiwan Train",
  },
  25: {
    ...common_obj,
    verticalName: "Vietnam Train",
  },
  31: {
    ...common_obj,
    verticalName: "WiFi & SIM Card",
  },
  41: {
    ...common_obj,
    verticalName: "Transport & Travel Services",
  },
};

const RENDER_LANGUAGE_MAP = {
  "en-US": {
    activity_reviews: "Reviews",
    view_more: "View More",
    search: "Search",
    search_placeholder: "Search by destination or activity",
    search_title: "Discover and book amazing things to do at exclusive prices",
    banner_book_now: "BOOK NOW",
    discount: "%s% OFF",
  },
  "zh-CN": {
    activity_reviews: "评价",
    view_more: "查看更多",
    search: "搜索",
    search_placeholder: "输入目的地/景点/活动",
    search_title: "发现更好玩的世界，预订独一无二的旅行体验",
    banner_book_now: "立即预订",
    discount: "%s折",
  },
  "zh-TW": {
    activity_reviews: "評價",
    view_more: "查看更多",
    search: "搜索",
    search_placeholder: "輸入目的地/景點/活動",
    search_title: "發現更好玩的世界，預訂獨一無二的旅行體驗",
    banner_book_now: "立即預訂",
    discount: "%s折",
  },
  "zh-HK": {
    activity_reviews: "評論",
    view_more: "查看更多",
    search: "搜索",
    search_placeholder: "輸入目的地/景點/活動",
    search_title: "發現更好玩的世界，預訂獨一無二的旅行體驗",
    banner_book_now: "立即預訂",
    discount: "%s折",
  },
  ko: {
    activity_reviews: "리뷰",
    view_more: "더 보기",
    search: "검색",
    search_placeholder: "도시 또는 액티비티 검색",
    search_title: "더 넓은 세상을 경험하고, 잊지 못할 순간을 예약하세요.",
    banner_book_now: "지금 예약하기",
    discount: "%s% 할인",
  },
  th: {
    activity_reviews: "ความคิดเห็น",
    view_more: "ดูเพิ่มเติม",
    search: "ค้นหา",
    search_placeholder:
      "ค้นหาโดยใช้จุดหมายปลายทางหรือกิจกรรม ค้นหาด้วยจุดหมายปลายทางหรือกิจกรรม",
    search_title: "สำรวจและจองกิจกรรมที่น่าสนใจมากมายในราคาสุดพิเศษ",
    banner_book_now: "จองตอนนี้",
    discount: "ลด %s%",
  },
  vi: {
    activity_reviews: "đánh giá",
    view_more: "Xem thêm",
    search: "Tìm kiếm",
    search_placeholder: "Tìm theo điểm đến hoặc hoạt động",
    search_title:
      "Khám phá và đặt trước các hoạt động du lịch đặc sắc với giá độc quyền",
    banner_book_now: "Đặt ngay",
    discount: "GIÁM GIÁ %s%",
  },
  id: {
    activity_reviews: "ulasan",
    view_more: "Lihat Selengkapnya",
    search: "Cari",
    search_placeholder: "Cari berdasarkan destinasi atau aktivitas",
    search_title:
      "Temukan dan pesanlah aktivitas seru dengan harga yang eksklusif",
    banner_book_now: "PESAN SEKARANG",
    discount: "DISKON %s%",
  },
  ja: {
    activity_reviews: "件のクチコミ",
    view_more: "もっと見る",
    search: "検索",
    search_placeholder: "目的地・アクティビティから検索",
    search_title:
      "現地のワクワクを見つける手がかりに。旅を思いのままにアレンジしよう。",
    banner_book_now: "詳細を見る",
    discount: "%s% OFF",
  },
  fr: {
    activity_reviews: "La revue",
    view_more: "voir plus",
    search: "Search",
    search_placeholder: "Search by destination or activity",
    search_title: "Réservez des activités inoubliables aux meilleurs prix",
    banner_book_now: "BOOK NOW",
    discount: "%s% OFF",
  },
  de: {
    activity_reviews: "Rezension",
    view_more: "Mehr sehen",
    search: "Search",
    search_placeholder: "Search by destination or activity",
    search_title:
      "Entdecke und buche einzigartige Aktivitäten zu exklusiven Preisen",
    banner_book_now: "BOOK NOW",
    discount: "%s% OFF",
  },
  es: {
    activity_reviews: "Revisión",
    view_more: "ver más",
    search: "Search",
    search_placeholder: "Search by destination or activity",
    search_title: "Scopri e prenota attività straordinarie a prezzi esclusivi",
    banner_book_now: "BOOK NOW",
    discount: "%s% OFF",
  },
  ru: {
    activity_reviews: "отзыв",
    view_more: "узнать больше",
    search: "Search",
    search_placeholder: "Search by destination or activity",
    search_title:
      "Находите удивительные варианты досуга и бронируйте их по эксклюзивным ценам",
    banner_book_now: "BOOK NOW",
    discount: "%s% OFF",
  },
};

const currencyDecimalplace = {
  CHF: 2,
  SGD: 2,
  AUD: 2,
  JOD: 2,
  USD: 2,
  EUR: 2,
  GBP: 2,
  MYR: 2,
  OMR: 2,
  NZD: 2,
  CAD: 2,
};

function transformImageUrl(url) {
  let arr = url.split("/upload");
  return (
    arr[0] +
    "/upload/fl_lossy.progressive,q_60,f_auto/c_fill,w_650,h_420" +
    arr[1]
  );
}

function formatRedirectUrl(redirect_url, lang) {
  // 设置 view_more 的语言规则
  redirect_url.view_more_text =
    (lang && RENDER_LANGUAGE_MAP[lang].view_more) || "View More";
  redirect_url.activity_reviews =
    (lang && RENDER_LANGUAGE_MAP[lang].activity_reviews) || "Reviews";
  // 设置韩语标识
  redirect_url.iskoa = lang === "ko";
  if (["fr", "de", "es", "ru"].indexOf(lang) > -1) {
    // 跳转到 portal 英文界面
    redirect_url.affiliate_url = "/home";
  }
  return redirect_url;
}

function getUrlParam(name) {
  const reg = new RegExp(`(^|&)${name}=([^&]*)(&|$)`);
  const r = window.location.search.substr(1).match(reg);
  if (r !== null) return r[2];
  return null;
}

function adsEvent(type, data) {
  ajaxPost(
    urlObj.ads_event,
    {
      type,
      data: data || "",
    },
    {
      "X-iframe-Data": JSON.stringify({
        type,
        data: data || "",
      }),
    }
  );
}

function ads_monitor(prod, receiveMessageCallback) {
  const sendParentMessage = () => {
    const reg = new RegExp(`(^|&)renderId=([^&]*)(&|$)`);
    const r = window.location.search.substr(1).match(reg);
    if (r) {
      window.parent.postMessage(
        {
          type: "iframeRender",
          content: {
            renderID: r[2],
          },
        },
        "*"
      );
    }
  };

  const receiveMessage = (event) => {
    let data;
    if (event.data.type === `${prod}_onload`) {
      data = DOMPurify.sanitize(JSON.stringify(event.data.content));
      receiveMessageCallback && receiveMessageCallback(data);
    }
  };

  const main = () => {
    window.addEventListener("message", receiveMessage, false);
    sendParentMessage();
  };
  main();
}

function formatPriceThousands(price) {
  let priceStr = (price || "0").toString(),
    tmp;

  if (priceStr.indexOf(".") < 0) {
    tmp = priceStr.replace(/(?=(?!(\b))(\d{3})+$)/g, ",");
  } else {
    priceStr = priceStr.split(".");
    tmp =
      priceStr[0].toString().replace(/(?=(?!(\b))(\d{3})+$)/g, ",") +
      "." +
      priceStr[1];
  }

  return tmp;
}

function priceFormatUtil(sell_price, currency) {
  sell_price = formatPriceThousands(sell_price);
  if (!sell_price.includes(".")) {
    return sell_price;
  } else {
    let decimalplace = currencyDecimalplace[currency];
    let indexOf = sell_price.indexOf(".") + 1;
    return sell_price.substr(0, indexOf + (decimalplace || -1));
  }
}

export {
  urlObj,
  RENDER_LANGUAGE_MAP,
  getUrlParam,
  ads_monitor,
  adsEvent,
  formatRedirectUrl,
  transformImageUrl,
  currencyDecimalplace,
  formatPriceThousands,
  priceFormatUtil,
  SEARCH_VERTICAL_FILLING_POINT_MAP,
};
