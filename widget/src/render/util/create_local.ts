import { getUrlParam } from "./common_data.js";

export class WidgetLocal {
  lang = "en";
  locales: any = {
    "zh-TW": {
      from: "{0} 起",
      powered_by: "由 KLOOK 技術提供",
      view_more: "查看更多",
    },
    "zh-HK": {
      from: "{0} 起",
      powered_by: "由 KLOOK 技術提供",
      view_more: "查看更多",
    },
    "zh-CN": {
      from: "{0} 起",
      powered_by: "由 KLOOK 强力驱动",
      view_more: "查看更多",
    },
    vi: {
      from: "Từ {0}",
      powered_by: "Được hỗ trợ bởi KLOOK",
      view_more: "Xem thêm",
    },
    th: {
      from: "เริ่มที่ {0}",
      powered_by: "ขับเคลื่อนโดย KLOOK",
      view_more: "ดูเพิ่มเติม",
    },
    ko: {
      from: "{0} 부터",
      powered_by: "KLOOK 지원",
      view_more: "더 보기",
    },
    ja: {
      from: "{0}〜",
      powered_by: "Powered by KLOOK",
      view_more: "もっと見る",
    },
    id: {
      from: "Mulai {0}",
      powered_by: "Diberdayakan oleh KLOOK",
      view_more: "Lihat Selengkapnya",
    },
    en: {
      from: "From {0}",
      powered_by: "Powered by KLOOK",
      view_more: "View More",
    },
    "en-US": {
      from: "From {0}",
      powered_by: "Powered by KLOOK",
      view_more: "View More",
    },
    // 欧洲四种语言
    fr: {
      from: "À partir de {0}",
      powered_by: "Fourni par KLOOK",
      view_more: "voir plus",
    },
    es: {
      from: "Desde {0}",
      powered_by: "Con la tecnología de KLOOK",
      view_more: "ver más",
    },
    de: {
      from: "ab {0}",
      powered_by: "Powered by KLOOK",
      view_more: "Mehr sehen",
    },
    ru: {
      from: "От {0}",
      powered_by: "Технологии KLOOK",
      view_more: "узнать больше",
    },
  };

  transParams(string, args) {
    function hasOwn(obj, key) {
      return Object.prototype.hasOwnProperty.call(obj, key);
    }

    const RE_NARGS = /(%|)\{([0-9a-zA-Z_]+)\}/g;

    return string.replace(RE_NARGS, (match, prefix, i, index) => {
      let result;
      // {{0}} 双花括号为转义 直接返回原内容  {0} 单花括号填充为参数内容
      if (string[index - 1] === "{" && string[index + match.length] === "}") {
        return i;
      } else {
        result = hasOwn(args, i) ? args[i] : null;
        if (result === null || result === undefined) {
          return "";
        }
        return result;
      }
    });
  }

  setLang(lang) {
    this.lang = lang;
  }

  getLang() {
    return this.lang;
  }

  $t(string, args?: string[]) {
    if (!args) {
      return this.locales[this.lang || "en"][string];
    } else {
      return this.transParams(this.locales[this.lang || "en"][string], args);
    }
  }
}

function installLocal(_Vue) {
  const widgetLocal = new WidgetLocal();
  _Vue.prototype.widgetLocal = widgetLocal;
  _Vue.prototype.$t = widgetLocal.$t.bind(widgetLocal);
  _Vue.mixin({
    created() {
      this.widgetLocal.setLang(getUrlParam("lang") || "en");
    },
  });
}

export default installLocal;
