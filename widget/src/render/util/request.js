import Cookies from "js-cookie";
import { v4 as uuidv4 } from "uuid";
import ajax from "./ajax";
// const ajax = $.ajax
import {
  trackRequestError,
  trackRequestSuccess,
} from "../../../../shared/optimus";
import Logger from "../../../../shared/logquery";

const logger = new Logger();
const CODE_CLIENT_ERROR = 990001;

window.addEventListener("error", (error) =>
  logger.handlerError(error, { type: "js" })
);

//生成32位的hash
function stringHashCode() {
  let hash = 0;
  if (this.length === 0) return hash;
  for (let i = 0; i < this.length; i++) {
    const c = this.charCodeAt(i);
    hash = (hash << 5) - hash + c;
    hash = hash & hash; // Convert to 32bit integer
  }
  return hash;
}

let errorLastSendTime = {};

/**
 *  为ErrorObj生成唯一的key, 来判断这个key是否重复发送过
 */
function getErrorObjKey(errorObj) {
  let strs = [];
  const keys = Object.keys(errorObj).sort();
  for (let i in keys) {
    strs.push(i + "=" + errorObj[i]);
  }
  return stringHashCode.call(strs.join(""));
}

const sendAffiliateLog = (type, msg, url, lineNo, columnNo, err) => {
  if (process.env.NODE_ENV !== "production") {
    return;
  }
  const post_url = "/v1/affnode/error_msg",
    loc = window.location,
    errorObj = {
      type: type || "js_err",
      page_query: loc.search,
      page_url: loc.protocol + "//" + loc.host + loc.pathname,
      err_msg: msg,
      script_url: url,
      line_number: lineNo,
      column: columnNo,
      error_obj: err,
      user_agent: window.navigator.userAgent,
    };
  let filterList = ["ResizeObserver loop limit exceeded", "Script error."];
  //排除三方引入的js 上报log
  if (errorObj.err_msg && filterList.includes(errorObj.err_msg)) {
    return;
  }
  //有一些error会很快的重复出现, 这里为了减少发送
  const key = getErrorObjKey(errorObj);
  const t = Date.now();
  const lastSendTime = errorLastSendTime[key] || 0;
  if (t - lastSendTime > 60 * 1000) {
    errorLastSendTime[key] = t;
    const xhr = new XMLHttpRequest();
    xhr.open("POST", post_url);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify(errorObj));
  } else {
    console.warn("same error log send too frequent!");
  }
};

function ajaxBase(method, url, data, headers, callback) {
  if (typeof data == "function") {
    callback = data;
    headers = undefined;
    data = undefined;
  } else if (typeof headers == "function") {
    callback = headers;
    headers = undefined;
  }
  headers = headers || {};
  callback = callback || function () {};

  const params = {
    method,
    url: url,
    cache: false,
    dataType: "json",
    // TODO: 这个好像没啥用，待移除
    startTime: Date.now(),
  };

  // set common headers
  headers["X-Klook-Kepler-Id"] = Cookies.get("kepler_id");
  headers["X-Klook-Request-Id"] = uuidv4();
  headers["X-Klook-Client-Version"] = process.env.__VERSION__; // 前端 master 值
  params.headers = headers;
  params.data = data || undefined;
  // timeout
  params.timeout = 60 * 2 * 1000;
  // callback
  params.success = function (resp, statusText, xhr) {
    onSuccess(resp, statusText, xhr, params);
    callback(null, resp);
  };
  params.error = function (xhr, statusText, error) {
    onError(xhr, statusText, error, params);
    callback(
      {
        success: false,
        error: {
          code: CODE_CLIENT_ERROR,
          message: "MULTIPLE_client_network_failure",
        },
      },
      null
    );
  };
  return ajax(params);
}

function onSuccess(resp, statusText, xhr, params) {
  if (!resp.success) {
    logger.handlerError(
      { name: "Ajax Failure", type: "ajax" },
      { reqUrl: params.url || "", reqParams: params, reqResponse: resp }
    );
    sendAffiliateLog(
      "ajax_err",
      (resp.error || {}).message,
      params.url,
      "",
      "",
      {
        data: params.data,
        // response : resp //减少上报信息
      }
    );
  }
  trackRequestSuccess({ ...resp, status: xhr.status }, params);
  // 如果没有 message 返回默认的 message
  resp.error = resp.error || {};
}

function onError(xhr, statusText, error, params) {
  sendAffiliateLog("ajax_err", "", params.url, "", "", {
    // errorType : errorType // 减少上报信息
  });
  logger.handlerError(error, {
    type: "ajax",
    reqUrl: (error.config || {}).url,
  });
  trackRequestError({ message: error, status: xhr.status }, params);
}

// 偏函数
function curry(fn, ...args1) {
  return function curried(...args2) {
    return fn.apply(null, [...args1, ...args2]);
  };
}

// 将 callback 的函数转为 promise
function promisify(fn) {
  return function promisified(...args) {
    return new Promise((resolve, reject) => {
      fn.apply(null, [
        ...args,
        (err, data) => {
          if (err) reject(err);
          else resolve(data);
        },
      ]);
    });
  };
}

const ajaxGet = curry(ajaxBase, "GET");

const ajaxGetPromise = promisify(ajaxGet);

const ajaxPut = curry(ajaxBase, "PUT");

const ajaxPost = curry(ajaxBase, "POST");

export { ajaxGet, ajaxPut, ajaxPost, ajaxGetPromise };
