declare module "*.vue" {
  import Vue from "vue";
  export default Vue;
}

declare module "vue/types/vue" {
  interface Vue {
    $store: {
      [index: string]: any;
    };
    $renderStore: {
      [index: string]: any;
    };
    widgetLocal: {
      [index: string]: any;
    };
  }
}
