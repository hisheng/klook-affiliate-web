export { default as urlObj } from "./url";

export const currencyDecimalplace = {
  CHF: 2,
  SGD: 2,
  AUD: 2,
  JOD: 2,
  USD: 2,
  EUR: 2,
  GBP: 2,
  MYR: 2,
  OMR: 2,
  NZD: 2,
  CAD: 2,
};

// render广告页面 多语言映射
export const RENDER_LANGUAGE_MAP = {
  "en-US": {
    activity_reviews: "Reviews",
    view_more: "View More",
    search: "Search",
    search_placeholder: "Search by destination or activity",
    search_title: "Discover and book amazing things to do at exclusive prices",
    banner_book_now: "BOOK NOW",
    discount: "%s% OFF",
  },
  "zh-CN": {
    activity_reviews: "评价",
    view_more: "查看更多",
    search: "搜索",
    search_placeholder: "输入目的地/景点/活动",
    search_title: "发现更好玩的世界，预订独一无二的旅行体验",
    banner_book_now: "立即预订",
    discount: "%s折",
  },
  "zh-TW": {
    activity_reviews: "評價",
    view_more: "查看更多",
    search: "搜索",
    search_placeholder: "輸入目的地/景點/活動",
    search_title: "發現更好玩的世界，預訂獨一無二的旅行體驗",
    banner_book_now: "立即預訂",
    discount: "%s折",
  },
  "zh-HK": {
    activity_reviews: "評論",
    view_more: "查看更多",
    search: "搜索",
    search_placeholder: "輸入目的地/景點/活動",
    search_title: "發現更好玩的世界，預訂獨一無二的旅行體驗",
    banner_book_now: "立即預訂",
    discount: "%s折",
  },
  ko: {
    activity_reviews: "리뷰",
    view_more: "더 보기",
    search: "검색",
    search_placeholder: "도시 또는 액티비티 검색",
    search_title: "더 넓은 세상을 경험하고, 잊지 못할 순간을 예약하세요.",
    banner_book_now: "지금 예약하기",
    discount: "%s% 할인",
  },
  th: {
    activity_reviews: "ความคิดเห็น",
    view_more: "ดูเพิ่มเติม",
    search: "ค้นหา",
    search_placeholder:
      "ค้นหาโดยใช้จุดหมายปลายทางหรือกิจกรรม ค้นหาด้วยจุดหมายปลายทางหรือกิจกรรม",
    search_title: "สำรวจและจองกิจกรรมที่น่าสนใจมากมายในราคาสุดพิเศษ",
    banner_book_now: "จองตอนนี้",
    discount: "ลด %s%",
  },
  vi: {
    activity_reviews: "đánh giá",
    view_more: "Xem thêm",
    search: "Tìm kiếm",
    search_placeholder: "Tìm theo điểm đến hoặc hoạt động",
    search_title:
      "Khám phá và đặt trước các hoạt động du lịch đặc sắc với giá độc quyền",
    banner_book_now: "Đặt ngay",
    discount: "GIÁM GIÁ %s%",
  },
  id: {
    activity_reviews: "ulasan",
    view_more: "Lihat Selengkapnya",
    search: "Cari",
    search_placeholder: "Cari berdasarkan destinasi atau aktivitas",
    search_title:
      "Temukan dan pesanlah aktivitas seru dengan harga yang eksklusif",
    banner_book_now: "PESAN SEKARANG",
    discount: "DISKON %s%",
  },
  ja: {
    activity_reviews: "件のクチコミ",
    view_more: "もっと見る",
    search: "検索",
    search_placeholder: "目的地・アクティビティから検索",
    search_title:
      "現地のワクワクを見つける手がかりに。旅を思いのままにアレンジしよう。",
    banner_book_now: "詳細を見る",
    discount: "%s% OFF",
  },
  fr: {
    activity_reviews: "La revue",
    view_more: "voir plus",
    search: "Search",
    search_placeholder: "Search by destination or activity",
    search_title: "Réservez des activités inoubliables aux meilleurs prix",
    banner_book_now: "BOOK NOW",
    discount: "%s% OFF",
  },
  de: {
    activity_reviews: "Rezension",
    view_more: "Mehr sehen",
    search: "Search",
    search_placeholder: "Search by destination or activity",
    search_title:
      "Entdecke und buche einzigartige Aktivitäten zu exklusiven Preisen",
    banner_book_now: "BOOK NOW",
    discount: "%s% OFF",
  },
  es: {
    activity_reviews: "Revisión",
    view_more: "ver más",
    search: "Search",
    search_placeholder: "Search by destination or activity",
    search_title: "Scopri e prenota attività straordinarie a prezzi esclusivi",
    banner_book_now: "BOOK NOW",
    discount: "%s% OFF",
  },
  ru: {
    activity_reviews: "отзыв",
    view_more: "узнать больше",
    search: "Search",
    search_placeholder: "Search by destination or activity",
    search_title:
      "Находите удивительные варианты досуга и бронируйте их по эксклюзивным ценам",
    banner_book_now: "BOOK NOW",
    discount: "%s% OFF",
  },
};
