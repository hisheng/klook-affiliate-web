// import _ from 'underscore';
const klook_c_related_url_map = {
  //used for core.js to isolate c platform releted url to adjust accept-Language header
  user_forget_password: "/v1/usrcsrv/password/forgetpwd",
  user_login: "/v3/userserv/user/login_service/login_email_with_captcha",
  captcha_init: "/v3/userserv/user/captcha_service/captcha_init",
};

const urlObj: any = {
  get_site_list: "/v3/affsrv/websites",
  get_admin_site_list: "/v3/affsrv/admin/websites",
  user_info: "/v3/affsrv/user",
  admin_website: "/ajax/v2/admin_website",
  contact_us: "/ajax/v2/contact_us",
  transfer_users: "/ajax/v2/transfer_users",
  get_admin_list: "/ajax/v2/admin_list",
  get_monthly_share: "/ajax/v2/monthly_share",
  get_admin_payments: "/v3/affsrv/admin/payments/list",
  get_admin_payment_detail_by_uid_fn: (uid) =>
    `/v3/affsrv/admin/payments/detail/${uid}`,
  change_user_payment: (uid) => `/v3/affsrv/admin/user/${uid}/suspension`,
  get_income_detail: "/v3/affsrv/income_detail",
  get_recent_summary: "/v3/affsrv/recent_summary",
  download_finance_template: "/v3/affsrv/finance/report",
  upload_finance_template: "/v3/affsrv/finance/report",
  get_my_bonus_info: "/v3/affsrv/bonus",
  update_bonus_rate: "/ajax/v2/update_bonus_rate",

  // dynamic commission
  get_category: "/ajax/v2/category",
  get_book_user_country: "/v3/affsrv/get_book_user_country",
  get_admin_website_list: "/v3/affsrv/admin_website/list",
  get_commission_list: "/ajax/v2/commission_rule/filter",
  check_commission_rule: "/v3/affsrv/check_commission_rule",
  commission_rule_create: "/ajax/v2/commission_rule/create",
  commission_get_klook_ranges: "/v3/affsrv/get_klook_ranges",
  commission_rule_stop: (rule_id) => `/ajax/v2/commission_rule/stop/${rule_id}`,
  commission_rule_delete: (rule_id) =>
    `/ajax/v2/commission_rule/delete/${rule_id}`,
  commission_rule_update: (rule_id) =>
    `/ajax/v2/commission_rule/update/${rule_id}`,

  //乐活大方相关
  list_lohas_ads: "/ajax/v2/list_lohas_ads",
  lohas_ad_delete: "/ajax/v2/lohas_ad/delete",
  lohas_ad_add: "/ajax/v2/lohas_ad/add",
  lohas_ad_update: "/ajax/v2/lohas_ad/update",

  // 新增 /v3 类型接口, 作以区分
  user_financial: "/v3/affsrv/user/finance",
  user_taxation: "/v3/affsrv/user/taxation",
  user_payment: "/v3/affsrv/user/payment",
  get_payment_bank_fields: "/v3/affsrv/payment/bank/fields",
  get_gift_card_list: "/v3/affsrv/activities/giftcard/list",
  get_commission_total: "/v3/affsrv/commission",
  activities_get_data_by_city: (cityId, type) => {
    return `/v3/affsrv/activities/city/${cityId}?ad_type=${type}`;
  },
  activities_validate: "/v3/affsrv/activities/validate",
  hotel_static_widget_validate: "/v3/affsrv/hotels/validate",
  update_solution_type: (uid) => `/v3/affsrv/users/${uid}/update`,
  v3_affsrv_ad: "/v3/affsrv/ads",
  v3_batch_update_aff_type: "/v3/affsrv/users/batch_update_aff_type",
  get_admin_uid: "/v3/affsrv/admin/users",
  get_country_list: "/v3/affsrv/country",
  update_website_list: "/v3/affsrv/websites",
  search_box_vertical_list: "/v3/affsrv/ads/search/domain",
  get_special_activity_list: "/v3/affsrv/activities/special/list",
  get_category_commission: "/v3/affsrv/activities/category/commission",
  get_ad_performance: "/v3/affsrv/ads/performance",
  gen_ad_performance_report: "/v3/affsrv/ads/performance/report",
  network_publisher_list: "/v3/affsrv/publisher",
  gen_network_publisher_report: "/v3/affsrv/publisher/report",
  websites_list: "/v3/affsrv/websites",
  ad_type_map: "/v3/affsrv/ads/adtype",
  email_validation: "/v3/affsrv/user/email/validation",
  aff_user_register: "/v3/affsrv/user/register",
  change_user_status: (uid) => `/v3/affsrv/admin/user/${uid}/status`,
  change_user_checkstatus: (uid) => `/v3/affsrv/admin/user/${uid}/checkstatus`,
  user_activation: (token) => {
    return "/v3/affsrv/user/activation/" + token;
  },
  resend_activation: "/v3/affsrv/user/email/resend",

  // Widget渲染相关
  widgetRenderData: (adid) => {
    return `/v3/affsrv/ads/widget?adid=${adid}`;
  },
  search_box_render_data: (adid) => {
    return `/v3/affsrv/ads/search?adid=${adid}`;
  },

  static_banner_render_data: (adid) => {
    return `/v3/affsrv/ads/banner/static?adid=${adid}`;
  },

  activity_banner_render_data: (adid) => {
    return `/v3/affsrv/ads/banner/activity?adid=${adid}`;
  },

  // auto_dynamic_widget
  auto_widget: "/v3/affsrv/ads/autowidget",
  // user role
  user_role_list: "/v3/affsrv/admin/user/group",
  internal_user_list: "/v3/affsrv/admin/user",
  internal_user_update: "/v3/affsrv/admin/user/group",

  // co-brnad 相关
  co_brand_upload_img: "/v3/affsrv/websites/cobrands/upload",
  co_brand_aid_list: "/v3/affsrv/websites",
  co_brand_websites_cobrands: "/v3/affsrv/websites/cobrands",
  co_brand_history_list: "/v3/affsrv/websites/cobrands/operation",
  co_brand_admin_history_list: "/v3/affsrv/admin/websites/cobrands/operation",

  //admin -> affiliates admin 页面
  admin_get_affiliates_list: "/v3/affsrv/admin/affiliates_list",
  admin_post_affiliate_info: "/v3/affsrv/admin/affiliate",
  admin_export_affiliates_list: "/v3/affsrv/admin/affiliates/export",

  //admin -> monthly report 页面，order/ticket 报表 (by month)
  admin_monthly_order_export: "/v3/affsrv/admin/monthly/orders/export",
  admin_monthly_ticket_export: "/v3/affsrv/admin/monthly/tickets/export",
  admin_monthly_order: "/v3/affsrv/admin/monthly/orders",
  admin_monthly_ticket: "/v3/affsrv/admin/monthly/tickets",
  admin_monthly_status_billing_report:
    "/v3/affsrv/admin/billing/tickets/export",
  admin_monthly_status_booking_report:
    "/v3/affsrv/admin/booking/tickets/export",

  //用户侧，performance页面，order/ticket 报表 (by date range)
  user_order: "/v3/affsrv/user/orders",
  user_order_export: "/v3/affsrv/user/orders/export",
  user_ticket: "/v3/affsrv/user/tickets",
  user_ticket_export: "/v3/affsrv/user/tickets/export",
  user_billing_ticket_report: "/v3/affsrv/user/billing/tickets/export",
  user_booking_ticket_report: "/v3/affsrv/user/booking/tickets/export",

  //admin侧，admin panel页面，order/ticket 报表 (by date range)
  admin_order: "/v3/affsrv/admin/orders",
  admin_order_export: "/v3/affsrv/admin/orders/export",
  admin_ticket: "/v3/affsrv/admin/tickets",
  admin_ticket_export: "/v3/affsrv/admin/tickets/export",

  // expense
  admin_expense: "/v3/affsrv/admin/user/expense",
  admin_expense_edit: (id) => `/v3/affsrv/admin/user/expense/${id}`,
  expense_type: "/v3/affsrv/admin/user/expense/type",
  delete_expense: (id) => `/v3/affsrv/admin/user/expense/flag/${id}`,
  search_user: (query) => `/v3/affsrv/admin/payment/user?query=${query}`,
  expense_export: (uid, month) =>
    `/v3/affsrv/admin/user/expense/export?uid=${uid}&month=${month}`,

  // dynamic commission 2.0
  get_publish_by_wid: "/v3/affsrv/publishers/query",
  search_commission_campaign: "/v3/affsrv/commission/campaign/query",
  create_campaign: "/v3/affsrv/commission/campaign/create",
  update_campaign: (campaign_id) =>
    `/v3/affsrv/commission/campaign/${campaign_id}/update`,
  delete_campaign: (campaign_id) =>
    `/v3/affsrv/commission/campaign/${campaign_id}/delete`,
  terminate_campaign: (campaign_id) =>
    `/v3/affsrv/commission/campaign/${campaign_id}/terminate`,
  get_website_list_by_wid: `/v3/affsrv/websites/query`,
  template_id_mapping: `/v3/affsrv/commission/template_id_mapping`,
  campaign_rule_create: (campaign_id) =>
    `/v3/affsrv/commission/campaign/${campaign_id}/rule/create`,
  campaign_rule_update: (campaign_id, campaign_rule_id) =>
    `/v3/affsrv/commission/campaign/${campaign_id}/rule/${campaign_rule_id}/update`,
  campaign_rule_delete: (campaign_id, campaign_rule_id) =>
    `/v3/affsrv/commission/campaign/${campaign_id}/rule/${campaign_rule_id}/delete`,
  campaign_rule_query: `/v3/affsrv/commission/campaign/rule/query`,
  campaign_search_by_id: (campaign_id) =>
    `/v3/affsrv/commission/campaign/${campaign_id}/retrieve`,
  campaign_rule_search_by_id: (campaign_id, campaign_rule_id) =>
    `/v3/affsrv/commission/campaign/${campaign_id}/rule/${campaign_rule_id}/retrieve`,

  // notification
  notification_create: "/v3/affsrv/admin/notification",
  notification_history: "/v3/affsrv/admin/notification/action",
  notification_upload_img: "/v3/affsrv/admin/notification/image",
  notification_detail: (id) => `/v3/affsrv/admin/notification/${id}/detail`,
  notification_read_totals: `/v3/affsrv/notification/read_status`,
  notification_list: "/v3/affsrv/notification",
  getAdminList: "/v3/affsrv/admin/user?gid=1,2&pcount=100",
  notification_publisher_detail: (id) => `/v3/affsrv/notification/${id}/detail`,
  notification_publisher_read: (id) =>
    `/v3/affsrv/notification/${id}/read_status`,

  getAllNotificationId: "/v3/affsrv/notification/all/ids", //获取当前用户所有noti id 和 未读的 noti id
  getNotificationDetailById: (id) =>
    "/v3/affsrv/notification/details?noti_ids=" + id, // 获取noti_id 的详情
  setAllRead: "/v3/affsrv/notification/all/read_status", // 标记所有notification为已读

  // 登录注册相关
  session: "/session",
  logout: "/logout",
  user_forget_password: klook_c_related_url_map.user_forget_password,
  user_login: klook_c_related_url_map.user_login,
  captcha_init: klook_c_related_url_map.captcha_init,
  get_affiliate_coupon_codes: "/v3/affsrv/ads/coupons",
  csrf_token: "/v3/affsrv/csrf-token",

  // ads_render
  // hotel
  ads_render_hotel_dynamic_widget: "/v3/affsrv/ads/hotelwidget/dynamic",
  ads_render_hotel_static_widget: "/v3/affsrv/ads/hotelwidget/static",
  ads_render_hotel_auto_dynamic_widget: "/v3/affsrv/ads/hotelwidget/auto",

  // kepler相关
  kepler_experiments: "/v2/usrcsrv/hit/experiments",

  // product_list 页面
  product_list_config: "/v3/affsrv/ads/recommend-product-page-config", // 获取所有目的地和类别
  product_list_data: "/v3/affsrv/ads/recommend-product", // 获取 product_list 数据
  product_list_gen_links: "/v3/affsrv/ads/gen-listing-link", // 生成链接

  // 广告上报
  ads_event: "/v3/affsrv/ads/event",

  // 下载
  special_allow_list_export: "/v3/affsrv/admin/activity/export",
  special_allow_list_upload: "/v3/affsrv/admin/activity/batch",
  special_allow_list_history: "/v3/affsrv/admin/activity/history",
  special_allow_list_confirm: "/v3/affsrv/admin/activity/status",
  special_allow_list_activity: (category_id) =>
    `/v3/affsrv/activity/${category_id}`,

  help_login_menu_list: "/v3/affsrv/feedback/faqcontent/login",
  help_unlogin_menu_list: "/v3/affsrv/feedback/faqcontent/no-login",
  help_faq_feedback: "/v3/affsrv/feedback/faq",

  // feedback
  feedback_get_question: "/v3/affsrv/feedback/question",
  feedback_post_cast: "/v3/affsrv/feedback/csat",
};

urlObj.klook_c_related_url_map = klook_c_related_url_map;

export default urlObj;
