const path = require("path");
const webpack = require("webpack");
// const { BundleAnalyzerPlugin } = require("webpack-bundle-analyzer");

const pages = {};

[
  "common/mul_act_banner",
  "common/search_vertical",
  "common/single_act_banner",
  "common/static_banner",
  "common/search_vertical",
  "hotel/auto_hotel_dynamic_widget",
  "hotel/hotel_dynamic_widget",
  "things-to-do/auto_dynamic_widget",
  "things-to-do/dynamic_widget",
  // "common/static_banner/solidjs",
].reduce((acc, pagePath) => {
  const name = pagePath.split("/")[1];
  acc[name] = {
    entry: `src/render/${pagePath}/index.ts`,
    template: `public/${name}.html`,
    filename: `${name}.html`,
    minify: false,
  };
  return acc;
}, pages);

module.exports = {
  devServer: {
    port: 8785,
  },
  publicPath: "/static/widget",
  outputDir: path.join(__dirname, "../server/app/public/widget"),
  pages,
  crossorigin: "anonymous",
  configureWebpack: {
    devtool: false,
    entry: {
      iframe: path.join(__dirname, "./src/iframe/index.ts"),
    },
    output: {
      filename: (pathData) => {
        return pathData.chunk.name === "iframe"
          ? "js/iframe.js"
          : "js/[name].[chunkhash:8].js";
      },
    },
    context: __dirname,
    plugins: [
      new webpack.ProvidePlugin({
        $: "jquery",
      }),
      // new BundleAnalyzerPlugin(),
    ],
  },
};
